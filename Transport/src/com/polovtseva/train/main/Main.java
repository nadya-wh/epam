package com.polovtseva.train.main;

import com.polovtseva.train.action.WagonFinder;
import com.polovtseva.train.creator.TrainCreator;
import com.polovtseva.train.entity.ComfortLevel;
import com.polovtseva.train.entity.Train;
import com.polovtseva.train.entity.Wagon;
import com.polovtseva.train.util.ComfortLevelComparator;
import com.sun.java.swing.plaf.windows.WindowsTreeUI;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by User on 23.11.2015.
 */
public class Main {

    static final Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        Train train = TrainCreator.createTrainFromJSON("input.json");
        System.out.println(train);
        train.sortWagonsByComfortLevel();
        System.out.println(train.getWagons());
        int totalPassengerCount = 0;
        for (Wagon wagon : train.getWagons()) {
            totalPassengerCount += wagon.countPassengers();
        }

        System.out.println("Total passenger's count: " + totalPassengerCount);

        ArrayList<Wagon> foundWagons = WagonFinder.findWagons(train, 2, ComfortLevel.LOW);
        if (foundWagons.size() > 0) {
            System.out.println("Search result:\n" + foundWagons.toString());
        }

    }
}
