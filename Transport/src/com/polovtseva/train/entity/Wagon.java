package com.polovtseva.train.entity;

import java.util.ArrayList;

/**
 * Created by User on 23.11.2015.
 */
public class Wagon extends RailwayTransport {
    private int maxPlaceCount;
    private ComfortLevel comfortLevel;
    private int maxLuggageCount;

    private ArrayList<Passenger> passengers;

    public Wagon(int maxPlaceCount, ComfortLevel comfortLevel, int maxLuggageCount) {
        this.maxPlaceCount = maxPlaceCount;
        this.comfortLevel = comfortLevel;
        this.maxLuggageCount = maxLuggageCount;
        passengers = new ArrayList<>();
    }


    public Wagon(int maxPlaceCount, ComfortLevel comfortLevel, int maxLuggageCount, ArrayList<Passenger> passengers) {
        this.maxPlaceCount = maxPlaceCount;
        this.comfortLevel = comfortLevel;
        this.maxLuggageCount = maxLuggageCount;
        this.passengers = passengers;
    }

    public ComfortLevel getComfortLevel() {
        return comfortLevel;
    }

    public boolean addPassenger(Passenger passenger) {
        if (passengers.size() < maxPlaceCount && passengers.add(passenger)) {
            return true;
        }
        return false;
    }

    public boolean removePassenger(Passenger passenger) {
        return passengers.remove(passenger);
    }

    public int findPassengersCount() {
        return passengers.size();
    }

    public int countPassengers() {
        return passengers.size();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wagon wagon = (Wagon) o;

        if (maxPlaceCount != wagon.maxPlaceCount) return false;
        if (maxLuggageCount != wagon.maxLuggageCount) return false;
        if (comfortLevel != wagon.comfortLevel) return false;
        return !(passengers != null ? !passengers.equals(wagon.passengers) : wagon.passengers != null);

    }

    @Override
    public int hashCode() {
        int result = maxPlaceCount;
        result = 31 * result + (comfortLevel != null ? comfortLevel.hashCode() : 0);
        result = 31 * result + maxLuggageCount;
        result = 31 * result + (passengers != null ? passengers.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "\nWagon:" +
                "maxPlaceCount=" + maxPlaceCount +
                ", comfortLevel=" + comfortLevel +
                ", maxLuggageCount=" + maxLuggageCount +
                ", \n       passengers=" + passengers;
    }
}
