package com.polovtseva.train.entity;

/**
 * Created by User on 23.11.2015.
 */
public class Locomotive extends RailwayTransport {
    private int weight;

    public Locomotive(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Locomotive that = (Locomotive) o;

        return weight == that.weight;

    }

    @Override
    public int hashCode() {
        return weight;
    }

    @Override
    public String toString() {
        return "Locomotive{" +
                "weight=" + weight +
                '}';
    }
}
