package com.polovtseva.train.entity;

/**
 * Created by User on 24.11.2015.
 */
public class Luggage {
    private int weight;


    public Luggage(int weight) {
        this.weight = weight;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Luggage luggage = (Luggage) o;

        return weight == luggage.weight;

    }

    @Override
    public int hashCode() {
        return weight;
    }

    @Override
    public String toString() {
        return "Luggage{" +
                "weight=" + weight +
                '}';
    }
}
