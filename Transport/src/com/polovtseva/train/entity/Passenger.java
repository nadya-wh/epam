package com.polovtseva.train.entity;

/**
 * Created by User on 24.11.2015.
 */
public class Passenger {
    private int ticketId;
    private Luggage luggage;


    public Passenger(int ticketId, Luggage luggage) {
        this.ticketId = ticketId;
        this.luggage = luggage;
    }

    public int getTicketId() {
        return ticketId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passenger passenger = (Passenger) o;

        if (ticketId != passenger.ticketId) return false;
        return !(luggage != null ? !luggage.equals(passenger.luggage) : passenger.luggage != null);

    }

    @Override
    public int hashCode() {
        int result = ticketId;
        result = 31 * result + (luggage != null ? luggage.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "ticketId=" + ticketId +
                ", luggage=" + luggage +
                '}';
    }
}
