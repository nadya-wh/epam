package com.polovtseva.train.entity;

import com.polovtseva.train.util.ComfortLevelComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by User on 23.11.2015.
 */
public class Train {
    private Locomotive locomotive;
    private ArrayList<Wagon> wagons;

    public Train(Locomotive locomotive, ArrayList<Wagon> wagons) {
        this.locomotive = locomotive;
        this.wagons = wagons;
    }

    public int findWagonsCount() {
        return wagons.size();
    }

    public List<Wagon> getWagons() {
        return Collections.unmodifiableList(wagons);
    }

    public boolean addWagon(Wagon wagon) {
        return wagons.add(wagon);
    }

    public void sortWagonsByComfortLevel() {
        Collections.sort(wagons, new ComfortLevelComparator());
    }

    @Override
    public String toString() {
        return "Train: " +
                "\nlocomotive=" + locomotive +
                ",\nwagons=" + wagons;
    }
}
