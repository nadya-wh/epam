package com.polovtseva.train.entity;

/**
 * Created by User on 25.11.2015.
 */
public enum ComfortLevel {
    LOW(0),
    MEDIUM(1),
    HIGH(2);

    private Integer level;

    ComfortLevel(int level) {
        this.level = level;
    }

    public boolean isBetterThan(ComfortLevel other) {
        return this.level > other.level;
    }
}
