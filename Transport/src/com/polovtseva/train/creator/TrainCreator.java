package com.polovtseva.train.creator;

import com.polovtseva.train.entity.Locomotive;
import com.polovtseva.train.entity.Passenger;
import com.polovtseva.train.entity.Train;
import com.polovtseva.train.entity.Wagon;
import com.polovtseva.train.init.TrainInitializer;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by User on 24.11.2015.
 */
public class TrainCreator {
    public static Train createTrainFromJSON(String filename) {
        TrainInitializer trainInitializer = new TrainInitializer(filename);
        Locomotive locomotive = trainInitializer.initLocomotive();
        ArrayList<Wagon> wagons = trainInitializer.initWagons();
        Train train = new Train(locomotive, wagons);

        return train;
    }
}
