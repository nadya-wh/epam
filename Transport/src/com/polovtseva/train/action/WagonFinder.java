package com.polovtseva.train.action;

import com.polovtseva.train.entity.ComfortLevel;
import com.polovtseva.train.entity.Train;
import com.polovtseva.train.entity.Wagon;

import java.util.ArrayList;

/**
 * Created by User on 28.11.2015.
 */
public class WagonFinder {
    public static ArrayList<Wagon> findWagons(Train train, int passengerCount, ComfortLevel comfortLevel) {
        ArrayList<Wagon> wagons = new ArrayList<>();
        for (Wagon wagon : train.getWagons()) {
            if (wagon.getComfortLevel() == comfortLevel && wagon.countPassengers() == passengerCount) {
                wagons.add(wagon);
            }
        }
        return wagons;
    }
}
