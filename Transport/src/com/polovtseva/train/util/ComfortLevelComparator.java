package com.polovtseva.train.util;

import com.polovtseva.train.entity.Wagon;

import java.util.Comparator;

/**
 * Created by User on 25.11.2015.
 */
public class ComfortLevelComparator implements Comparator<Wagon> {

    @Override
    public int compare(Wagon o1, Wagon o2) {
        if (o1.getComfortLevel() == o2.getComfortLevel()) {
            return 0;
        } else if (o1.getComfortLevel().isBetterThan(o2.getComfortLevel())) {
            return 1;
        }
        return -1;
    }
}
