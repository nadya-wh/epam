package com.polovtseva.train.init;

import com.polovtseva.train.entity.*;

import java.io.*;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.json.*;

/**
 * Created by User on 24.11.2015.
 */
public class TrainInitializer {

    static final Logger LOGGER = Logger.getLogger(TrainInitializer.class);

    private String filename;
    private String file;
    private JSONObject jsonObject;

    public TrainInitializer(String filename) {
        this.filename = filename;
        init();
    }

    private void init() {
        readJSON();
        try {
            jsonObject = new JSONObject(file);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void readJSON() {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            file = sb.toString();
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage());
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }
    }

    public Locomotive initLocomotive() {
        JSONObject locomotiveJSON = null;
        Locomotive locomotive = null;
        try {
            locomotiveJSON = jsonObject.getJSONObject("locomotive");
            locomotive = new Locomotive(locomotiveJSON.getInt("weight"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locomotive;
    }

    public ArrayList<Wagon> initWagons() {
        ArrayList<Wagon> wagons = null;
        try {
            JSONArray wagonsJSON = jsonObject.getJSONArray("wagons");
            wagons = new ArrayList<>(wagonsJSON.length());
            for (int i = 0; i < wagonsJSON.length(); i++) {
                int maxPlaceCount = wagonsJSON.getJSONObject(i).getInt("maxPlaceCount");
                int cl = wagonsJSON.getJSONObject(i).getInt("comfortLevel");
                ComfortLevel comfortLevel = ComfortLevel.values()[cl];
                int maxLuggageCount = wagonsJSON.getJSONObject(i).getInt("maxLuggageCount");

                JSONArray passengersJSON = wagonsJSON.getJSONObject(i).getJSONArray("passengers");
                ArrayList<Passenger> passengers = makePassengerListFromJSON(passengersJSON);


                wagons.add(new Wagon(maxPlaceCount, comfortLevel, maxLuggageCount, passengers));
            }
        } catch (JSONException e) {
            LOGGER.error(e.getMessage());
        }
        return wagons;
    }

    private ArrayList<Passenger> makePassengerListFromJSON(JSONArray json) {
        ArrayList<Passenger> passengers = null;
        if (json != null) {
            passengers = new ArrayList<>(json.length());
            for (int i = 0; i < json.length(); i++) {
                try {
                    int ticketId = json.getJSONObject(i).getInt("ticketId");
                    int weight = json.getJSONObject(i).getInt("luggageWeight");
                    Luggage passengerLuggage = new Luggage(weight);
                    Passenger passenger = new Passenger(ticketId, passengerLuggage);
                    passengers.add(passenger);
                } catch (JSONException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }
        return passengers;
    }


}
