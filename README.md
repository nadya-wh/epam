# Epam Tasks #


### This repository contains the following tasks: ###

* Base OOP (Transport)
* Threads (Parking)
* Composite (TextParser)
* XML & XSD (XML)
* XML parsing with Web (WebParsingXml)
* Final Project (CoffeeMaven)