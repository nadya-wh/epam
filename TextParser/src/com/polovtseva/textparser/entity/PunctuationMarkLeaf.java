package com.polovtseva.textparser.entity;

/**
 * Created by User on 14.12.2015.
 */
public class PunctuationMarkLeaf implements Component {

    private LexemeType type;
    private char value;

    public PunctuationMarkLeaf(char value) {
        this.type = LexemeType.PUNCTUATION_MARK;
        this.value = value;
    }

    @Override
    public String toString() {
        return "PunctuationMarkLeaf{" +
                "type=" + type +
                ", value=" + value +
                '}';
    }

    @Override
    public LexemeType getType() {
        return type;
    }
}
