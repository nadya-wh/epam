package com.polovtseva.textparser.entity;

/**
 * Created by User on 13.12.2015.
 */
public enum LexemeType {
    TEXT,
    PARAGRAPH,
    SENTENCE,
    WORD,
    SYMBOL,
    PUNCTUATION_MARK,
    TAG,
    CODE
}
