package com.polovtseva.textparser.entity;

import java.util.ArrayList;

/**
 * Created by User on 13.12.2015.
 */
public class TextComposite implements Component { //Composite

    private ArrayList<Component> components;
    private LexemeType type;

    public TextComposite(ArrayList<Component> components, LexemeType type) {
        this.components = components;
        this.type = type;
    }

    public TextComposite(LexemeType type) {
        this.type = type;
    }

    @Override
    public LexemeType getType() {
        return type;
    }

    public void setComponents(ArrayList<Component> components) {
        this.components = components;
    }

    public int componentsSize() {
        return components.size();
    }

    public Component getComponent(int index) {
        return components.get(index);
    }

    public void setComponent(int index, Component component) {
        components.set(index, component);
    }

    public void removeComponent(Component component) {
        components.remove(component);
    }

    @Override
    public String toString() {
        return "\nTextComposite{" +
                "components=" + components +
                ", type=" + type +
                '}';
    }
}
