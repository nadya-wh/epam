package com.polovtseva.textparser.entity;

/**
 * Created by User on 13.12.2015.
 */
public interface Component {
    LexemeType getType();
}
