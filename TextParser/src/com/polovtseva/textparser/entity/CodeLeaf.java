package com.polovtseva.textparser.entity;

/**
 * Created by User on 14.12.2015.
 */
public class CodeLeaf implements Component {

    private LexemeType type;
    private String value;

    public CodeLeaf(String value) {
        this.type = LexemeType.CODE;
        this.value = value;
    }

    @Override
    public String toString() {
        return "CodeLeaf{" +
                "type=" + type +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public LexemeType getType() {
        return type;
    }
}
