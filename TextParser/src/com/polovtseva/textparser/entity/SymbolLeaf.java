package com.polovtseva.textparser.entity;

/**
 * Created by User on 14.12.2015.
 */
public class SymbolLeaf implements Component {

    private LexemeType type;
    private char value;

    public SymbolLeaf(char value) {
        this.type = LexemeType.SYMBOL;
        this.value = value;
    }

    @Override
    public String toString() {
        return "SymbolLeaf{" +
                "type=" + type +
                ", value=" + value +
                '}';
    }

    @Override
    public LexemeType getType() {
        return type;
    }
}
