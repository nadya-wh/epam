package com.polovtseva.textparser.entity;

/**
 * Created by User on 14.12.2015.
 */
public class TagLeaf implements Component {

    private LexemeType type;
    private String value;

    public TagLeaf(String value) {
        this.type = LexemeType.TAG;
        this.value = value;
    }

    @Override
    public String toString() {
        return "TagLeaf{" +
                "type=" + type +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public LexemeType getType() {
        return type;
    }
}
