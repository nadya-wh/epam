package com.polovtseva.textparser.main;

import com.polovtseva.textparser.action.ParserHandler;
import com.polovtseva.textparser.entity.TextComposite;
import com.polovtseva.textparser.init.TextReader;
import com.polovtseva.textparser.util.LastAndFirstWordsExchanger;
import com.polovtseva.textparser.util.WordRemoverByLength;


/**
 * Created by User on 14.12.2015.
 */


public class Main {

    public static void main(String[] args) {
        String textString = TextReader.readFile("input.txt");
        if (textString == null) {
            System.err.println("File was not found.");
            System.exit(0);
        }
        TextComposite text = ParserHandler.parse(textString);
        LastAndFirstWordsExchanger.exchange(text);
        WordRemoverByLength.remove(2, text);
        System.out.println(text);

    }
}
