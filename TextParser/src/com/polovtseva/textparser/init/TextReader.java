package com.polovtseva.textparser.init;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.Scanner;
import java.util.logging.Level;

/**
 * Created by User on 14.12.2015.
 */
public class TextReader {

    static Logger LOGGER = Logger.getLogger(TextReader.class);

    public static String readFile(String filename) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException e) {
            LOGGER.fatal(e);
            return null;
        }
        StringBuilder text = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                text.append(line);
            }
        } catch (IOException e) {
            LOGGER.error(e);
            return null;
        }
        return text.toString();
    }
}
