package com.polovtseva.textparser.util;

import com.polovtseva.textparser.entity.Component;
import com.polovtseva.textparser.entity.LexemeType;
import com.polovtseva.textparser.entity.TextComposite;

import java.util.ArrayList;

/**
 * Created by User on 16.12.2015.
 */
public class LastAndFirstWordsExchanger { //task 5

    private static void changeWords(TextComposite sentence) {
        if (sentence.componentsSize() > 0) {
            Component firstWord = sentence.getComponent(0);
            sentence.setComponent(0, sentence.getComponent(sentence.componentsSize() - 1));
            sentence.setComponent(sentence.componentsSize() - 1, firstWord);
        }
    }

    private static void changeWordsInParagraph(TextComposite paragraph) {
        for (int j = 0; j < paragraph.componentsSize(); j++) {
            if (paragraph.getComponent(j).getType() == LexemeType.SENTENCE) {
                TextComposite sentence = (TextComposite) paragraph.getComponent(j);
                changeWords(sentence);
            }
        }
    }

    public static void exchange(TextComposite text) {
        for (int i = 0; i < text.componentsSize(); i++) {
            Component component = text.getComponent(i);
            if (component.getType() == LexemeType.PARAGRAPH) {
                TextComposite paragraph = (TextComposite) component;
                changeWordsInParagraph(paragraph);
            }
        }
    }
}
