package com.polovtseva.textparser.util;

import com.polovtseva.textparser.entity.Component;
import com.polovtseva.textparser.entity.LexemeType;
import com.polovtseva.textparser.entity.TextComposite;

/**
 * Created by User on 16.12.2015.
 */
public class WordRemoverByLength { // task 12

    private static void removeWords(TextComposite sentence, int wordLength) {
        for (int i = 0; i < sentence.componentsSize(); i++) {
            if (sentence.getComponent(i).getType() == LexemeType.WORD) {
                TextComposite word = (TextComposite) sentence.getComponent(i);
                if (word.componentsSize() == wordLength) {
                    sentence.removeComponent(word);
                }
            }
        }
    }

    private static void removeWordsInParagraph(TextComposite paragraph, int wordLength) {
        for (int j = 0; j < paragraph.componentsSize(); j++) {
            if (paragraph.getComponent(j).getType() == LexemeType.SENTENCE) {
                TextComposite sentence = (TextComposite) paragraph.getComponent(j);
                removeWords(sentence, wordLength);
            }
        }
    }


    public static void remove(int wordLength, TextComposite text) {
        for (int i = 0; i < text.componentsSize(); i++) {
            Component component = text.getComponent(i);
            if (component.getType() == LexemeType.PARAGRAPH) {
                TextComposite paragraph = (TextComposite) component;
                removeWordsInParagraph(paragraph, wordLength);
            }
        }
    }
}
