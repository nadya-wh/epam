package com.polovtseva.textparser.action;

import com.polovtseva.textparser.entity.Component;

import java.util.ArrayList;

/**
 * Created by User on 14.12.2015.
 */
public abstract class AbstractParser {
    protected AbstractParser successor;

    public void setSuccessor(AbstractParser successor) {
        this.successor = successor;
    }

    abstract public ArrayList<Component> parse(String toParse);
}
