package com.polovtseva.textparser.action;

import com.polovtseva.textparser.entity.Component;
import com.polovtseva.textparser.entity.LexemeType;
import com.polovtseva.textparser.entity.PunctuationMarkLeaf;
import com.polovtseva.textparser.entity.TextComposite;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 14.12.2015.
 */
public class WordParser extends AbstractParser {

    public static final String WORD_REGEX = "(\\w+)";

    public WordParser(AbstractParser successor) {
        setSuccessor(successor);
    }

    @Override
    public ArrayList<Component> parse(String toParse) {
        System.out.println(toParse);
        ArrayList<Component> words = new ArrayList<>();
        Pattern pattern = Pattern.compile(WORD_REGEX);
        Matcher matcher = pattern.matcher(toParse);
        int prevIndex = 0;
        int currentIndex = 0;
        while (matcher.find()) {
            TextComposite word = new TextComposite(LexemeType.WORD);
            String wordString = matcher.group(1);
            if (prevIndex > 0) {
                currentIndex = matcher.start();
                if (currentIndex - prevIndex == 1) {
                    words.add(new PunctuationMarkLeaf(toParse.charAt(prevIndex)));
                }
            }
            if (successor != null) {
                word.setComponents(successor.parse(wordString));
                words.add(word);
            }

            prevIndex = matcher.end();
        }
        if (prevIndex != toParse.length() && prevIndex < toParse.length()) {
            for (int i = prevIndex; i < toParse.length(); i++) {
                words.add(new PunctuationMarkLeaf(toParse.charAt(i)));
            }
        }
        return words;

    }
}
