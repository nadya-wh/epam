package com.polovtseva.textparser.action;

import com.polovtseva.textparser.entity.Component;
import com.polovtseva.textparser.entity.LexemeType;
import com.polovtseva.textparser.entity.TextComposite;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 14.12.2015.
 */
public class ParagraphParser extends AbstractParser {

    public ParagraphParser(AbstractParser successor) {
        setSuccessor(successor);
    }

    @Override
    public ArrayList<Component> parse(String toParse) {
        ArrayList<Component> paragraphs = new ArrayList();
        Pattern pattern = Pattern.compile("\\t?+.+");
        Matcher matcher = pattern.matcher(toParse);

        while (matcher.find()) {
            String paragraphString = toParse.substring(matcher.start(), matcher.end());
            if (successor != null) {
                TextComposite paragraph = new TextComposite(successor.parse(paragraphString), LexemeType.PARAGRAPH);
                paragraphs.add(paragraph);
            }
        }
        return paragraphs;
    }
}
