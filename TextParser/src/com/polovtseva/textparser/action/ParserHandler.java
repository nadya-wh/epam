package com.polovtseva.textparser.action;

import com.polovtseva.textparser.entity.LexemeType;
import com.polovtseva.textparser.entity.TextComposite;

/**
 * Created by User on 14.12.2015.
 */
public interface ParserHandler {
    static TextComposite parse(String textToParse) {
        SymbolParser symbolParser = new SymbolParser(null);
        WordParser wordParser = new WordParser(symbolParser);
        SentenceParser sentenceParser = new SentenceParser(wordParser);
        ParagraphParser paragraphParser = new ParagraphParser(sentenceParser);
        TextParser textParser = new TextParser(paragraphParser);
        TextComposite text = new TextComposite(textParser.parse(textToParse), LexemeType.TEXT);
        return text;
    }
}
