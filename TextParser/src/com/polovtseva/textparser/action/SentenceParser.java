package com.polovtseva.textparser.action;

import com.polovtseva.textparser.entity.Component;
import com.polovtseva.textparser.entity.LexemeType;
import com.polovtseva.textparser.entity.PunctuationMarkLeaf;
import com.polovtseva.textparser.entity.TextComposite;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 14.12.2015.
 */
public class SentenceParser extends AbstractParser {

    public static final String SENTENCE_REGEX = "([A-Z].*?)([.!?])(?=\\s[A-Z]|$|\\n)";
    public static final int PUNCTUATION_MARK_GROUP_NUM = 2;


    public SentenceParser(AbstractParser successor) {
        setSuccessor(successor);
    }

    @Override
    public ArrayList<Component> parse(String toParse) {
        ArrayList<Component> parts = new ArrayList<>();
        Pattern pattern = Pattern.compile(SENTENCE_REGEX); //TODO: regex
        Matcher matcher = pattern.matcher(toParse);
        while (matcher.find()) {
            TextComposite sentence = new TextComposite(LexemeType.SENTENCE);
            String sentenceString = matcher.group(1);
            if (successor != null) {
                sentence.setComponents(successor.parse(sentenceString));
                parts.add(sentence);
            }
            parts.add(new PunctuationMarkLeaf(matcher.group(PUNCTUATION_MARK_GROUP_NUM).charAt(0)));
        }
        return parts;
    }
}
