package com.polovtseva.textparser.action;

import com.polovtseva.textparser.entity.CodeLeaf;
import com.polovtseva.textparser.entity.Component;
import com.polovtseva.textparser.entity.LexemeType;
import com.polovtseva.textparser.entity.TagLeaf;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 13.12.2015.
 */
public class TextParser extends AbstractParser {

    public final String START_CODE_TAG = "<code>";
    public final String STOP_CODE_TAG = "</code>";

    public TextParser(AbstractParser successor) {
        setSuccessor(successor);
    }

    @Override
    public ArrayList<Component> parse(String toParse) {

        Pattern patternBeginCode = Pattern.compile(START_CODE_TAG);
        Pattern patternStopCode = Pattern.compile(STOP_CODE_TAG);
        Matcher beginCodeMatcher = patternBeginCode.matcher(toParse);
        Matcher stopCodeMatcher = patternStopCode.matcher(toParse);
        ArrayList<Component> parts = new ArrayList<>();
        while (beginCodeMatcher.find()) {
            int beginCode = beginCodeMatcher.end();
            stopCodeMatcher.find();
            int stopCode = stopCodeMatcher.start();

            String beforeParts = toParse.substring(0, beginCodeMatcher.start());
            String code = toParse.substring(beginCode, stopCode);
            toParse = toParse.substring(stopCodeMatcher.end()); //TODO: check!
            if (successor != null) {
                parts.addAll(successor.parse(beforeParts));
            }
            parts.add(new TagLeaf(START_CODE_TAG));
            parts.add(new CodeLeaf(code));
            parts.add(new TagLeaf(STOP_CODE_TAG));
            beginCodeMatcher = patternBeginCode.matcher(toParse);
            stopCodeMatcher = patternStopCode.matcher(toParse);
        }

        if (successor != null && toParse != "") {
            parts.addAll(successor.parse(toParse));
        }
        return parts;
    }
}
