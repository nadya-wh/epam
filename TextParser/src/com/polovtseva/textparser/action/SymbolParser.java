package com.polovtseva.textparser.action;

import com.polovtseva.textparser.entity.Component;
import com.polovtseva.textparser.entity.SymbolLeaf;

import java.util.ArrayList;

/**
 * Created by User on 14.12.2015.
 */
public class SymbolParser extends AbstractParser {

    public SymbolParser(AbstractParser successor) {
        setSuccessor(successor);
    }

    @Override
    public ArrayList<Component> parse(String toParse) {
        ArrayList<Component> parts = new ArrayList<>();
        for (int i = 0; i < toParse.length(); i++) {
            parts.add(new SymbolLeaf(toParse.charAt(i)));
        }
        return parts;
    }
}
