package com.polovtseva.parking.main;

import com.polovtseva.parking.creator.RandomCreator;
import com.polovtseva.parking.entity.Car;

import java.util.ArrayList;

/**
 * Created by User on 05.12.2015.
 */
public class Main {

    public static void main(String[] args) {
        ArrayList<Car> cars = RandomCreator.createCars();
        System.out.println(cars.size());
        cars.forEach(Car::start);
    }
}
