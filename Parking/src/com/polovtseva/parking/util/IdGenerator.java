package com.polovtseva.parking.util;

/**
 * Created by User on 05.12.2015.
 */
public class IdGenerator {
    private static int parkingId = 0;
    private static int carId = 0;

    public static int getParkingId() {
        return parkingId++;
    }

    public static int getCarId() {
        return carId++;
    }
}
