package com.polovtseva.parking.creator;

import com.polovtseva.parking.entity.Car;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by User on 06.12.2015.
 */
public class RandomCreator {

    private static Random random = new Random();

    public static final int MAX_CAR_COUNT = 10;
    public static final int MAX_WAIT_TIME = 3000;

    public static ArrayList<Car> createCars() {
        int carCount = random.nextInt(MAX_CAR_COUNT);
        ArrayList<Car> carOwners = new ArrayList<>(carCount);
        for (int i = 0; i < carCount; i++) {
            carOwners.add(new Car(random.nextInt(MAX_WAIT_TIME)));
        }
        return carOwners;
    }
}
