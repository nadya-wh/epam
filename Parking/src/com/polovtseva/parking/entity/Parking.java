package com.polovtseva.parking.entity;

import com.polovtseva.parking.exception.SingletonException;
import com.polovtseva.parking.util.IdGenerator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 05.12.2015.
 */
public class Parking {

    static final Logger LOGGER = Logger.getLogger(Parking.class);

    public static final int MAX_AVAILABLE_INSTANCES = 2;
    public static final int PLACE_COUNT = 5;

    private int parkingId;
    private List<Car> parkedCars;

    private AtomicInteger placeAvailable = new AtomicInteger(PLACE_COUNT);
    private ReentrantLock lockPlaceAvailable = new ReentrantLock(true);
    private static Semaphore instanceSemaphore = new Semaphore(MAX_AVAILABLE_INSTANCES, true);
    private static ArrayList<Parking> instances = new ArrayList<Parking>(MAX_AVAILABLE_INSTANCES);


    private Parking() {
        parkingId = IdGenerator.getParkingId();
        parkedCars = Collections.synchronizedList(new LinkedList<Car>());
    }

    public static Parking getInstance(int index) throws SingletonException {
        if (index >= 0 && index < instances.size()) {
            Parking parking = instances.get(index);
            return parking;
        } else if (instanceSemaphore.tryAcquire()) {
            Parking parking = new Parking();
            instances.add(parking);
            return parking;
        } else {
            throw new SingletonException("Limit is exceeded." + index + " " + instances.size());
        }
    }

    public int getParkingId() {
        return parkingId;
    }

    public boolean takePlace(Car car) {
        try {
            if (lockPlaceAvailable.tryLock(car.getTime(), TimeUnit.MILLISECONDS)) {
                if (placeAvailable.get() > 0) {
                    placeAvailable.getAndDecrement();
                    parkedCars.add(car);
                    lockPlaceAvailable.unlock();
                    return true;
                }

            }
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
        return false;
    }


    public void returnPlace(Car car) {
        placeAvailable.incrementAndGet();
        //lockPlaceAvailable.lock();
        parkedCars.remove(car);
        //lockPlaceAvailable.unlock();
    }


    @Override
    public String toString() {
        return "Parking{" +
                "parkingId=" + parkingId +
                ", parkedCars=" + parkedCars +
                ", placeAvailable=" + placeAvailable +
                '}';
    }
}
