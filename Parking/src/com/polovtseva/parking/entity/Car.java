package com.polovtseva.parking.entity;

import com.polovtseva.parking.exception.SingletonException;
import com.polovtseva.parking.util.IdGenerator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.Random;

/**
 * Created by User on 02.11.2015.
 */
public class Car extends Thread {

    static final Logger LOGGER = Logger.getLogger(Car.class);

    private boolean parked;
    private int parkingId;
    private int time;
    private int carId;
    private static Random random = new Random();

    public static final int MAX_TIME = 3000;


    public Car(int time) {
        parked = false;
        this.time = time;
        carId = IdGenerator.getCarId();
    }

    public int getCarId() {
        return carId;
    }

    public int getTime() {
        return time;
    }

    public boolean enterParking(Parking parking) {
        if (!parking.takePlace(this)) {
            return false;
        }
        parked = true;
        parkingId = parking.getParkingId();
        return true;
    }

    public void leaveParking() {
        System.out.println("Trying to leave parking: " + getCarId());
        parked = false;
        try {
            Parking.getInstance(parkingId).returnPlace(this);
        } catch (SingletonException e) {
            LOGGER.error(e);
        }
    }

    @Override
    public void run() {
        boolean carWasParked = false;
        LOGGER.debug("Get instance: " + parkingId);
        for (int parkingId = 0; parkingId < Parking.MAX_AVAILABLE_INSTANCES && !carWasParked; parkingId++) {
            try {
                if (enterParking(Parking.getInstance(parkingId))) {
                    carWasParked = true;
                    int timeToStay = random.nextInt(MAX_TIME);
                    System.out.println("Car was parked for : " + timeToStay + " " + toString());
                    try {
                        System.out.println("Sleep for: " + timeToStay);
                        Thread.sleep(timeToStay);
                        LOGGER.debug("Stopped sleeping for: " + timeToStay);
                    } catch (InterruptedException e) {
                        LOGGER.error(e);
                    }
                    leaveParking();
                    System.out.println("\tCar left parking: " + toString());
                } else {
                    System.out.println("Car left without parking: carId: " + getCarId() + " parkingId: " + parkingId);
                }
            } catch (SingletonException e) {
                LOGGER.error(e);
                System.out.println("Car left without parking: carId: " + getCarId() + " parkingId: " + parkingId);
            }
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "parked=" + parked +
                ", parkingId=" + parkingId +
                ", time=" + time +
                ", carId=" + carId +
                '}';
    }
}
