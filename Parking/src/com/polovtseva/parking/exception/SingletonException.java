package com.polovtseva.parking.exception;

import java.security.PrivilegedActionException;

/**
 * Created by User on 05.12.2015.
 */
public class SingletonException extends Exception {

    public SingletonException() {
    }

    public SingletonException(String message) {
        super(message);
    }

    public SingletonException(String message, Throwable cause) {
        super(message, cause);
    }

    public SingletonException(Throwable cause) {
        super(cause);
    }

    public SingletonException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
