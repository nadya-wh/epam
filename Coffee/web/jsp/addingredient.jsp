<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="addrink.jsp"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="res.i18n.text"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="addingredient.header"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">

    <script src="../js/ie-emulation-modes-warning.js"></script>
</head>
<header>

</header>
<body>

<%@include file="jspf/navbar.jspf"%>

<div class="container" style="vertical-align: middle !important;">
    <form class="form-signin" action="/coffee" method="POST">
        <c:if test="${error}">
            <div class="alert alert-danger">
                <fmt:bundle basename="res.messages">
                    <fmt:message key="message.addingredienterror"/>
                </fmt:bundle>
            </div>
        </c:if>
        <c:if test="${success}">
            <div class="alert alert-success">
                <fmt:bundle basename="res.messages">
                    <fmt:message key="message.addingredientsuccess"/>
                </fmt:bundle>
            </div>
        </c:if>
        <c:if test="${validationError}">
            <div class="alert alert-danger">
                <fmt:bundle basename="res.messages">
                    <fmt:message key="message.validationerror"/>
                </fmt:bundle>
            </div>
        </c:if>
        <input type="hidden" name="command" value="add_ingredient">
        <h2 class="form-signin-heading" align="center">
            <fmt:message key="addingredient.header"/>
        </h2>
        <h5>
            <fmt:message key="addingredient.form.ingredientname"/>*
        </h5>
        <input class="form-control custom-input" type="text" name="ingredientName">
        <h5>
            <fmt:message key="addingredient.form.ingredientprice"/>*
        </h5>
        <input class="form-control custom-input" type="text" name="ingredientPrice">
        <h5>
            <fmt:message key="addingredient.form.imageurl"/>
        </h5>
        <input class="form-control custom-input" type="url" name="imageUrl">
        <h5>
            <fmt:message key="addingredient.form.portionsLeft"/>
        </h5>
        <input class="form-control custom-input" type="number" step="0.1" name="portionsLeft">
        <button class="btn-block custom-button" type="submit">
            <fmt:message key="addingredient.button.submit"/>
        </button>
    </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <ctg:pfooter/>
</footer>
</html>
