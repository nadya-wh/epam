<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="/jsp/signupform.jsp"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="res.i18n.text"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="signup"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">
    <script src="../js/ie-emulation-modes-warning.js"></script>
    <script>
        function validateForm() {
            document.getElementById("firstNameValidationError").innerHTML = '';
            document.getElementById("loginValidationError").innerHTML = '';
            document.getElementById("passwordValidationError").innerHTML = '';
            document.getElementById("secondNameValidationError").innerHTML = '';
            var login = document.forms["signUpForm"]["username"].value;
            var loginPattern = /^[a-zA-Z0-9_-]{6,15}$/;
            var password = document.forms["signUpForm"]["password"].value;
            var confirmPassword = document.forms["signUpForm"]["confirm-password"].value;
            var passwordPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.])(?=\S+$).{8,}$/;
            var firstName = document.forms["signUpForm"]["firstname"].value;
            var secondName = document.forms["signUpForm"]["lastname"].value;
            var namePattern = /^[a-zA-Z0-9]+$/;
            if (!namePattern.test(firstName)) {
                document.getElementById("firstNameValidationError").innerHTML =
                        "<div class='alert alert-danger'><fmt:bundle basename='res.messages'><fmt:message key='message.notvalidname'/></fmt:bundle></div>";
                return false;
            } else if (!namePattern.test(secondName)) {
                document.getElementById("secondNameValidationError").innerHTML =
                        "<div class='alert alert-danger'><fmt:bundle basename='res.messages'><fmt:message key='message.notvalidname'/></fmt:bundle></div>";
                return false;AC
            } else if (!loginPattern.test(login)) {
                document.getElementById("loginValidationError").innerHTML =
                        "<div class='alert alert-danger'><fmt:bundle basename='res.messages'><fmt:message key='message.notvalidlogin'/></fmt:bundle></div>";
                return false;
            } else if (!passwordPattern.test(password)) {
                document.getElementById("passwordValidationError").innerHTML =
                        "<div class='alert alert-danger'><fmt:bundle basename='res.messages'><fmt:message key='message.notvalidpassword'/></fmt:bundle></div>";
                return false;
            } else if (password.localeCompare(confirmPassword) != 0) {
                document.getElementById("passwordValidationError").innerHTML =
                        "<div class='alert alert-danger'><fmt:bundle basename='res.messages'><fmt:message key='message.passwordsdontmatch'/></fmt:bundle></div>";
                return false;
            }
        }
    </script>
</head>
<header>

</header>
<body>


<%@include file="jspf/navbar.jspf" %>

<div class="container" style="vertical-align: middle !important;">


    <h2 class="form-signin-heading" align="center">
        <fmt:message key="signup"/>
    </h2>
    <c:if test="${error}">
        <div id="error" class="alert alert-danger">
            <fmt:bundle basename='res.messages'>
                <fmt:message key="message.notuniquelogin"/>
            </fmt:bundle>
        </div>
    </c:if>
    <c:if test="${confirmError}">
        <div id="error" class="alert alert-danger">
            <fmt:bundle basename='res.messages'>
                <fmt:message key="message.passwordsdontmatch"/>
            </fmt:bundle>
        </div>
    </c:if>
    <form id="signUpForm" onsubmit="return validateForm()" class="form-signin" name="login" action="/coffee"
          method="POST">

        <input type="hidden" id="command" name="command" value="sign_up"/>
        <h5>
            <fmt:message key="firstname"/>*
        </h5>
        <div id="firstNameValidationError"></div>
        <input type="text" id="firstname" name="firstname" class="form-control custom-input"
               placeholder="<fmt:message key="firstname"/>"/>
        <h5>
            <fmt:message key="lastname"/>*
        </h5>
        <div id="secondNameValidationError"></div>
        <input type="text" id="lastname" name="lastname" class="form-control custom-input"
               placeholder="<fmt:message key="lastname"/>"/>
        <h5>
            <fmt:message key="login.label.username"/>*
        </h5>

        <div id="loginValidationError"></div>


        <input type="text" id="username" name="username" class="form-control custom-input"
               placeholder="<fmt:message key="login.label.username"/>"/>
        <h5>
            <fmt:message key="login.label.password"/>*
        </h5>


        <div id="passwordValidationError"></div>
        <input type="password" name="password" class="form-control custom-input"
               placeholder="<fmt:message key="login.label.password"/>"/>

        <input type="password" name="confirm-password" class="form-control custom-input"
               placeholder="<fmt:message key="login.label.confirmpassword"/>"/>
        <button class="btn-block custom-button" type="submit">
            <fmt:message key="signup.button.submit"/>
        </button>
    </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <ctg:pfooter/>
</footer>
</html>