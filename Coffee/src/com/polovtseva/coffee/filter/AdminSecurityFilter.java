package com.polovtseva.coffee.filter;

import com.polovtseva.coffee.command.PagesNames;
import com.polovtseva.coffee.domain.UserRole;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by User on 14.02.2016.
 */
@WebFilter(filterName = "RoleSecurityFilter",
        urlPatterns = {"/coffee/drinks", "/coffee/ingredients", "/coffee/orders"})
public class AdminSecurityFilter implements Filter {

    public static final String USER_ROLE_ATTR = "role";

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpServletResponse httpResponse = (HttpServletResponse) resp;
        if (httpRequest.getSession().getAttribute(USER_ROLE_ATTR) != null) {
            UserRole role = UserRole.valueOf(httpRequest.getSession().getAttribute(USER_ROLE_ATTR).toString().toUpperCase());
            if (role != UserRole.ADMIN) {
                httpResponse.sendRedirect(PagesNames.MAIN_PAGE_KEY); // TODO: 14.02.2016 or send to page "you don't have access"?
            }
        } else {
            httpResponse.sendRedirect(PagesNames.SIGN_IN_URL);
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
