package com.polovtseva.coffee.filter;

import com.polovtseva.coffee.command.PagesNames;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by User on 14.02.2016.
 */
@WebFilter(filterName = "PagesRedirectSecurityFilter",
        urlPatterns = {"/jsp/*"})
public class PagesRedirectSecurityFilter implements Filter {

    public static final String USERNAME_ATTR = "username";

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        if (httpRequest.getSession().getAttribute(USERNAME_ATTR) != null) {
            httpResponse.sendRedirect(PagesNames.MAIN_PAGE_KEY);
        } else  {
            httpResponse.sendRedirect(PagesNames.SIGN_IN_URL);
        }

        chain.doFilter(request, response);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
