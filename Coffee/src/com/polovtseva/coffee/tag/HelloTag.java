package com.polovtseva.coffee.tag;

import com.polovtseva.coffee.domain.UserRole;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class HelloTag extends TagSupport {

    public static final String FIRST_NAME_ATTR = "username";
    private String role;

    public void setRole(String role) {
        this.role = role;
    }


    @Override
    public int doStartTag() throws JspException {
        try {
            String to;
            if (UserRole.ADMIN.getValue().equalsIgnoreCase(role)) {
                to = role;
            } else if (UserRole.USER.getValue().equalsIgnoreCase(role)) {
                to = pageContext.getSession().getAttribute(FIRST_NAME_ATTR).toString();
            } else {
                to = "guest";
            }
            pageContext.getOut().write(to);
        } catch (IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }
}