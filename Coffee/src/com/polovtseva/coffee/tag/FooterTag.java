package com.polovtseva.coffee.tag;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Created by User on 11.02.2016.
 */
public class FooterTag extends TagSupport {


    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.write("<h4>&copy; Nadezhda Polovtseva</h4>"); // TODO: 22.02.2016 const
        } catch (IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }



}
