package com.polovtseva.coffee.domain;

/**
 * Created by User on 26.01.2016.
 */
public class Ingredient extends Entity {
    private String ingredientName;
    private double ingredientPrice;
    private String imageUrl;// TODO: 07.02.2016 is ot okay?
    private int portionsLeft;
    private int numberInDrink;

    public Ingredient() {
        super();
    }

    public Ingredient(long id, String ingredientName, double ingredientPrice, String imageUrl, int portionsLeft) {
        super(id);
        this.ingredientName = ingredientName;
        this.ingredientPrice = ingredientPrice;
        this.imageUrl = imageUrl;
        this.portionsLeft = portionsLeft;
    }

    public Ingredient(long id, String ingredientName, double ingredientPrice, String imageUrl, int portionsLeft, int numberInDrink) {
        super(id);
        this.ingredientName = ingredientName;
        this.ingredientPrice = ingredientPrice;
        this.imageUrl = imageUrl;
        this.portionsLeft = portionsLeft;
        this.numberInDrink = numberInDrink;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public double getIngredientPrice() {
        return ingredientPrice;
    }

    public void setIngredientPrice(double ingredientPrice) {
        this.ingredientPrice = ingredientPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPortionsLeft() {
        return portionsLeft;
    }

    public void setPortionsLeft(int portionsLeft) {
        this.portionsLeft = portionsLeft;
    }

    public int getNumberInDrink() {
        return numberInDrink;
    }

    public void setNumberInDrink(int numberInDrink) {
        this.numberInDrink = numberInDrink;
    }
}
