package com.polovtseva.coffee.domain;

/**
 * Created by User on 28.01.2016.
 */
public enum UserRole {
    USER("user"),
    ADMIN("admin");

    String value;

    UserRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
