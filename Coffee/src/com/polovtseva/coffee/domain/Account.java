package com.polovtseva.coffee.domain;

/**
 * Created by User on 10.02.2016.
 */
public class Account extends Entity {
    private long userId;
    private double balance;

    public Account(int id, long userId, double balance) {
        super(id);
        this.userId = userId;
        this.balance = balance;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
