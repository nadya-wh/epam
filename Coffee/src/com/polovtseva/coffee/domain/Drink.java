package com.polovtseva.coffee.domain;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * Created by User on 26.01.2016.
 */
public class Drink extends Entity{

    static final Logger LOG = Logger.getLogger(Drink.class);

    private String drinkName;
    private int portionsLeft;
    private double drinkPrice;
    private String imageUrl;
    private List<Ingredient> ingredients;
    private int numberInOrder;


    public Drink(long id, String drinkName, int portionsLeft, double drinkPrice, String imageUrl) {
        super(id);
        this.drinkName = drinkName;
        this.portionsLeft = portionsLeft;
        this.drinkPrice = drinkPrice;
        this.imageUrl = imageUrl;
        ingredients = new ArrayList<>();
    }

    public Drink(long id, String drinkName, int portionsLeft, double drinkPrice, String imageUrl, int numberInOrder) {
        super(id);
        this.drinkName = drinkName;
        this.portionsLeft = portionsLeft;
        this.drinkPrice = drinkPrice;
        this.imageUrl = imageUrl;
        this.numberInOrder = numberInOrder;
        ingredients = new ArrayList<>();
    }

    public String getDrinkName() {
        return drinkName;
    }

    public void setDrinkName(String drinkName) {
        this.drinkName = drinkName;
    }

    public int getPortionsLeft() {
        return portionsLeft;
    }

    public void setPortionsLeft(int portionsLeft) {
        this.portionsLeft = portionsLeft;
    }

    public double getDrinkPrice() {
        return drinkPrice;
    }

    public void setDrinkPrice(double drinkPrice) {
        this.drinkPrice = drinkPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public boolean addIngredient(Ingredient ingredient) {
        return ingredients.add(ingredient);
    }

    public boolean removeIngredient(Ingredient ingredient) {
        return ingredients.remove(ingredient);
    }

    public int getNumberInOrder() {
        return numberInOrder;
    }

    public void setNumberInOrder(int numberInOrder) {
        this.numberInOrder = numberInOrder;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Drink drink = (Drink) o;

        if (portionsLeft != drink.portionsLeft) return false;
        if (Double.compare(drink.drinkPrice, drinkPrice) != 0) return false;
        if (numberInOrder != drink.numberInOrder) return false;
        if (drinkName != null ? !drinkName.equals(drink.drinkName) : drink.drinkName != null) return false;
        if (imageUrl != null ? !imageUrl.equals(drink.imageUrl) : drink.imageUrl != null) return false;
        return ingredients != null ? ingredients.equals(drink.ingredients) : drink.ingredients == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = drinkName != null ? drinkName.hashCode() : 0;
        result = 31 * result + portionsLeft;
        temp = Double.doubleToLongBits(drinkPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + (ingredients != null ? ingredients.hashCode() : 0);
        result = 31 * result + numberInOrder;
        return result;
    }
}
