package com.polovtseva.coffee.domain;

/**
 * Created by User on 28.01.2016.
 */
public abstract class Entity {
    private long id;

    public Entity() {

    }

    public Entity(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
