package com.polovtseva.coffee.dao.impl;

import com.polovtseva.coffee.dao.DrinkDAO;
import com.polovtseva.coffee.dao.pool.ConnectionPool;
import com.polovtseva.coffee.dao.pool.ProxyConnection;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.dao.pool.exception.ConnectionPoolException;
import com.polovtseva.coffee.dao.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 04.02.2016.
 */
public class DrinkDAOImpl implements DrinkDAO {

    private static final DrinkDAOImpl instance = new DrinkDAOImpl();

    private static final String SQL_SELECT_DRINKS = "SELECT drink_id, drink_name, " +
            "portions_left, drink_price, image_url FROM drink;";

    private static final String SQL_SELECT_DRINKS_IN_ORDER = "SELECT drink_id, drink_name, " +
            "portions_left, drink_price, image_url FROM drink NATURAL JOIN order_drink WHERE order_id=?;";

    private static final String SQL_INSERT_DRINK = "INSERT INTO drink (drink_name, portions_left, " +
            "drink_price, image_url) VALUES(?, ?, ?, ?);";

    private static final String SQL_SELECT_DRINK_BY_ID = "SELECT drink_name, portions_left, drink_price, " +
            "image_url FROM drink WHERE drink_id=?;";

    private static final String SQL_UPDATE_PORTIONS = "UPDATE drink SET portions_left=? WHERE " +
            "drink_id=?";

    private static final String SQL_DELETE_DRINK_BY_ID = "DELETE FROM drink WHERE drink_id=?;";

    private static final String SQL_COUNT_DRINKS_IN_ORDER = "SELECT COUNT(*) FROM drink " +
            "NATURAL JOIN order_drink WHERE drink_id=?;";


    private DrinkDAOImpl() {
    }

    public static DrinkDAOImpl getInstance() {
        return instance;
    }

    @Override
    public List<Drink> findAll() throws DAOException {
        List<Drink> drinks;
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_DRINKS)
        ) {
            ResultSet resultSet = preparedStatement.executeQuery();
            drinks = takeDrinks(resultSet);
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
        return drinks;
    }

    @Override
    public Drink findOne(long id) throws DAOException {
        Drink drink = null;
        try (
                ProxyConnection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_SELECT_DRINK_BY_ID)

        ) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                String drinkName = resultSet.getString("drink_name");
                String imageUrl = resultSet.getString("image_url");
                int portionsLeft = resultSet.getInt("portions_left");
                double drinkPrice = resultSet.getDouble("drink_price");
                drink = new Drink(id, drinkName, portionsLeft, drinkPrice, imageUrl);
            }

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
        return drink;
    }

    @Override
    public int delete(long id) throws DAOException {// FIXME: 14.02.2016 change
        try (
                ProxyConnection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_DELETE_DRINK_BY_ID);
        ) {
            preparedStatement.setLong(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }

    }

    @Override
    public int delete(Drink entity) throws DAOException {
        return delete(entity.getId());
    }

    @Override
    public int create(Drink entity) throws DAOException {
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_DRINK);
        ) {
            preparedStatement.setString(1, entity.getDrinkName());
            preparedStatement.setInt(2, entity.getPortionsLeft());
            preparedStatement.setDouble(3, entity.getDrinkPrice());
            preparedStatement.setString(4, entity.getImageUrl());
            return preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
    }



    public ArrayList<Drink> findAllInOrder(long orderId) throws DAOException {
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_DRINKS_IN_ORDER);
        ) {
            preparedStatement.setLong(1, orderId);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Drink> drinks = takeDrinks(resultSet);
            return drinks;
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
    }

    private ArrayList<Drink> takeDrinks(ResultSet resultSet) throws SQLException {
        ArrayList<Drink> drinks = new ArrayList<>();
        while (resultSet.next()) {
            long drinkId = resultSet.getLong("drink_id");
            String drinkName = resultSet.getString("drink_name");
            int portionsLeft = resultSet.getInt("portions_left");
            double drinkPrice = resultSet.getDouble("drink_price");
            String imageUrl = resultSet.getString("image_url");
            drinks.add(new Drink(drinkId, drinkName, portionsLeft, drinkPrice, imageUrl));
        }
        return drinks;
    }

    public int updatePortions(long drinkId, int portionsNumber) throws DAOException {
        try (
                ProxyConnection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_UPDATE_PORTIONS);
        ) {
            preparedStatement.setInt(1, portionsNumber);
            preparedStatement.setLong(2, drinkId);
            return preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
    }

    public int countInOrders(long drinkId) throws DAOException {
        try(
                ProxyConnection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_COUNT_DRINKS_IN_ORDER);
                ) {
            preparedStatement.setLong(1, drinkId);
            ResultSet resultSet = preparedStatement.executeQuery();
            int count = 0;
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            return count;
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
    }

}
