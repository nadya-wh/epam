package com.polovtseva.coffee.dao.impl;

import com.polovtseva.coffee.dao.UserDAO;
import com.polovtseva.coffee.dao.pool.ConnectionPool;
import com.polovtseva.coffee.dao.pool.ProxyConnection;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;
import com.polovtseva.coffee.dao.pool.exception.ConnectionPoolException;
import com.polovtseva.coffee.dao.exception.DAOException;

import org.apache.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by User on 20.01.2016.
 */
public class UserDAOImpl implements UserDAO {

    private static UserDAOImpl instance = new UserDAOImpl();

    final static Logger LOG = Logger.getLogger(UserDAOImpl.class);

    private static final String ROLE_ID_COLUMN_NAME = "role_id";

    private static final String SQL_CHECK_USER_EXIST = "SELECT user_id FROM user WHERE username = ? AND password = ?;";
    private static final String SQL_INSERT_USER = "INSERT INTO user(role, username, password, " +
            "first_name, last_name) VALUES (?, ?, ?, ?, ?);";
    private static final String SQL_SELECT_ROLE_ID = "SELECT role_id FROM role WHERE role_name=?;";
    private static final String SQL_SELECT_USER_BY_ID = "SELECT role_name, username, password, first_name, last_name " +
            "FROM user u JOIN role r ON (u.role = r.role_id) WHERE user_id=?;";
    private static final String SQL_SELECT_USER_BY_USERNAME = "SELECT user_id, role_name, username, password, first_name, last_name " +
            "FROM user u JOIN role r ON (u.role = r.role_id) WHERE username=?;";
    private static final String SQL_DELETE_USER_BY_ID = "DELETE FROM user WHERE user_id=?;";

    private UserDAOImpl() {
    }

    public static UserDAOImpl getInstance() {
        return instance;
    }

    @Override
    public List<User> findAll() {
        LOG.warn("Unsupported operation.");
        return null;
    }

    @Override
    public User findOne(long id) throws DAOException {
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_ID)
        ) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                UserRole userRole = UserRole.valueOf(resultSet.getString("role_name").toUpperCase());
                String login = resultSet.getString("username");
                String password = resultSet.getString("password");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                return new User(id, login, password, firstName, lastName, userRole);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
        return null;
    }

    @Override
    public int delete(long id) throws DAOException {
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_ID);
        ) {
            preparedStatement.setLong(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public int delete(User entity) {
        return 0;
    }

    @Override
    public int create(User entity) throws DAOException {
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER);
        ) {
            int role = findRoleId(entity.getRole());
            preparedStatement.setInt(1, role);
            preparedStatement.setString(2, entity.getLogin());
            preparedStatement.setString(3, entity.getPassword());
            preparedStatement.setString(4, entity.getFirstName());
            preparedStatement.setString(5, entity.getLastName());
            return preparedStatement.executeUpdate();

        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
    }


    @Override
    public boolean authenticate(String login, String password) {
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CHECK_USER_EXIST);
        ) {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (ConnectionPoolException | SQLException e) {
            LOG.error(e);
        }
        return false;
    }

    @Override
    public User findOne(String username) throws DAOException {
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_USERNAME);
        ) {
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                UserRole userRole = UserRole.valueOf(resultSet.getString("role_name").toUpperCase());
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                long userId = resultSet.getInt("user_id");
                return new User(userId, username, null, firstName, lastName, userRole);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
        return null;
    }

    private int findRoleId(UserRole role) throws DAOException {// FIXME: 12.02.2016 do something
        try (
                ProxyConnection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ROLE_ID);
        ) {
            preparedStatement.setString(1, role.getValue());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(ROLE_ID_COLUMN_NAME);
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        }
        return -1;
    }
}
