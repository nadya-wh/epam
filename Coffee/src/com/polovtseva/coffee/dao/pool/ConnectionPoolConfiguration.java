package com.polovtseva.coffee.dao.pool;

import org.apache.log4j.Logger;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Created by User on 28.01.2016.
 */
public class ConnectionPoolConfiguration {

    static Logger LOG = Logger.getLogger(ConnectionPoolConfiguration.class);

    public static final String DB_USER_NAME;
    public static final String DB_PASSWORD;
    public static final String DB_URL;
    public static final Integer DB_MAX_CONNECTIONS;

    static {
        try {
            ResourceBundle resource = ResourceBundle.getBundle("res.database");
            DB_URL = resource.getString("db.url");
            DB_USER_NAME = resource.getString("db.user");
            DB_PASSWORD = resource.getString("db.password");
            DB_MAX_CONNECTIONS = Integer.parseInt(resource.getString("db.poolsize"));
        } catch (MissingResourceException e) {
            LOG.fatal(e);
            throw new RuntimeException();
        }
    }

    private ConnectionPoolConfiguration() {

    }
}
