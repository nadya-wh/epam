package com.polovtseva.coffee.dao.pool;

import com.polovtseva.coffee.dao.pool.exception.ConnectionPoolException;
import org.apache.log4j.Logger;

import javax.annotation.PreDestroy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by User on 27.01.2016.
 */
public class ConnectionPool {

    final static Logger LOG = Logger.getLogger(ConnectionPool.class);
    private static BlockingQueue<ProxyConnection> blockingQueue;

    private static ConnectionPool instance;
    private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
    private static AtomicInteger connectionsCreated = new AtomicInteger(0);
    private static AtomicBoolean poolReleased = new AtomicBoolean(false);

    private static ReentrantLock lock = new ReentrantLock();


    private ConnectionPool() {
        initializeConnectionPool();
    }

    /**
     * Returns connection pool instance to access database.
     * @return connection pool instance
     */
    public static ConnectionPool getInstance() {
        if (!instanceCreated.get()) {
            lock.lock();
            if (instance == null) {
                instance = new ConnectionPool();
                instanceCreated.set(true);
            }
            lock.unlock();
        }

        return instance;
    }

    private ProxyConnection makeConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(ConnectionPoolConfiguration.DB_URL,
                ConnectionPoolConfiguration.DB_USER_NAME,
                ConnectionPoolConfiguration.DB_PASSWORD);
        return new ProxyConnection(connection);
    }

    private void initializeConnectionPool() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (SQLException e) {
            LOG.fatal(e);
            throw new RuntimeException(e);
        }
        blockingQueue = new ArrayBlockingQueue<>(ConnectionPoolConfiguration.DB_MAX_CONNECTIONS);

    }

    /**
     * Returns a connection to database.
     * @return Connection to database.
     * @throws ConnectionPoolException Exception is thrown, if pool was release or SQLException occurred.
     */
    public ProxyConnection getConnection() throws ConnectionPoolException { // TODO: 12.02.2016 refactoring
        ProxyConnection connection = null;
        try {
            if (!poolReleased.get()) {
                if (blockingQueue.size() == 0 &&
                        connectionsCreated.get() < ConnectionPoolConfiguration.DB_MAX_CONNECTIONS) {

                    connection = makeConnection();
                    connectionsCreated.incrementAndGet();
                } else {
                    connection = blockingQueue.take();
                }
            } else {
                throw new ConnectionPoolException("Pool was released.");
            }
        } catch (InterruptedException e) {
            LOG.error(e);
        } catch (SQLException e) {
            throw new ConnectionPoolException(e);
        }
        return connection;
    }


    /**
     * This method puts connection int connection pool.
     * @param connection The connection that was taken from pool.
     */
    public void returnConnection(ProxyConnection connection) {
        if (connection != null) {
            blockingQueue.offer(connection);
        }
    }

    /**
     * Releases all connections in connection pool.
     */
    @PreDestroy
    public void releasePool() {
        LOG.info("release connection pool");
        poolReleased.set(true);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            LOG.error(e);
        }
        while (blockingQueue.size() != 0) {
            try {
                blockingQueue.take().destroy();
            } catch (InterruptedException | SQLException e) {
                LOG.error(e);
            }
        }

    }
}