package com.polovtseva.coffee.dao;

import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Account;

/**
 * Created by User on 12.02.2016.
 */
public interface AccountDAO extends GenericDAO<Account> {
    /**
     * Reads account by id.
     * @param userId user's id.
     * @return read account.
     * @throws DAOException if a database access error occurs.
     */
    Account findOneByUserId(long userId) throws DAOException;

    /**
     * Updates user's balance.
     * @param entity user.
     * @param newBalance new value for balance.
     * @throws DAOException if a database access error occurs.
     */
    int updateBalance(Account entity, double newBalance) throws DAOException;
}
