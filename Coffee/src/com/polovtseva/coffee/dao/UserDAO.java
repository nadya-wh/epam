package com.polovtseva.coffee.dao;

import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.User;

/**
 * Created by User on 12.02.2016.
 */
public interface UserDAO extends GenericDAO<User> {

    /**
     * Authenticates user.
     * @param login user's login.
     * @param password user's password.
     * @return true, if user with such credentials exists, false otherwise.
     */
    boolean authenticate(String login, String password);

    /**
     * Reades a user with specified username.
     * @param username
     * @return read user.
     * @throws DAOException DAOException DAOException if a database access error occurs.
     */
    User findOne(String username) throws DAOException;

}
