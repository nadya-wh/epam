package com.polovtseva.coffee.dao;

import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 12.02.2016.
 */
public interface OrderDAO extends GenericDAO<Order> {

    /**
     * Reads all user's orders.
     *
     * @param userId user;s id.
     * @return all user's orders.
     * @throws DAOException if a database access error occurs.
     */
    ArrayList<Order> findAllForUser(long userId) throws DAOException;

    /**
     * Inserts history information.
     *
     * @param order order to insert.
     * @return for each order either (1) the row count for SQL Data Manipulation Language (DML) statements or
     * (2) 0 for SQL statements that return nothing.
     * @throws DAOException if a database access error occurs.
     */
    //int[] createOrderDrinkIngredient(Order order) throws DAOException;

    /**
     * Inserts history information.
     *
     * @param order to insert.
     * @return for each order either (1) the row count for SQL Data Manipulation Language (DML) statements or
     * (2) 0 for SQL statements that return nothing.
     * @throws DAOException if a database access error occurs.
     */
    //int[] createOrderDrink(Order order) throws DAOException;


    /**
     * Reads specified entity.
     *
     * @param userId user'ss id.
     * @param date   order's date and time.
     * @return read order.
     * @throws DAOException if a database access error occurs.
     */
    long findOne(long userId, LocalDateTime date) throws DAOException;


    ArrayList<Order> findAll(boolean approved) throws DAOException;

    ArrayList<Order> findAll(long userId, boolean approved) throws DAOException;

    int[] approve(long[] orderIds) throws DAOException;

}
