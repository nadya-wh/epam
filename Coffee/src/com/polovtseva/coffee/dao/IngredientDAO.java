package com.polovtseva.coffee.dao;

import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Ingredient;

import java.util.List;

/**
 * Created by User on 12.02.2016.
 */
public interface IngredientDAO extends GenericDAO<Ingredient> {

    /**
     * Updates number of portions.
     * @param ingredientId ingredient's id.
     * @param portionsNumber new number of portions.
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or
     * (2) 0 for SQL statements that return nothing.
     * @throws DAOException if a database access error occurs.
     */
    int updatePortions(long ingredientId, int portionsNumber) throws DAOException;

    /**
     * Reads all ingredients in specified order and drink.
     * @param orderId order's id.
     * @param drinkId drink's id.
     * @return list of ingredients.
     * @throws DAOException if a database access error occurs.
     */
    List<Ingredient> findAll(long orderId, long drinkId) throws DAOException;

    /**
     * Counts how many times an ingredient was ordered.
     * @param ingredientId ingredient's id.
     * @return how many times an ingredient was ordered.
     * @throws DAOException
     */
    int countIngredientInOrders(long ingredientId) throws DAOException;
}
