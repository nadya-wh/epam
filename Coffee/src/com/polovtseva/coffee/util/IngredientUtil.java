package com.polovtseva.coffee.util;

import com.polovtseva.coffee.domain.Ingredient;

import java.util.List;

/**
 * Created by User on 09.02.2016.
 */
public class IngredientUtil {
    public static boolean containsIngredient(List<Ingredient> ingredients, int ingredientId) {
        for (Ingredient ingredient : ingredients) {
            if (ingredient.getId() == ingredientId) {
                return true;
            }
        }
        return false;
    }
}
