package com.polovtseva.coffee.util;

import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.Order;

import java.util.List;

/**
 * Created by User on 09.02.2016.
 */
public class OrderUtil {
    public static Drink findDrink(int drinkId, int[] ingredientIds, int drinksCount, Order order) {
        List<Drink> drinks = order.getDrinks();
        for (Drink drink : drinks) {
            if (drink.getId() == drinkId && drink.getNumberInOrder() == drinksCount &&
                    drink.getIngredients().size() == ingredientIds.length) {
                List<Ingredient> ingredients = drink.getIngredients();
                for (int i = 0; i < ingredientIds.length; i++) {
                    if (!IngredientUtil.containsIngredient(ingredients, ingredientIds[i])) {
                        break;
                    }
                }
                return drink;
            }
        }
        return null;
    }

    public static Drink findDrink(int drinkId, int drinksCount, Order order) {
        List<Drink> drinks = order.getDrinks();
        for (Drink drink : drinks) {
            if (drink.getId() == drinkId && drink.getNumberInOrder() == drinksCount &&
                    drink.getIngredients().size() == 0) {
                return drink;
            }
        }
        return null;
    }

    public static double countSum(Order order) {
        double totalSum = 0;
        if (order != null) {
            for (Drink drink : order.getDrinks()) {
                totalSum += drink.getDrinkPrice() * drink.getNumberInOrder();
                for (Ingredient ingredient : drink.getIngredients()) {
                    totalSum += ingredient.getIngredientPrice() * ingredient.getNumberInDrink() * drink.getNumberInOrder();
                }
            }
        }
        return totalSum;
    }
}
