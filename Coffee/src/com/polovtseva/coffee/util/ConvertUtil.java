package com.polovtseva.coffee.util;

/**
 * Created by User on 09.02.2016.
 */
public class ConvertUtil {
    public static int[] castStringArrayToIntArray(String[] values) {
        int[] res = new int[values.length];
        for (int i = 0; i < values.length; i++) {
            res[i] = Integer.parseInt(values[i]);
        }
        return res;
    }
}
