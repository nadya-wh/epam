package com.polovtseva.coffee.util;

import java.util.ResourceBundle;

/**
 * Created by User on 27.01.2016.
 */
public class PagesConfigurationManager {

    public static final String LOGIN_PAGE_KEY = "path.page.login";
    public static final String MAIN_PAGE_KEY = "path.page.main";
    public static final String ERROR_PAGE_KEY = "path.page.error";
    public static final String INDEX_PAGE_KEY = "path.page.index";
    public static final String SIGN_UP_PAGE_KEY = "path.page.signup";
    public static final String MENU_PAGE_KEY = "path.page.menu";
    public static final String HISTORY_PAGE_KEY = "path.page.history";
    public static final String ADD_DRINK_PAGE_KEY = "path.page.adddrink";
    public static final String ADD_INGREDIENT_PAGE_KEY = "path.page.addingredient";
    public static final String INGREDIENTS_PAGE_KEY = "path.page.ingredients";
    public static final String CURRENT_ORDER_PAGE_KEY = "path.page.currentorder";

    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("res\\pages");

    private PagesConfigurationManager() {
    }

    public static String getProperty(String key) {
        return resourceBundle.getString(key);
    }
}
