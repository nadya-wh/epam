package com.polovtseva.coffee.util;

import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.domain.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by User on 27.01.2016.
 */
public class Validator {
    public static final String LOGIN_REGEX = "^[a-zA-Z0-9_-]{6,15}$";
    public static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.])(?=\\S+$).{8,}$";
    public static final String NAME_REGEX = "\\p{javaLetterOrDigit}+";
    public static final String URL_REGEX = "((@)?(href=')?(HREF=')?(HREF=\")?(href=\")?(http://)?[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?)";

    public static final int MAX_URL_LENGTH = 255;

    public static boolean validateLogin(String login) {
        if (login != null) {
            Pattern pattern = Pattern.compile(LOGIN_REGEX);
            return pattern.matcher(login).matches();
        } else {
            return false;
        }
    }

    public static boolean validatePassword(String password) {
        if (password == null) {
            return false;
        }
        Pattern pattern = Pattern.compile(PASSWORD_REGEX);
        return pattern.matcher(password).matches();
    }

    public static boolean validateName(String name) {
        if (name == null) {
            return false;
        }
        Pattern pattern = Pattern.compile(NAME_REGEX);
        return pattern.matcher(name).matches();
    }

    public static boolean validatePortionsNumber(int num) {
        return num >= 0;
    }

    public static boolean validateUrl(String url) {
        if (url == null) {
            return true;
        }
        Pattern pattern = Pattern.compile(URL_REGEX);
        if (!pattern.matcher(url).matches()) {
            return false;
        }
        return url.length() < MAX_URL_LENGTH;
    }

    public static boolean validatePrice(double price) {
        return price >= 0;
    }

    public static boolean validateUser(User user) {
        return Validator.validateLogin(user.getLogin()) &&
                Validator.validatePassword(user.getPassword()) &&
                Validator.validateName(user.getFirstName()) &&
                Validator.validateName(user.getLastName());
    }

    public static final boolean validateIngredient(Ingredient ingredient) {
        return Validator.validateName(ingredient.getIngredientName()) &&
                Validator.validatePortionsNumber(ingredient.getPortionsLeft()) &&
                Validator.validatePrice(ingredient.getIngredientPrice()) &&
                Validator.validateUrl(ingredient.getImageUrl());
    }


}
