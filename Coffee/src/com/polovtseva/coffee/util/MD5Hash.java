package com.polovtseva.coffee.util;

import org.apache.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by User on 29.01.2016.
 */
public class MD5Hash {

    final static Logger LOG = Logger.getLogger(MD5Hash.class);
    public static final String ALGORITHM = "MD5";
    public static final String CHARSET_NAME = "UTF-8";

    public static String countHash(String password) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance(ALGORITHM);
            byte[] digest = messageDigest.digest(password.getBytes(CHARSET_NAME));
            BigInteger bigInt = new BigInteger(1, digest);
            return bigInt.toString(16);

        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            LOG.error(e);
        }
        return password; // TODO: 29.01.2016 or throw Exception?
    }
}
