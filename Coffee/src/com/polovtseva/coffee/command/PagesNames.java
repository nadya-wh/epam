package com.polovtseva.coffee.command;

/**
 * Created by User on 22.02.2016.
 */
public class PagesNames {
    public static final String LOGIN_PAGE_KEY = "/jsp/login.jsp";
    public static final String MAIN_PAGE_KEY = "/jsp/main.jsp";
    public static final String ERROR_PAGE_KEY = "/jsp/error/error.jsp";
    public static final String INDEX_PAGE_KEY = "/index.jsp";
    public static final String SIGN_UP_PAGE_KEY = "/jsp/signupform.jsp";
    public static final String MENU_PAGE_KEY = "/jsp/menu.jsp";
    public static final String HISTORY_PAGE_KEY = "/jsp/history.jsp";
    public static final String ADD_DRINK_PAGE_KEY = "/jsp/adddrink.jsp";
    public static final String ADD_INGREDIENT_PAGE_KEY = "/jsp/addingredient.jsp";
    public static final String INGREDIENTS_PAGE_KEY = "/jsp/ingredients.jsp";
    public static final String CURRENT_ORDER_PAGE_KEY = "/jsp/currentorder.jsp";
    public static final String DRINKS_URL = "/coffee/drinks";
    public static final String TECH_ERROR_PAGE_KEY = "/error/techerror.jsp";
    public static final String SUCCESSFUL_ORDER_PAGE_KEY = "/jsp/successfullorder.jsp";
    public static final String MAIN_URL = "/main";
    public static final String MENU_INGREDIENTS_URL = "/coffee/ingredients";
    public static final String ORDERS_PAGE_KEY = "/jsp/orders.jsp";
    public static final String SIGN_UP_URL = "/signup";
    public static final String SIGN_IN_URL = "/signin";
}
