package com.polovtseva.coffee.command;


import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 27.01.2016.
 */
public class CommandFactory {

    public static String COMMAND_PARAM = "command";
    public static String WRONG_ACTION_ATTRIBUTE_NAME = "error";

    public static Command defineCommand(HttpServletRequest request) {
        String commandName = request.getParameter(COMMAND_PARAM);
        Command currentCommand = new EmptyCommand();
        if (commandName != null) {
            try {
                CommandEnum commandEnum = CommandEnum.valueOf(commandName.toUpperCase());
                currentCommand = commandEnum.getCommand();
            } catch (IllegalArgumentException e) {
                request.setAttribute(WRONG_ACTION_ATTRIBUTE_NAME, true);
            }
        }
        return currentCommand;
    }

}
