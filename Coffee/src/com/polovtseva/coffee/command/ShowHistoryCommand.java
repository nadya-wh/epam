package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.OrderDAOImpl;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.OrderServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by User on 04.02.2016.
 */
public class ShowHistoryCommand implements Command {
    static final Logger LOG = Logger.getLogger(ShowHistoryCommand.class);

    public static final String USER_ID_ATTR = "userId";
    public static final String ORDERS_ATTR = "orders";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public ShowHistoryCommand() {

    }

    @Override
    public String execute(HttpServletRequest request) {

        String page = null;
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_HISTORY.toString());
        if (request.getSession().getAttribute(USER_ID_ATTR) != null) {
            int userId = Integer.parseInt(request.getSession().getAttribute(USER_ID_ATTR).toString());
            OrderService orderService = new OrderServiceImpl(OrderDAOImpl.getInstance());
            try {
                List<Order> orders = orderService.findAll(userId);
                request.setAttribute(ORDERS_ATTR, orders);
                page = PagesNames.HISTORY_PAGE_KEY;
            } catch (ServiceException e) {
                LOG.error(e);
                page = PagesNames.TECH_ERROR_PAGE_KEY;
            }
        }
        return page;
    }
}
