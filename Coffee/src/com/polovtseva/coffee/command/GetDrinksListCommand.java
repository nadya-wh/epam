package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by User on 04.02.2016.
 */
public class GetDrinksListCommand implements Command {

    static final Logger LOG = Logger.getLogger(GetDrinksListCommand.class);
    public static final String DRINKS_ATTR = "drinks";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public GetDrinksListCommand() {

    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        String page;
        DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
        try {
            List<Drink> drinks = drinkService.findAllDrinks();
            request.setAttribute(DRINKS_ATTR, drinks);
            page = PagesNames.MENU_PAGE_KEY;
        } catch (ServiceException e) {
            LOG.error(e);
            page = PagesNames.ERROR_PAGE_KEY;
            return page;
        }

        return page;
    }
}
