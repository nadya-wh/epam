package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 07.02.2016.
 */
public class DeleteDrinkCommand implements Command {

    static final Logger LOG = Logger.getLogger(DeleteDrinkCommand.class);

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String DELETE_ERROR_ATTR = "deleteError";
    public static final String DELETE_SUCCESS_ATTR = "deleteSuccess";
    public static final String DRINKS_ATTR = "drinks";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public DeleteDrinkCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        String page = PagesNames.MENU_PAGE_KEY;
        int drinkId = Integer.parseInt(request.getParameter(DRINK_ID_PARAM));
        DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());

        Drink chosenDrink;
        try {
            chosenDrink = drinkService.find(drinkId);
            if (drinkService.delete(chosenDrink.getId())) {
                request.setAttribute(DELETE_SUCCESS_ATTR, true);
                request.setAttribute(DRINKS_ATTR, drinkService.findAllDrinks());
            } else {
                request.setAttribute(DELETE_ERROR_ATTR, true);
                request.setAttribute(DRINKS_ATTR, drinkService.findAllDrinks());
            }
        } catch (ServiceException e) {
            LOG.error(e);
            request.setAttribute(DELETE_ERROR_ATTR, false);
        }

        return page;
    }
}
