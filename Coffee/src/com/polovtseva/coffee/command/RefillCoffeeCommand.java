package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 07.02.2016.
 */
public class RefillCoffeeCommand implements Command {

    static final Logger LOG = Logger.getLogger(RefillCoffeeCommand.class);

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String DRINKS_NUMBER_PARAM = "drinksNumber";
    public static final String DRINKS_ATTR = "drinks";
    public static final String REFILL_ERROR_ATTR = "refillError";
    public static final String REFILL_SUCCESS_ATTR = "refillSuccess";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public RefillCoffeeCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        String page = PagesNames.MENU_PAGE_KEY;
        request.setAttribute(REFILL_ERROR_ATTR, null);
        request.setAttribute(REFILL_SUCCESS_ATTR, null);
        try {
            int drinkId = Integer.parseInt(request.getParameter(DRINK_ID_PARAM));
            DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
            int drinkCount = Integer.parseInt(request.getParameter(DRINKS_NUMBER_PARAM));
            Drink chosenDrink = drinkService.find(drinkId);
            int portionsLeft =  chosenDrink.getPortionsLeft() + drinkCount;
            if (drinkCount > 0 && portionsLeft > chosenDrink.getPortionsLeft()) {
                drinkService.updatePortionsLeft(chosenDrink.getId(), chosenDrink.getPortionsLeft() + drinkCount);
                request.setAttribute(DRINKS_ATTR, drinkService.findAllDrinks());
                request.setAttribute(REFILL_SUCCESS_ATTR, true);
            } else {
                request.setAttribute(REFILL_ERROR_ATTR, true);
                return page;
            }
        } catch (NumberFormatException | ServiceException e) {
            LOG.error(e);
            request.setAttribute(REFILL_ERROR_ATTR, true);
        }
        return page;
    }
}
