package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.UserDAOImpl;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;
import com.polovtseva.coffee.service.UserService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.UserServiceImpl;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * Created by User on 27.01.2016.
 */
public class SignUpCommand implements Command {

    static final Logger LOG = Logger.getLogger(SignUpCommand.class);

    private static final String LOGIN_PARAM_NAME = "username";
    private static final String PASSWORD_PARAM_NAME = "password";
    private static final String CONFIRM_PASSWORD_PARAM_NAME = "confirm-password";
    private static final String FIRST_NAME_PARAM_NAME = "firstname";
    private static final String LAST_NAME_PARAM_NAME = "lastname";
    public static final String ERROR_ATTR = "error";
    public static final String CONFIRM_PASSWORD_ERROR = "confirmError";

    public SignUpCommand() {

    }


    @Override
    public String execute(HttpServletRequest request) {
        String page = PagesNames.SIGN_UP_URL;
        String username = request.getParameter(LOGIN_PARAM_NAME);
        String password = request.getParameter(PASSWORD_PARAM_NAME);
        String confirmPassword = request.getParameter(CONFIRM_PASSWORD_PARAM_NAME);
        String firstName = request.getParameter(FIRST_NAME_PARAM_NAME);
        String lastName = request.getParameter(LAST_NAME_PARAM_NAME);
        if (!password.equals(confirmPassword)) {
            request.setAttribute(CONFIRM_PASSWORD_ERROR, true);
            return page;
        }
        User user = new User(username, password, firstName, lastName, UserRole.USER);

        // TODO: 21.02.2016 check
        UserService userService = new UserServiceImpl(UserDAOImpl.getInstance());
        try {
            if (userService.create(user)) {
                page = PagesNames.LOGIN_PAGE_KEY;
            } else {
                request.setAttribute(ERROR_ATTR, true);
                return page;
            }
        } catch (ServiceException e) {
            LOG.error(e);
            request.setAttribute(ERROR_ATTR, true);
            return PagesNames.TECH_ERROR_PAGE_KEY;
        }
        return page;
    }
}
