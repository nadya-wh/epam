package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 05.02.2016.
 */
public class AddDrinkCommand implements Command {

    static final Logger LOG = Logger.getLogger(AddDrinkCommand.class);

    public static final String DRINK_NAME_PARAM = "drinkName";
    public static final String DRINK_PRICE_PARAM = "drinkPrice";
    public static final String IMAGE_URL_PARAM = "imageUrl";
    public static final String PORTIONS_LEFT_PARAM = "portionsLeft";
    public static final String CREATE_DRINK_ERROR_ATTR = "error";
    public static final String CREATE_DRINK_SUCCESS_ATTR = "success";
    public static final String VALID_ERROR = "validationError";
    public static final String PREV_PAGE = "prevPage";

    public AddDrinkCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        String page = PagesNames.ADD_DRINK_PAGE_KEY;
        request.getSession().setAttribute(PREV_PAGE, PagesNames.DRINKS_URL);
        String drinkName = request.getParameter(DRINK_NAME_PARAM);
        String imageUrl = request.getParameter(IMAGE_URL_PARAM);
        int portionsLeft;
        double drinkPrice;
        try {
            portionsLeft = Integer.parseInt(request.getParameter(PORTIONS_LEFT_PARAM));
            drinkPrice = Double.parseDouble(request.getParameter(DRINK_PRICE_PARAM));
        } catch (NumberFormatException e) {
            LOG.error(e);
            request.setAttribute(VALID_ERROR, true);
            return page;
        }
        Drink drink = new Drink(0, drinkName, portionsLeft, drinkPrice, imageUrl);
        DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
        try {
            if (drinkService.create(drink)) {
                request.setAttribute(CREATE_DRINK_SUCCESS_ATTR, true);
            } else {
                request.setAttribute(CREATE_DRINK_ERROR_ATTR, true);
            }
        } catch (ServiceException e) {
            page = PagesNames.TECH_ERROR_PAGE_KEY;
            LOG.error(e);
        }
        return page;

    }
}
