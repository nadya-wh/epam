package com.polovtseva.coffee.command;

import com.polovtseva.coffee.util.PagesConfigurationManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 27.01.2016.
 */
public class EmptyCommand implements Command {

    public static final String CURRENT_PAGE = "currentPage";

    public EmptyCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(CURRENT_PAGE) != null) {
            return request.getSession().getAttribute(CURRENT_PAGE).toString();
        } else {
            return PagesConfigurationManager.getProperty(PagesConfigurationManager.LOGIN_PAGE_KEY);
        }
    }
}
