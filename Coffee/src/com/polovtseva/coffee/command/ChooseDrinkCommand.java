package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 06.02.2016.
 */
public class ChooseDrinkCommand implements Command {

    static final Logger LOG = Logger.getLogger(ChooseDrinkCommand.class);

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String USER_ID_ATTR = "userId";
    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String CURRENT_DRINK_ATTR = "currentDrink";
    public static final String DRINKS_NUMBER_PARAM = "drinksNumber";
    public static final String DRINKS_ATTR = "drinks";
    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String ADD_TO_ORDER_ERROR_ATTR = "error";
    public static final String ADD_TO_ORDER_SUCCESS_ATTR = "success";
    public static final String ADD_DRINK_TO_ORDER_PARAM = "addDrinkToOrder";
    public static final String ADD_INGREDIENT_PARAM = "addIngredient";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public ChooseDrinkCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        String page = PagesNames.MENU_PAGE_KEY;
        int drinkId = Integer.parseInt(request.getParameter(DRINK_ID_PARAM));
        Order order;
        DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
        if((request.getSession().getAttribute(USER_ID_ATTR) == null)) {
            return PagesNames.LOGIN_PAGE_KEY;
        }
        try {
            int drinkCount = Integer.parseInt(request.getParameter(DRINKS_NUMBER_PARAM));
            Drink chosenDrink = drinkService.find(drinkId);
            chosenDrink.setNumberInOrder(drinkCount);
            if (request.getParameter(ADD_DRINK_TO_ORDER_PARAM) != null) {
                if (request.getSession().getAttribute(CURRENT_ORDER_ATTR) == null) {
                    int userId = Integer.parseInt(request.getSession().getAttribute(USER_ID_ATTR).toString());
                    order = new Order(userId);
                } else {
                    order = (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
                }
                order.addDrink(chosenDrink);
                request.setAttribute(DRINKS_ATTR, drinkService.findAllDrinks());
                request.getSession().setAttribute(CURRENT_ORDER_ATTR, order);
                request.setAttribute(ADD_TO_ORDER_SUCCESS_ATTR, true);
            } else if (request.getParameter(ADD_INGREDIENT_PARAM) != null) {
                request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
                request.getSession().setAttribute(CURRENT_DRINK_ATTR, chosenDrink);
                IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
                page = PagesNames.INGREDIENTS_PAGE_KEY;
                request.setAttribute(INGREDIENTS_ATTR, ingredientService.findAll());
            }
        } catch (ServiceException e) {
            LOG.error(e);
            request.setAttribute(ADD_TO_ORDER_ERROR_ATTR, true);
        }
        return page;
    }
}
