package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.AccountDAOImpl;
import com.polovtseva.coffee.dao.impl.OrderDAOImpl;
import com.polovtseva.coffee.domain.Account;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.AccountService;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.AccountServiceImpl;
import com.polovtseva.coffee.service.impl.OrderServiceImpl;
import com.polovtseva.coffee.util.OrderUtil;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 09.02.2016.
 */
public class OrderCommand implements Command {

    static final Logger LOG = Logger.getLogger(OrderCommand.class);

    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String USER_ID_ATTR = "userId";
    public static final String ERROR_ATTR = "error";
    public static final String TOTAL_SUM_ATTR = "totalSum";

    public OrderCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(CURRENT_ORDER_ATTR) != null) {
            Order order = (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
            double totalSum = OrderUtil.countSum(order);
            int userId = Integer.parseInt(request.getSession().getAttribute(USER_ID_ATTR).toString());
            AccountService accountService = new AccountServiceImpl(AccountDAOImpl.getInstance());
            try {
                Account account = accountService.findEntityByUserId(userId);
                if (accountService.withdrawMoney(account, totalSum)) {
                    request.getSession().setAttribute(CURRENT_ORDER_ATTR, null);
                    OrderService orderService = new OrderServiceImpl(OrderDAOImpl.getInstance());
                    orderService.createOrder(order);
                    return PagesNames.SUCCESSFUL_ORDER_PAGE_KEY;
                } else {
                    request.setAttribute(ERROR_ATTR, true);
                    request.setAttribute(TOTAL_SUM_ATTR, totalSum);
                }

            } catch (ServiceException e) {
                LOG.error(e);
            }
        }
        return PagesNames.CURRENT_ORDER_PAGE_KEY;
    }
}
