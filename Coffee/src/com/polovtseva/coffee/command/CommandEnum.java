package com.polovtseva.coffee.command;

/**
 * Created by User on 27.01.2016.
 */
public enum CommandEnum {
    LOGIN {
        {
            this.command = new LoginCommand();
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    SIGN_UP {
        {
            this.command = new SignUpCommand();
        }

    },
    GET_DRINKS_LIST {
        {
            this.command = new GetDrinksListCommand();
        }
    },
    SHOW_HISTORY {
        {
            this.command = new ShowHistoryCommand();
        }
    },
    ADD_DRINK {
        {
            this.command = new AddDrinkCommand();
        }
    },
    ADD_INGREDIENT {
        {
            this.command = new AddIngredientCommand();
        }
    },
    ADD_INGREDIENT_TO_DRINK {
        {
            this.command = new ChooseIngredientCommand();
        }
    },
    CHOOSE_DRINK {
        {
            this.command = new ChooseDrinkCommand();
        }
    },
    GET_INGREDIENTS_LIST {
        {
            this.command = new GetIngredientsListCommand();
        }
    },
    REFILL_COFFEE {
        {
            this.command = new RefillCoffeeCommand();
        }
    },
    REFILL_INGREDIENTS {
        {
            this.command = new RefillIIngredientsCommand();
        }
    },
    DELETE_DRINK {
        {
            this.command = new DeleteDrinkCommand();
        }
    },
    DELETE_INGREDIENT {
        {
            this.command = new DeleteIngredientCommand();
        }
    },
    ADD_DRINK_TO_ORDER {
        {
            this.command = new AddDrinkToOrderCommand();
        }
    },
    SHOW_CURRENT_ORDER {
        {
            this.command = new ShowCurrentOrderCommand();
        }
    },
    ORDER {
        {
            this.command = new OrderCommand();
        }
    },
    REMOVE_DRINK_FROM_ORDER {
        {
            this.command = new RemoveDrinkFromOrderCommand();
        }
    },
    CHANGE_LANGUAGE {
        {
            this.command = new ChangeLanguageCommand();
        }
    },
    SHOW_NOT_APPROVED_ORDERS {
        {
            this.command = new ShowNotApprovedOrdersCommand();
        }
    },
    APPROVE_ORDER {
        {
            this.command = new ApproveOrderCommand();
        }
    },
    SHOW_WAITING_LIST {
        {
            this.command = new ShowWaitingListCommand();
        }
    }
    ;

    Command command;

    public Command getCommand() {
        return command;
    }
}
