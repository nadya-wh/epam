package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by User on 06.02.2016.
 */
public class GetIngredientsListCommand implements Command {

    static final Logger LOG = Logger.getLogger(GetIngredientsListCommand.class);

    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public GetIngredientsListCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        String page = PagesNames.INGREDIENTS_PAGE_KEY;
        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {
            List<Ingredient> ingredients = ingredientService.findAll();
            request.setAttribute(INGREDIENTS_ATTR, ingredients);
        } catch (ServiceException e) {
            LOG.error(e);
            page = PagesNames.ERROR_PAGE_KEY;
        }
        return page;
    }
}
