package com.polovtseva.coffee.command;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * Created by User on 25.01.2016.
 */
//command decides which page to call next
public interface Command {
    String execute(HttpServletRequest request);

}
