package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import com.polovtseva.coffee.util.PagesConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 08.02.2016.
 */
public class DeleteIngredientCommand implements Command {

    static final Logger LOG = Logger.getLogger(DeleteIngredientCommand.class);

    public static final String INGREDIENT_ID_PARAM = "ingredientId";
    public static final String DELETE_ERROR_ATTR = "deleteError";
    public static final String DELETE_SUCCESS_ATTR = "deleteSuccess";
    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public DeleteIngredientCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        String page = PagesNames.INGREDIENTS_PAGE_KEY;
        int ingredientId = Integer.parseInt(request.getParameter(INGREDIENT_ID_PARAM));
        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {

            Ingredient ingredient = ingredientService.find(ingredientId);
            if (ingredient != null) {
                ingredientService.delete(ingredientId);
                request.setAttribute(DELETE_SUCCESS_ATTR, true);
            }
            request.setAttribute(INGREDIENTS_ATTR, ingredientService.findAll());
        } catch (ServiceException e) {
            LOG.error(e);
            request.setAttribute(DELETE_ERROR_ATTR, true);
        }
        return page;
    }
}
