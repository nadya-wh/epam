package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * add ingredient to drink
 */
public class ChooseIngredientCommand implements Command {

    static final Logger LOG = Logger.getLogger(ChooseIngredientCommand.class);

    public static final String CURRENT_DRINK_ATTR = "currentDrink";
    public static final String INGREDIENT_ID_PARAM = "ingredientId";
    public static final String INGREDIENT_NUMBER_PARAM = "ingredientsNumber";
    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String ADD_TO_ORDER_ERROR_ATTR = "error";
    public static final String ADD_TO_ORDER_SUCCESS_ATTR = "success";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    public ChooseIngredientCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        String page = PagesNames.INGREDIENTS_PAGE_KEY;
        if (request.getSession().getAttribute(CURRENT_DRINK_ATTR) != null) {
            Drink chosenDrink = (Drink) request.getSession().getAttribute(CURRENT_DRINK_ATTR);
            int ingredientId = Integer.parseInt(request.getParameter(INGREDIENT_ID_PARAM));
            int ingredientNumber = Integer.parseInt(request.getParameter(INGREDIENT_NUMBER_PARAM));
            IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
            try {
                Ingredient chosenIngredient = ingredientService.find(ingredientId);
                chosenIngredient.setNumberInDrink(ingredientNumber);
                chosenDrink.addIngredient(chosenIngredient);
                List<Ingredient> ingredients = ingredientService.findAll();
                request.setAttribute(INGREDIENTS_ATTR, ingredients);
                request.setAttribute(ADD_TO_ORDER_SUCCESS_ATTR, true);
                request.getSession().setAttribute(CURRENT_DRINK_ATTR, chosenDrink);
            } catch (ServiceException e) {
                LOG.error(e);
                request.setAttribute(ADD_TO_ORDER_ERROR_ATTR, true);
            }
        }
        return page;
    }
}
