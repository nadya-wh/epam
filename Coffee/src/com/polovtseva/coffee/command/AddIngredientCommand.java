package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import com.polovtseva.coffee.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 05.02.2016.
 */
public class AddIngredientCommand implements Command {

    static final Logger LOG = Logger.getLogger(AddIngredientCommand.class);

    public static final String INGREDIENT_NAME_PARAM = "ingredientName";
    public static final String INGREDIENT_PRICE_PARAM = "ingredientPrice";
    public static final String IMAGE_URL_PARAM = "imageUrl";
    public static final String PORTIONS_LEFT_PARAM = "portionsLeft";
    public static final String ADD_INGREDIENT_ERROR_ATTR = "error";
    public static final String ADD_INGREDIENT_SUCCESS_ATTR = "success";
    public static final String VALID_ERROR = "validationError";
    public static final String PREV_PAGE = "prevPage";


    public AddIngredientCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_PAGE, PagesNames.MENU_INGREDIENTS_URL);
        String page = PagesNames.ADD_INGREDIENT_PAGE_KEY;
        String ingredientName = request.getParameter(INGREDIENT_NAME_PARAM);
        String imageUrl = request.getParameter(IMAGE_URL_PARAM);
        double ingredientPrice;
        int left;
        try {
            ingredientPrice = Double.parseDouble(request.getParameter(INGREDIENT_PRICE_PARAM));
            left = Integer.parseInt(request.getParameter(PORTIONS_LEFT_PARAM));
        } catch (NumberFormatException e) {
            LOG.error(e);
            request.setAttribute(VALID_ERROR, true);
            return page;
        }
        Ingredient ingredient = new Ingredient(0, ingredientName, ingredientPrice, imageUrl, left);
        if (!Validator.validateIngredient(ingredient)) {
            request.setAttribute(VALID_ERROR, true);
            return page;
        }
        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {
            ingredientService.create(ingredient);
            request.setAttribute(ADD_INGREDIENT_SUCCESS_ATTR, true);
        } catch (ServiceException e) {
            LOG.error(e);
            request.setAttribute(ADD_INGREDIENT_ERROR_ATTR, true);
        }
        return page;
    }
}
