package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by User on 07.02.2016.
 */
public class RefillIIngredientsCommand implements Command {

    static final Logger LOG = Logger.getLogger(RefillIIngredientsCommand.class);

    public static final String INGREDIENT_ID_PARAM = "ingredientId";
    public static final String INGREDIENT_NUMBER_PARAM = "ingredientsNumber";
    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String REFILL_ERROR_ATTR = "refillError";
    public static final String REFILL_SUCCESS_ATTR = "refillSuccess";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public RefillIIngredientsCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        String page = PagesNames.INGREDIENTS_PAGE_KEY;

        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {
            int ingredientId = Integer.parseInt(request.getParameter(INGREDIENT_ID_PARAM));
            Ingredient chosenIngredient = ingredientService.find(ingredientId);

            int ingredientNumber = Integer.parseInt(request.getParameter(INGREDIENT_NUMBER_PARAM));
            int portionsLeft = chosenIngredient.getPortionsLeft() + ingredientNumber;
            if (ingredientNumber > 0 && portionsLeft > chosenIngredient.getPortionsLeft()) {
                ingredientService.updatePortionsLeft(chosenIngredient.getId(),
                        portionsLeft);
            } else {
                request.setAttribute(REFILL_ERROR_ATTR, true);
            }
            List<Ingredient> ingredients = ingredientService.findAll();
            request.setAttribute(INGREDIENTS_ATTR, ingredients);
            request.setAttribute(REFILL_SUCCESS_ATTR, true);
        } catch (NumberFormatException | ServiceException e) {
            LOG.error(e);
            request.setAttribute(REFILL_ERROR_ATTR, true);
        }
        return page;
    }
}
