package com.polovtseva.coffee.command;

import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.util.ConvertUtil;
import com.polovtseva.coffee.util.OrderUtil;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 09.02.2016.
 */
public class RemoveDrinkFromOrderCommand implements Command {

    static final Logger LOG = Logger.getLogger(RemoveDrinkFromOrderCommand.class);

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String COUNT_IN_ORDER_PARAM = "numberInOrder";
    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String INGREDIENT_ID_PARAM = "ingredientId";
    public static final String TOTAL_SUM_PARAM = "totalSum";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    public RemoveDrinkFromOrderCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_CURRENT_ORDER.toString());
        int drinkId = Integer.parseInt(request.getParameter(DRINK_ID_PARAM));
        int numberInOrder = Integer.parseInt(request.getParameter(COUNT_IN_ORDER_PARAM));
        Order order = (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
        Drink drinkToRemove;
        if (request.getParameterValues(INGREDIENT_ID_PARAM) != null) {
            int[] ingredientIds = ConvertUtil.castStringArrayToIntArray(request.getParameterValues(INGREDIENT_ID_PARAM));
            drinkToRemove = OrderUtil.findDrink(drinkId, ingredientIds, numberInOrder, order);
        } else {
            drinkToRemove = OrderUtil.findDrink(drinkId, numberInOrder, order);
        }
        if (drinkToRemove != null) {
            order.removeDrink(drinkToRemove);
        }
        request.setAttribute(TOTAL_SUM_PARAM, OrderUtil.countSum(order));
        request.getSession().setAttribute(CURRENT_ORDER_ATTR, order);

        return PagesNames.CURRENT_ORDER_PAGE_KEY;
    }
}
