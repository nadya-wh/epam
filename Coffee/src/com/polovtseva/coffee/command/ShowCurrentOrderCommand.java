package com.polovtseva.coffee.command;

import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.util.OrderUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 09.02.2016.
 */
public class ShowCurrentOrderCommand implements Command {

    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String TOTAL_SUM_ATTR = "totalSum";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public ShowCurrentOrderCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_CURRENT_ORDER.toString());
        String page = PagesNames.CURRENT_ORDER_PAGE_KEY;
        if (request.getSession().getAttribute(CURRENT_ORDER_ATTR) != null) {
            Order currentOrder = (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
            double totalSum = OrderUtil.countSum(currentOrder);
            request.setAttribute(TOTAL_SUM_ATTR, totalSum);
        }
        return page;
    }
}
