package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import com.polovtseva.coffee.util.PagesConfigurationManager;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 09.02.2016.
 */
public class AddDrinkToOrderCommand implements Command {

    static final Logger LOG = Logger.getLogger(AddDrinkToOrderCommand.class);

    public static final String CURRENT_DRINK_ATTR = "currentDrink";
    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String USER_ID_ATTR = "userId";
    public static final String DRINKS_ATTR = "drinks";
    public static final String ADD_TO_ORDER_SUCCESS_ATTR = "success";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    public AddDrinkToOrderCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        Order order;
        String page = PagesConfigurationManager.getProperty(PagesConfigurationManager.INGREDIENTS_PAGE_KEY);
        if (request.getSession().getAttribute(CURRENT_DRINK_ATTR) != null) {
            Drink drink = (Drink) request.getSession().getAttribute(CURRENT_DRINK_ATTR);
            if (request.getSession().getAttribute(CURRENT_ORDER_ATTR) == null) {
                int userId = Integer.parseInt(request.getSession().getAttribute(USER_ID_ATTR).toString());
                order = new Order(userId);
            } else {
                order = (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
            }
            order.addDrink(drink);
            request.setAttribute(ADD_TO_ORDER_SUCCESS_ATTR, true);
            request.getSession().setAttribute(CURRENT_ORDER_ATTR, order);
            request.getSession().setAttribute(CURRENT_DRINK_ATTR, null);
            page = PagesConfigurationManager.getProperty(PagesConfigurationManager.MENU_PAGE_KEY);
            DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
            try {
                request.setAttribute(DRINKS_ATTR, drinkService.findAllDrinks());
            } catch (ServiceException e) {
                LOG.error(e);
            }
        }
        return page;
    }
}
