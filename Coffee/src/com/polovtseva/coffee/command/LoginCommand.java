package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.UserDAOImpl;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.service.UserService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.UserServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 27.01.2016.
 */
public class LoginCommand implements Command {

    final static Logger LOG = Logger.getLogger(LoginCommand.class);

    public static final String LOGIN_PARAM_NAME = "username";
    public static final String PASSWORD_PARAM_NAME = "password";
    public static final String ERROR_LOGIN_PASS_ATTR = "errorLoginPasswordMessage";
    public static final String USER_ID_ATTR = "userId";
    public static final String USERNAME_ATTR = "username";
    public static final String FIRST_NAME_ATTR = "firstName";
    public static final String LAST_NAME_ATTR = "lastName";
    public static final String ROLE_ATTR = "role";

    public LoginCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        String username = request.getParameter(LOGIN_PARAM_NAME);
        String password = request.getParameter(PASSWORD_PARAM_NAME);
        User user = new User();
        user.setLogin(username);
        user.setPassword(password);
        String page = PagesNames.LOGIN_PAGE_KEY;
        UserService userService = new UserServiceImpl(UserDAOImpl.getInstance());
        try {
            user = userService.authorisation(username, password);
        } catch (ServiceException e) {
            LOG.error(e);
            return page;
        }
        if (user != null) {
            request.getSession().setAttribute(USER_ID_ATTR, user.getId());
            request.getSession().setAttribute(USERNAME_ATTR, user.getLogin());
            request.getSession().setAttribute(FIRST_NAME_ATTR, user.getFirstName());
            request.getSession().setAttribute(LAST_NAME_ATTR, user.getLastName());
            request.getSession().setAttribute(ROLE_ATTR, user.getRole().toString().toLowerCase());
            page = PagesNames.MAIN_PAGE_KEY;
            request.setAttribute(ERROR_LOGIN_PASS_ATTR, null);
        } else {
            request.setAttribute(ERROR_LOGIN_PASS_ATTR, true);
        }
        return page;
    }
}
