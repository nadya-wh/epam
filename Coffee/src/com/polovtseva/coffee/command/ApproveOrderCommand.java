package com.polovtseva.coffee.command;

import com.polovtseva.coffee.dao.impl.OrderDAOImpl;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.OrderServiceImpl;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 21.02.2016.
 */
public class ApproveOrderCommand implements Command {

    static final Logger LOG = Logger.getLogger(ApproveOrderCommand.class);
    public static final String APPROVE_SUCCESS_ATTR = "approveSuccess";
    public static final String APPROVE_ERROR_ATTR = "approveError";
    public static final String PREV_COMMAND_ATTR = "prevCommand";
    public static final String ORDER_ID_PARAM = "orderId";
    public static final String ORDERS_ATTR = "orders";

    public ApproveOrderCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_WAITING_LIST.toString());
        String page = PagesNames.ORDERS_PAGE_KEY;
        OrderService orderService = new OrderServiceImpl(OrderDAOImpl.getInstance());

        try {
            if (request.getParameterValues(ORDER_ID_PARAM) != null) {
                String[] orderIdString = request.getParameterValues(ORDER_ID_PARAM);
                long[] orderIds = new long[orderIdString.length];

                for (int i = 0; i < orderIdString.length; i++) {
                    orderIds[i] = Long.parseLong(orderIdString[i]);
                }

                if (orderService.approveOrders(orderIds)) {
                    request.setAttribute(APPROVE_SUCCESS_ATTR, true);
                } else {
                    request.setAttribute(APPROVE_ERROR_ATTR, true);
                }
            }
            request.setAttribute(ORDERS_ATTR, orderService.findAll(false));
        } catch (NumberFormatException | ServiceException e) {
            LOG.error(e);
            return PagesNames.TECH_ERROR_PAGE_KEY;
        }
        return page;
    }
}
