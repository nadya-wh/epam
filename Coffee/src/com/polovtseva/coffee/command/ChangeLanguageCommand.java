package com.polovtseva.coffee.command;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 16.02.2016.
 */
public class ChangeLanguageCommand implements Command {

    public static final String PREV_COMMAND_ATTR = "prevCommand";
    public static final String PREV_PAGE_ATTR = "prevPage";
    public static final String LANGUAGE_PARAM = "language";

    public ChangeLanguageCommand() {
    }

    @Override
    public String execute(HttpServletRequest request) {
        String language = request.getParameter(LANGUAGE_PARAM);
        request.getSession().setAttribute(LANGUAGE_PARAM, language);
        if (request.getSession().getAttribute(PREV_COMMAND_ATTR) != null) {
            String prevCommand = request.getSession().getAttribute(PREV_COMMAND_ATTR).toString();
            return CommandEnum.valueOf(prevCommand).command.execute(request);
        } else if (request.getSession().getAttribute(PREV_PAGE_ATTR) != null) {
            return request.getSession().getAttribute(PREV_PAGE_ATTR).toString();
        } else {
            return PagesNames.MAIN_URL;
        }
    }
}
