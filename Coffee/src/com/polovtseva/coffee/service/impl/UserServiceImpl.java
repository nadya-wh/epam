package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.UserDAO;
import com.polovtseva.coffee.dao.impl.UserDAOImpl;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.service.UserService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.util.MD5Hash;
import com.polovtseva.coffee.util.Validator;

/**
 * Created by User on 01.02.2016.
 */
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public User authorisation(String username, String password) throws ServiceException {
        // TODO: 19.02.2016 check or not to check?
//        if (!Validator.validateLogin(username) || !Validator.validatePassword(password)) {
//            return null;
//        }
        String hash = MD5Hash.countHash(password);
        if (userDAO.authenticate(username, hash)) {
            try {
                return userDAO.findOne(username);
            } catch (DAOException e) {
                throw new ServiceException(e);
            }
        }
        return null;
    }

    public boolean create(User user) throws ServiceException {
        if (user == null || !Validator.validateUser(user)) {
            return false;
        }
        try {
            if (userDAO.findOne(user.getLogin()) != null) {
                return false;
            }
            user.setPassword(MD5Hash.countHash(user.getPassword()));
            if (userDAO.create(user) == 1) {
                return true;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return false;
    }
}
