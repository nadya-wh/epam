package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.DrinkDAO;
import com.polovtseva.coffee.dao.IngredientDAO;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.util.Validator;

import java.util.List;

/**
 * Created by User on 02.02.2016.
 */
public class IngredientServiceImpl implements IngredientService {

    private IngredientDAO ingredientDAO;

    public IngredientServiceImpl(IngredientDAO ingredientDAO) {
        this.ingredientDAO = ingredientDAO;
    }

    public boolean create(Ingredient ingredient) throws ServiceException {
        if (ingredient == null ||
                !Validator.validateName(ingredient.getIngredientName()) ||
                !Validator.validateUrl(ingredient.getImageUrl()) ||
                !Validator.validatePrice(ingredient.getIngredientPrice()) ||
                !Validator.validatePortionsNumber(ingredient.getPortionsLeft())) {
            return false;
        }
        try {
            int count = ingredientDAO.create(ingredient);
            if (count == 1) {
                return true;
            } else {
                return false;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Ingredient> findAll() throws ServiceException {
        try {
            return ingredientDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Ingredient find(long id) throws ServiceException {
        try {
            return ingredientDAO.findOne(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public int updatePortionsLeft(long id, int count) throws ServiceException {
        try {
            return ingredientDAO.updatePortions(id, count);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean delete(long id) throws ServiceException {
        try {
            if (ingredientDAO.countIngredientInOrders(id) != 0) {
                return false;
            }
            int count = ingredientDAO.delete(id);
            if (count > 0) {
                return true;
            } else {
                return false;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
