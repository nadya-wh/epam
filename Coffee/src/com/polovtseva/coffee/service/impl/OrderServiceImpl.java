package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.OrderDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.dao.impl.OrderDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 04.02.2016.
 */
public class OrderServiceImpl implements OrderService {
    private OrderDAO orderDAO;


    public OrderServiceImpl(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;

    }

    @Override
    public List<Order> findAll(long userId) throws ServiceException {
        try {
            List<Order> orders = orderDAO.findAllForUser(userId);
            findAdditionalInfoForOrders(orders);
            return orders;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean createOrder(Order order) throws ServiceException { // TODO: 13.02.2016 how to fix (transaction)???;
        if (order == null) {
            return false;
        }
        try {
            if (orderDAO.create(order) == 1) {
                order.setId(orderDAO.findOne(order.getUserId(), order.getOrderDate()));
                // TODO: 19.02.2016 check result
                //orderDAO.createOrderDrink(order);
                //orderDAO.createOrderDrinkIngredient(order);
                return true;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return false;
    }

    public List<Order> findAll(boolean approved) throws ServiceException {
        try {
            List<Order> orders = orderDAO.findAll(approved);
            findAdditionalInfoForOrders(orders);

            return orders;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<Order> findAll(long userId, boolean approved) throws ServiceException {
        try {
            List<Order> orders = orderDAO.findAll(userId, approved);
            findAdditionalInfoForOrders(orders);

            return orders;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean approveOrders(long[] orderIds) throws ServiceException {
        try {
            int[] res = orderDAO.approve(orderIds);
            for (int i = 0; i < res.length; i++) {
                if (res[i] != 1) {
                    return false;
                }
            }
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    private void findAdditionalInfoForOrders(List<Order> orders) throws DAOException {
        for (Order order : orders) {
            List<Drink> drinks = DrinkDAOImpl.getInstance().findAllInOrder(order.getId());
            for (Drink drink : drinks) {
                List<Ingredient> ingredients = IngredientDAOImpl.getInstance().findAll(order.getId(), drink.getId());
                drink.setIngredients(ingredients);
            }
            order.setDrinks(drinks);
        }
    }

}
