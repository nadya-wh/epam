package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.AccountDAO;
import com.polovtseva.coffee.dao.impl.AccountDAOImpl;
import com.polovtseva.coffee.domain.Account;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.service.AccountService;
import com.polovtseva.coffee.service.exception.ServiceException;

/**
 * Created by User on 10.02.2016.
 */
public class AccountServiceImpl implements AccountService {

    private AccountDAO accountDAO;

    public AccountServiceImpl(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }


    public Account findEntityByUserId(long userId) throws ServiceException {
        try {
            return accountDAO.findOneByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean withdrawMoney(Account account, double sum) throws ServiceException {
        try {
            if (account.getBalance() >= sum &&
                    accountDAO.updateBalance(account, account.getBalance() - sum) == 1) {
                return true;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return false;
    }
}
