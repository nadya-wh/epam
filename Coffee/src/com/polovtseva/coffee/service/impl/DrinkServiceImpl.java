package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.DrinkDAO;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.util.Validator;

import java.util.List;

/**
 * Created by User on 04.02.2016.
 */
public class DrinkServiceImpl implements DrinkService {

    private DrinkDAO drinkDAO;

    public DrinkServiceImpl(DrinkDAO drinkDAO) {
        this.drinkDAO = drinkDAO;
    }

    public List<Drink> findAllDrinks() throws ServiceException {
        try {
            List<Drink> drinks = drinkDAO.findAll();
            return drinks;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean create(Drink drink) throws ServiceException {
        if (!Validator.validateName(drink.getDrinkName()) ||
                !Validator.validatePortionsNumber(drink.getPortionsLeft()) ||
                !Validator.validatePrice(drink.getDrinkPrice()) ||
                !Validator.validateUrl(drink.getImageUrl())) {
            return false;
        }
        try {
            int count =  drinkDAO.create(drink);
            if (count == 1) {
                return true;
            }
            return false;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Drink find(long drinkId) throws ServiceException {
        try {
            return drinkDAO.findOne(drinkId);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    public int updatePortionsLeft(long drinkId, int portionsLeft) throws ServiceException {
        try {
            return drinkDAO.updatePortions(drinkId, portionsLeft);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean delete(long drinkId) throws ServiceException {
        try {
            if (drinkDAO.countInOrders(drinkId) == 0) {
                int count = drinkDAO.delete(drinkId);
                if (count > 0) {
                    return true;
                }
            }
            return false;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
