package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.Account;
import com.polovtseva.coffee.service.exception.ServiceException;

/**
 * Created by User on 12.02.2016.
 */
public interface AccountService {
    Account findEntityByUserId(long userId) throws ServiceException;
    public boolean withdrawMoney(Account account, double sum) throws ServiceException;

}
