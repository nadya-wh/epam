package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.service.exception.ServiceException;

/**
 * Created by User on 12.02.2016.
 */
public interface UserService {

    User authorisation(String username, String password) throws ServiceException;

    boolean create(User user) throws ServiceException;

}
