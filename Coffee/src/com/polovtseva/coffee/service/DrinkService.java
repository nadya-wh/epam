package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.List;

/**
 * Created by User on 12.02.2016.
 */
public interface DrinkService {

    List<Drink> findAllDrinks() throws ServiceException;

    boolean create(Drink drink) throws ServiceException;

    Drink find(long drinkId) throws ServiceException;

    int updatePortionsLeft(long drinkId, int portionsLeft) throws ServiceException;

    boolean delete(long drinkId) throws ServiceException;
}
