package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 12.02.2016.
 */
public interface OrderService {

    List<Order> findAll(long userId) throws ServiceException;

    boolean createOrder(Order order) throws ServiceException;

    List<Order> findAll(boolean approved) throws ServiceException;

    List<Order> findAll(long userId, boolean approved) throws ServiceException;

    boolean approveOrders(long[] orderIds) throws ServiceException;

}
