package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.List;

/**
 * Created by User on 12.02.2016.
 */
public interface IngredientService {

    boolean create(Ingredient ingredient) throws ServiceException;

    List<Ingredient> findAll() throws ServiceException;

    Ingredient find(long id) throws ServiceException;

    int updatePortionsLeft(long id, int count) throws ServiceException;

    boolean delete(long id) throws ServiceException;

}
