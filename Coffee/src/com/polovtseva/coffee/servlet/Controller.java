package com.polovtseva.coffee.servlet;

import com.polovtseva.coffee.command.Command;
import com.polovtseva.coffee.command.CommandFactory;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by User on 20.01.2016.
 */
@WebServlet(
        name = "Controller",
        urlPatterns = {"/coffee/*", "/coffee", "/user/history", "/user/order", "/user/waitinglist"},
        initParams =
                {
                        @WebInitParam(name = "init_log4j", value = "log4j.properties")
                }
)
public class Controller extends HttpServlet {

    //final static Logger LOG = Logger.getLogger(Controller.class);

    @Override
    public void init() throws ServletException {
        String prefix = getServletContext().getRealPath("/");
        String filename = getInitParameter("init_log4j");
        PropertyConfigurator.configure(prefix + filename);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String page = processRequest(req);
        req.getRequestDispatcher(page).forward(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String page = processRequest(req);
        req.getRequestDispatcher(page).forward(req, resp);
    }

    private String processRequest(HttpServletRequest req) {
        Command command = CommandFactory.defineCommand(req);
        return command.execute(req);
    }

    @Override
    public void destroy() {
        //ConnectionPool.getInstance().releasePool();
    }
}
