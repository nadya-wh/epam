package test.com.polovtseva.coffee.util;

import com.polovtseva.coffee.util.MD5Hash;
import org.junit.Assert;
import org.junit.Test;


/**
 * Created by User on 11.02.2016.
 */
public class MD5HashTest {

    @Test
    public void countHashTest() {
        String expected = "21232f297a57a5a743894a0e4a801fc3";
        String actual = MD5Hash.countHash("admin");
        Assert.assertEquals(expected, actual);
    }


}