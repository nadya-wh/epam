package test.com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.DrinkDAO;
import com.polovtseva.coffee.dao.IngredientDAO;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 * Created by User on 18.02.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class IngredientServiceImplTest {

    @Mock
    private IngredientDAO ingredientDAO;

    private Ingredient correctIngredient;
    private Ingredient wrongIngredietnt;

    private IngredientServiceImpl ingredientService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        ingredientService = new IngredientServiceImpl(ingredientDAO);
        correctIngredient = new Ingredient(1, "name", 0.7, "url", 789);
        wrongIngredietnt = new Ingredient(1, "name", -0.7, "url", 789);

    }

    @Test
    public void testCreate() throws Exception {
        when(ingredientDAO.create(any(Ingredient.class))).thenReturn(1);
        assertTrue(ingredientService.create(correctIngredient));
        assertFalse(ingredientService.create(wrongIngredietnt));
    }

    @Test
    public void testDelete() throws Exception {
        when(ingredientDAO.countIngredientInOrders(anyInt())).thenReturn(0);
        when(ingredientDAO.delete(anyInt())).thenReturn(1);
        assertTrue(ingredientService.delete(correctIngredient.getId()));

    }
}