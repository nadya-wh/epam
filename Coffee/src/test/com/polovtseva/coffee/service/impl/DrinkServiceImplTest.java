package test.com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.DrinkDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 * Created by User on 17.02.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class DrinkServiceImplTest {

    static final Logger LOG = Logger.getLogger(DrinkServiceImplTest.class);

    @Mock
    private DrinkDAO drinkDAO;

    private Drink wrongDrink;
    private Drink correctDrink;
    private DrinkServiceImpl drinkService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        drinkService = new DrinkServiceImpl(drinkDAO); // TODO: 18.02.2016 inject mocks
        wrongDrink = new Drink(1, "lal", -123, 0.6, "lalala");
        correctDrink = new Drink(1, "lal", 123, 0.6, "lalala");
        try {
            when(drinkDAO.create(any(Drink.class))).thenReturn(1);
        } catch (DAOException e) {
            LOG.error(e);
        }
    }

    @Test
    public void testCreate() {

        assertNotNull(drinkDAO);
        try {
            assertFalse(drinkService.create(wrongDrink));
            assertTrue(drinkService.create(correctDrink));
        } catch (ServiceException e) {
            LOG.error(e);
        }
    }


    @Test
    public void testDeletePositive() {
        try {
            when(drinkDAO.countInOrders(anyInt())).thenReturn(0);
            when(drinkDAO.delete(anyInt())).thenReturn(1);
            assertTrue(drinkService.delete(wrongDrink.getId()));
        } catch (ServiceException | DAOException e) {
            LOG.error(e); // TODO: 18.02.2016 ASK!!!
        }
    }

    @Test
    public void testDeleteNegative() {
        try {
            when(drinkDAO.delete(anyInt())).thenReturn(0);
            when(drinkDAO.countInOrders(anyInt())).thenReturn(0);
            assertFalse(drinkService.delete(wrongDrink.getId()));
        } catch (ServiceException | DAOException e) {
            LOG.error(e);
        }
    }


}