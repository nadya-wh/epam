package test.com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.AccountDAO;
import com.polovtseva.coffee.domain.Account;
import com.polovtseva.coffee.service.impl.AccountServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 * Created by User on 19.02.2016.
 */
public class AccountServiceImplTest {
    @Mock
    private AccountDAO accountDAO;
    private AccountServiceImpl accountService;
    private Account correctAccount;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        accountService = new AccountServiceImpl(accountDAO);
        correctAccount = new Account(1, 1, 1);
    }

    @Test
    public void testFindEntityByUserId() throws Exception {
        when(accountDAO.findOneByUserId(anyLong())).thenReturn(correctAccount);
        assertEquals(accountService.findEntityByUserId(correctAccount.getUserId()), correctAccount);
    }

    @Test
    public void testWithdrawMoney() throws Exception {
        when(accountDAO.updateBalance(any(Account.class), anyDouble())).thenReturn(1);
        assertFalse(accountService.withdrawMoney(correctAccount, 2 * correctAccount.getBalance()));
        assertTrue(accountService.withdrawMoney(correctAccount, correctAccount.getBalance()));
    }
}