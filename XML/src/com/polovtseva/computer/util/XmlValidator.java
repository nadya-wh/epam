package com.polovtseva.computer.util;

import org.apache.log4j.Logger;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.FileReader;

/**
 * Created by User on 12.12.2015.
 */
public class XmlValidator {

    static Logger LOG = Logger.getLogger(XmlValidator.class);


    public static boolean validateAgainstXSD(FileReader xml, FileReader xsd) {
        try {
            SchemaFactory factory =
                    SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(xsd));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xml));
            return true;
        } catch (Exception ex) {
            LOG.error(ex);
            return false;
        }
    }
}
