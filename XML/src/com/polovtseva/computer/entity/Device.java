package com.polovtseva.computer.entity;

/**
 * Created by User on 01.01.2016.
 */
public abstract class Device {
    private Manufacture origin;
    private float price;
    private boolean critical;
    private String name;
    private DeviceType deviceType;

    public Device() {
    }

    public Device(Manufacture origin, float price, boolean critical) {
        this.origin = origin;
        this.price = price;
        this.critical = critical;
    }

    public void setOrigin(Manufacture origin) {
        this.origin = origin;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setCritical(boolean critical) {
        this.critical = critical;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Manufacture getOrigin() {
        return origin;
    }

    public float getPrice() {
        return price;
    }

    public boolean isCritical() {
        return critical;
    }

    public String getName() {
        return name;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }
}
