package com.polovtseva.computer.entity;

/**
 * Created by User on 01.01.2016.
 */
public class Laptop extends Device {

    private int batteryCapacity;

    public Laptop() {
    }

    public Laptop(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Laptop(Manufacture origin, float price, boolean critical, int batteryCapacity) {
        super(origin, price, critical);
        this.batteryCapacity = batteryCapacity;
    }

    public void setBatteryCapacity(int batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "batteryCapacity=" + batteryCapacity +
                ", origin=" + getOrigin() +
                ", price=" + getPrice() +
                ", name=" + getName() +
                ", critical= " + isCritical() +
                ", deviceType=" + getDeviceType() +
                '}';
    }
}
