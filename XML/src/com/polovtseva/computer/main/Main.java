package com.polovtseva.computer.main;

import com.polovtseva.computer.creator.DeviceBuilder;
import com.polovtseva.computer.creator.DeviceDOMBuilder;
import com.polovtseva.computer.creator.DeviceSAXBuilder;
import com.polovtseva.computer.creator.DevicesStAXBuilder;
import com.polovtseva.computer.entity.Device;
import com.polovtseva.computer.util.XmlValidator;
import org.apache.log4j.Logger;

import java.io.*;

public class Main {

    static Logger LOG = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        String xmlFilename = "res/devices.xml";
        String xsdFilename = "res/devices.xsd";
        try (FileReader xml = new FileReader(xmlFilename)) {
            FileReader xsd = new FileReader(xsdFilename);
            System.out.println("Validation result: " + XmlValidator.validateAgainstXSD(xml, xsd));
        } catch (FileNotFoundException e) {
            LOG.fatal(e);
        } catch (IOException e) {
            LOG.error(e);
        }
        DeviceBuilder deviceBuilder = null;
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String menu = "1) SAX\n2) DOM\n3) StAX";
        int choice;
        System.out.println(menu);
        try {
            choice = Integer.parseInt(input.readLine());
        } catch (IOException e) {
            System.err.println("Wrong input.");
            return;
        } catch (NumberFormatException e) {
            System.err.println("Wrong input.");
            return;
        }
        switch (choice) {
            case 1:
                deviceBuilder = new DeviceSAXBuilder();
                break;
            case 2:
                deviceBuilder = new DeviceDOMBuilder();
                break;
            case 3:
                deviceBuilder = new DevicesStAXBuilder();
                break;
            default:
                return;
        }

        if (deviceBuilder != null) {
            deviceBuilder.buildSetDevices(xmlFilename);
            deviceBuilder.getDevices().forEach(System.out::println);
        }
    }
}
