package com.polovtseva.computer.creator;

import com.polovtseva.computer.entity.*;
import com.polovtseva.computer.exception.NoSuchDeviceTypeException;
import org.apache.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by User on 02.01.2016.
 */
public class DevicesStAXBuilder extends DeviceBuilder {

    static Logger LOG = Logger.getLogger(DevicesStAXBuilder.class);

    private XMLInputFactory inputFactory;

    public DevicesStAXBuilder() {
        inputFactory = XMLInputFactory.newInstance();
        devices = new HashSet<>();
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public void buildSetDevices(String filename) {
        FileInputStream inputStream;
        XMLStreamReader reader;
        String name;
        try {
            inputStream = new FileInputStream(new File(filename));
            reader = inputFactory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int type = reader.next();
                if (type == XMLStreamConstants.START_ELEMENT) {
                    name = reader.getLocalName();
                    if (DeviceEnum.LAPTOP.getValue().equals(name)) {
                        Laptop laptop = buildLaptop(reader);
                        devices.add(laptop);
                    } else if (DeviceEnum.LAPTOP_TABLET.getValue().equals(name)) {
                        LaptopTablet laptopTablet = buildLaptopTablet(reader);
                        devices.add(laptopTablet);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            LOG.error(e);
        } catch (XMLStreamException e) {
            LOG.error(e);
        }
    }

    private Laptop buildLaptop(XMLStreamReader reader) throws XMLStreamException {
        Laptop laptop = new Laptop();
        buildDeviceAttributes(reader, laptop);
        putDeviceValues(reader, laptop);
        return laptop;
    }

    private void putDeviceValues(XMLStreamReader reader, Device device) throws XMLStreamException {
        String name;
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    name = name.replace('-', '_');
                    switch (DeviceEnum.valueOf(name.toUpperCase())) {
                        case PRICE:
                            float price = Float.parseFloat(getXMLText(reader));
                            device.setPrice(price);
                            break;
                        case CRITICAL:
                            boolean critical = Boolean.getBoolean(getXMLText(reader));
                            device.setCritical(critical);
                            break;
                        case ORIGIN:
                            device.setOrigin(getXMLOrigin(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    name = name.replace('-', '_');
                    if (DeviceEnum.valueOf(name.toUpperCase()) == DeviceEnum.LAPTOP ||
                            DeviceEnum.valueOf(name.toUpperCase()) == DeviceEnum.LAPTOP_TABLET) {
                        return;
                    }
                    break;
            }
        }
    }

    private LaptopTablet buildLaptopTablet(XMLStreamReader reader) throws XMLStreamException {
        LaptopTablet laptopTablet = new LaptopTablet();
        buildDeviceAttributes(reader, laptopTablet);
        float tabletWeight = Float.parseFloat(reader.getAttributeValue(null, DeviceEnum.TABLET_WEIGHT.getValue()));
        laptopTablet.setTabletWeight(tabletWeight);
        putDeviceValues(reader, laptopTablet);
        return laptopTablet;
    }

    private void buildDeviceAttributes(XMLStreamReader reader, Device device) {
        device.setName(reader.getAttributeValue(null, DeviceEnum.NAME.getValue()));
        try {
            String type = reader.getAttributeValue(null, DeviceEnum.TYPE.getValue());
            if (type != null &&!type.isEmpty()) {
                device.setDeviceType(DeviceType.takeValueFromString(type));
            } else {
                device.setDeviceType(DeviceType.COMPUTER);
            }
        } catch (NoSuchDeviceTypeException e) {
            LOG.error(e);
        }
    }

    private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
        String text = null;
        if (reader.hasNext()) {
            reader.next();
            text = reader.getText();
        }
        return text;
    }

    private Manufacture getXMLOrigin(XMLStreamReader reader) throws XMLStreamException {
        Manufacture manufacture = new Manufacture();
        int type;
        String name;
        while (reader.hasNext()) {
            type = reader.next();
            switch (type) {
                case XMLStreamConstants.START_ELEMENT:
                    name = reader.getLocalName();
                    switch (DeviceEnum.valueOf(name.toUpperCase())) {
                        case COUNTRY:
                            manufacture.setCountry(getXMLText(reader));
                            break;
                        case CITY:
                            manufacture.setCity(getXMLText(reader));
                            break;
                        case STREET:
                            manufacture.setStreet(getXMLText(reader));
                            break;
                        case NAME:
                            manufacture.setName(getXMLText(reader));
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    name = reader.getLocalName();
                    if (DeviceEnum.valueOf(name.toUpperCase()) == DeviceEnum.ORIGIN) {
                        return manufacture;
                    }
                    break;
            }
        }
        return manufacture;
    }

}
