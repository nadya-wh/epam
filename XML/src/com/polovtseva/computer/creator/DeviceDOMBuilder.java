package com.polovtseva.computer.creator;

import com.polovtseva.computer.entity.*;
import com.polovtseva.computer.exception.NoSuchDeviceTypeException;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by User on 01.01.2016.
 */
public class DeviceDOMBuilder extends DeviceBuilder {

    static Logger LOG = Logger.getLogger(DeviceDOMBuilder.class);

    private DocumentBuilder documentBuilder;

    public DeviceDOMBuilder() {
        devices = new HashSet<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            LOG.fatal(e);
        }
    }

    @Override
    public Set<Device> getDevices() {
        return devices;
    }

    @Override
    public void buildSetDevices(String filename) {
        Document document = null;
        try {
            document = documentBuilder.parse(new File(filename));
            Element root = document.getDocumentElement();
            NodeList laptopList = root.getElementsByTagName(DeviceEnum.LAPTOP.getValue());
            for (int i = 0; i < laptopList.getLength(); i++) {
                Element deviceElement = (Element) laptopList.item(i);
                Device device = buildLaptop(deviceElement);
                devices.add(device);
            }
            NodeList laptopTabletList = root.getElementsByTagName(DeviceEnum.LAPTOP_TABLET.getValue());
            for (int i = 0; i < laptopTabletList.getLength(); i++) {
                Element deviceElement = (Element) laptopTabletList.item(i);
                Device device = buildLaptopTablet(deviceElement);
                devices.add(device);
            }
        } catch (SAXException e) {
            LOG.error(e);
        } catch (IOException e) {
            LOG.error(e);
        }
    }

    private Laptop buildLaptop(Element deviceElement) {
        Laptop laptop = new Laptop();
        putDeviceAttributes(deviceElement, laptop);
        putDeviceValues(deviceElement, laptop);
        putLaptopAttributes(deviceElement, laptop);
        return laptop;
    }

    private LaptopTablet buildLaptopTablet(Element deviceElement) {
        LaptopTablet laptopTablet = new LaptopTablet();
        putLaptopAttributes(deviceElement, laptopTablet);
        putDeviceValues(deviceElement, laptopTablet);
        return laptopTablet;
    }

    private void putLaptopAttributes(Element element, Laptop laptop) {
        try {
            Integer batteryCapacity = Integer.parseInt(element.getAttribute(DeviceEnum.BATTERY_CAPACITY.getValue()));
            laptop.setBatteryCapacity(batteryCapacity);
        } catch (NumberFormatException e) {

        }
    }

    private void putDeviceAttributes(Element deviceElement, Device device) {
        device.setName(deviceElement.getAttribute(DeviceEnum.NAME.getValue()));
        try {
            device.setDeviceType(DeviceType.takeValueFromString(deviceElement.getAttribute(DeviceEnum.TYPE.getValue())));
        } catch (NoSuchDeviceTypeException e) {
            LOG.info(e);
        }
    }

    private void putDeviceValues(Element deviceElement, Device device) {
        try {
            float price = Float.parseFloat(getElementTextContext(deviceElement, DeviceEnum.PRICE.getValue()));
            device.setPrice(price);
        } catch (NumberFormatException e) {
            LOG.error(e);
        }
        boolean critical = Boolean.getBoolean(getElementTextContext(deviceElement, DeviceEnum.CRITICAL.getValue()));
        device.setCritical(critical);
        Element originElement = (Element) deviceElement.getElementsByTagName(DeviceEnum.ORIGIN.getValue()).item(0);
        Manufacture origin = getOriginFromElement(originElement);
        device.setOrigin(origin);

    }

    private static String getElementTextContext(Element element, String elementName) {
        NodeList nodeList = element.getElementsByTagName(elementName);
        Node node = nodeList.item(0);
        return node.getTextContent();
    }

    private static Manufacture getOriginFromElement(Element element) {
        Manufacture manufacture = new Manufacture();
        manufacture.setCountry(getElementTextContext(element, DeviceEnum.COUNTRY.getValue()));
        manufacture.setCity(getElementTextContext(element, DeviceEnum.CITY.getValue()));
        manufacture.setName(getElementTextContext(element, DeviceEnum.NAME.getValue()));
        manufacture.setStreet(getElementTextContext(element, DeviceEnum.STREET.getValue()));
        return manufacture;
    }


}
