package com.polovtseva.computer.creator;

import com.polovtseva.computer.entity.Device;

import java.util.Set;

/**
 * Created by User on 02.01.2016.
 */
public abstract class DeviceBuilder {

    protected Set<Device> devices;

    public abstract void buildSetDevices(String filename);
    public abstract Set<Device> getDevices();

}
