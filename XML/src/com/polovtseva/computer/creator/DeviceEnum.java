package com.polovtseva.computer.creator;

/**
 * Created by User on 01.01.2016.
 */
public enum DeviceEnum {
    DEVICES("devices"),
    DEVICE("device"),
    LAPTOP("laptop"),
    LAPTOP_TABLET("laptop-tablet"),
    BATTERY_CAPACITY("battery-capacity"),
    TABLET_WEIGHT("tablet-weight"),
    PRICE("price"),
    ORIGIN("origin"),
    CRITICAL("critical"),
    NAME("name"),
    COUNTRY("country"),
    CITY("city"),
    STREET("street"),
    TYPE("type");

    private String value;

    DeviceEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
