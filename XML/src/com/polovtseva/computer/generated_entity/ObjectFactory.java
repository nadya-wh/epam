
package com.polovtseva.computer.generated_entity;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.polovtseva.computer.generated_entity package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Device_QNAME = new QName("task4", "device");
    private final static QName _Laptop_QNAME = new QName("task4", "laptop");
    private final static QName _LaptopTablet_QNAME = new QName("task4", "laptop-tablet");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.polovtseva.computer.generated_entity
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LaptopTablet }
     * 
     */
    public LaptopTablet createLaptopTablet() {
        return new LaptopTablet();
    }

    /**
     * Create an instance of {@link Device }
     * 
     */
    public Device createDevice() {
        return new Device();
    }

    /**
     * Create an instance of {@link Devices }
     * 
     */
    public Devices createDevices() {
        return new Devices();
    }

    /**
     * Create an instance of {@link Laptop }
     * 
     */
    public Laptop createLaptop() {
        return new Laptop();
    }

    /**
     * Create an instance of {@link Manufacture }
     * 
     */
    public Manufacture createManufacture() {
        return new Manufacture();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Device }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "task4", name = "device")
    public JAXBElement<Device> createDevice(Device value) {
        return new JAXBElement<Device>(_Device_QNAME, Device.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Laptop }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "task4", name = "laptop", substitutionHeadNamespace = "task4", substitutionHeadName = "device")
    public JAXBElement<Laptop> createLaptop(Laptop value) {
        return new JAXBElement<Laptop>(_Laptop_QNAME, Laptop.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LaptopTablet }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "task4", name = "laptop-tablet", substitutionHeadNamespace = "task4", substitutionHeadName = "device")
    public JAXBElement<LaptopTablet> createLaptopTablet(LaptopTablet value) {
        return new JAXBElement<LaptopTablet>(_LaptopTablet_QNAME, LaptopTablet.class, null, value);
    }

}
