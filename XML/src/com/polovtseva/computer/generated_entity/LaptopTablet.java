
package com.polovtseva.computer.generated_entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Laptop-Tablet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Laptop-Tablet">
 *   &lt;complexContent>
 *     &lt;extension base="{task4}Laptop">
 *       &lt;attribute name="tablet-weight" type="{task4}Weight" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Laptop-Tablet", namespace = "task4")
public class LaptopTablet
    extends Laptop
{

    @XmlAttribute(name = "tablet-weight")
    protected Float tabletWeight;

    /**
     * Gets the value of the tabletWeight property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getTabletWeight() {
        return tabletWeight;
    }

    /**
     * Sets the value of the tabletWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setTabletWeight(Float value) {
        this.tabletWeight = value;
    }

}
