
package com.polovtseva.computer.generated_entity;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Laptop complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Laptop">
 *   &lt;complexContent>
 *     &lt;extension base="{task4}Device">
 *       &lt;attribute name="battery-capacity" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Laptop", namespace = "task4")
@XmlSeeAlso({
    LaptopTablet.class
})
public class Laptop
    extends Device
{

    @XmlAttribute(name = "battery-capacity")
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger batteryCapacity;

    /**
     * Gets the value of the batteryCapacity property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatteryCapacity() {
        return batteryCapacity;
    }

    /**
     * Sets the value of the batteryCapacity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatteryCapacity(BigInteger value) {
        this.batteryCapacity = value;
    }

}
