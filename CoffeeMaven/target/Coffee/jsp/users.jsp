<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="users.jsp"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="users.title"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <script src="../js/ie-emulation-modes-warning.js"></script>

</head>
<header>

</header>
<body>

<%@include file="jspf/navbar.jspf" %>
<div class="container" style="vertical-align: middle !important;">
    <form action="/coffee" method="POST">
        <input type="hidden" name="command" value="EDIT_USER">
        <table class="table table-info	table-striped">
            <thead>
            <tr>
                <th>

                </th>
                <th>

                </th>
                <th>

                </th>
                <th>

                </th>
                <th>

                </th>
            </tr>
            </thead>
            <tbody>

            <c:forEach var="item" items="${users}">
                <c:if test="${item.id != userId}">
                    <input type="hidden" name="userId" value="${item.id}"/>
                    <tr>
                        <td>
                                ${item.id}
                        </td>
                        <td>
                                ${item.login}
                        </td>
                        <td>
                                ${item.firstName}
                        </td>
                        <td>
                                ${item.lastName}
                        </td>
                        <td>
                            <select name="role" class="form-control">
                                <option value="admin"  ${item.role == 'ADMIN' ? 'selected' : ''}>
                                    <fmt:message key="users.admin"/>
                                </option>
                                <option value="user"  ${item.role == 'USER' ? 'selected' : ''}>
                                    <fmt:message key="users.user"/>
                                </option>
                                <option value="blocked"  ${item.role == 'BLOCKED' ? 'selected' : ''}>
                                    <fmt:message key="users.blocked"/>
                                </option>
                            </select>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>

            </tbody>
        </table>
        <button class="btn btn-primary pull-right" type="submit">
            <fmt:message key="users.button.submit"/>
        </button>
    </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <%--<ctg:pfooter/>--%>
</footer>
</html>
