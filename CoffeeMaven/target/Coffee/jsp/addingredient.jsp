<%@ page contentType="text/html;charset=UTF-8"  pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="addrink.jsp"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="addingredient.header"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">

    <script src="../js/ie-emulation-modes-warning.js"></script>
</head>
<header>
    <script>
        function validateForm() {
            var ingredientName = document.forms["addIngredientForm"]["ingredientName"].value;
            var portionsLeft = document.forms["addIngredientForm"]["portionsLeft"].value;
            var price = document.forms["addIngredientForm"]["ingredientPrice"].value;
            var namePattern = /^[a-zA-zа-яА-Я]+([ '-][a-zA-Zа-яА-Я]+)*$/;
            if (!namePattern.test(ingredientName)) {
                document.getElementById("ingredientNameValidationError").innerHTML =
                        "<div class='alert alert-danger'><fmt:bundle basename='i18n.messages'><fmt:message key='message.notvalidname'/></fmt:bundle></div>";
                return false;
            } else if (portionsLeft == "" || isNaN(portionsLeft) || portionsLeft < 0) {
                document.getElementById("portionsLeftValidationError").innerHTML =
                        "<div class='alert alert-danger'><fmt:bundle basename='i18n.messages'><fmt:message key='message.notvalidnumber'/></fmt:bundle></div>";
                return false;
            } else if (price == "" || isNaN(price) || price < 0) {
                document.getElementById("priceValidationError").innerHTML =
                        "<div class='alert alert-danger'><fmt:bundle basename='i18n.messages'><fmt:message key='message.notvalidnumber'/></fmt:bundle></div>";
                return false;
            }
        }
    </script>
</header>
<body>

<%@include file="jspf/navbar.jspf"%>

<div class="container" style="vertical-align: middle !important;">
    <form id="addIngredientForm" onsubmit="return validateForm()" class="form-signin" action="/coffee" method="POST">
        <c:if test="${error}">
            <div class="alert alert-danger">
                <fmt:bundle basename="i18n.messages">
                    <fmt:message key="message.addingredienterror"/>
                </fmt:bundle>
            </div>
        </c:if>
        <c:if test="${success}">
            <div class="alert alert-success">
                <fmt:bundle basename="i18n.messages">
                    <fmt:message key="message.addingredientsuccess"/>
                </fmt:bundle>
            </div>
        </c:if>
        <c:if test="${validationError}">
            <div class="alert alert-danger">
                <fmt:bundle basename="i18n.messages">
                    <fmt:message key="message.validationerror"/>
                </fmt:bundle>
            </div>
        </c:if>
        <input type="hidden" name="command" value="add_ingredient">
        <h2 class="form-signin-heading" align="center">
            <fmt:message key="addingredient.header"/>
        </h2>
        <h5>
            <fmt:message key="addingredient.form.ingredientname"/>*
        </h5>
        <div id="ingredientNameValidationError"></div>
        <input class="form-control custom-input" type="text" name="ingredientName">
        <h5>
            <fmt:message key="addingredient.form.ingredientprice"/>*
        </h5>
        <div id="priceValidationError"></div>
        <input class="form-control custom-input" type="text" name="ingredientPrice">
        <h5>
            <fmt:message key="addingredient.form.imageurl"/>
        </h5>
        <input class="form-control custom-input" type="url" name="imageUrl">
        <h5>
            <fmt:message key="addingredient.form.portionsLeft"/>
        </h5>
        <div id="portionsLeftValidationError"></div>
        <input class="form-control custom-input" type="number" step="0.1" name="portionsLeft">
        <button class="btn btn-primary center-block custom-button" type="submit">
            <fmt:message key="addingredient.button.submit"/>
        </button>

    </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <ctg:pfooter/>
</footer>
</html>
