package com.polovtseva.coffee.util;

import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.User;

import java.util.regex.Pattern;

/**
 * Class for data validation.
 */
public class Validator {
    public static final String LOGIN_REGEX = "^[a-zA-Z0-9_-]{6,20}$";
    public static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,20}$";
    public static final String NAME_REGEX = "[a-zA-zа-яА-Я]+([ '-][a-zA-Zа-яА-Я]+)*";

    public static final int MAX_URL_LENGTH = 255;

    /**
     * Validates login.
     *
     * @param login to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validateLogin(String login) {
        if (login != null) {
            Pattern pattern = Pattern.compile(LOGIN_REGEX);
            return pattern.matcher(login).matches();
        } else {
            return false;
        }
    }

    /**
     * Validates password.
     *
     * @param password to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validatePassword(String password) {
        if (password == null) {
            return false;
        }
        Pattern pattern = Pattern.compile(PASSWORD_REGEX);
        return pattern.matcher(password).matches();
    }

    /**
     * Validates name.
     *
     * @param name to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validateName(String name) {
        if (name == null) {
            return false;
        }
        Pattern pattern = Pattern.compile(NAME_REGEX);
        return pattern.matcher(name).matches();
    }

    /**
     * Validates number of portions.
     *
     * @param num to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validatePortionsNumber(int num) {
        return num >= 0;
    }

    /**
     * Validates url.
     *
     * @param url to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validateUrl(String url) {
        if (url == null) {
            return true;
        }
        return url.length() < MAX_URL_LENGTH;
    }

    /**
     * Validates price.
     *
     * @param price to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validatePrice(double price) {
        return price >= 0;
    }

    /**
     * Validates user.
     *
     * @param user to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validateUser(User user) {
        return Validator.validateLogin(user.getLogin()) &&
                Validator.validatePassword(user.getPassword()) &&
                Validator.validateName(user.getFirstName()) &&
                Validator.validateName(user.getLastName());
    }

    /**
     * Validates ingredient.
     *
     * @param ingredient to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validateIngredient(Ingredient ingredient) {
        return Validator.validateName(ingredient.getIngredientName()) &&
                Validator.validatePortionsNumber(ingredient.getPortionsLeft()) &&
                Validator.validatePrice(ingredient.getIngredientPrice()) &&
                Validator.validateUrl(ingredient.getImageUrl());
    }

    /**
     * Validates drink.
     *
     * @param drink to validate.
     * @return {@code true} if this object is valid; {@code false} otherwise.
     */
    public static boolean validateDrink(Drink drink) {
        return Validator.validateName(drink.getDrinkName()) &&
                Validator.validatePortionsNumber(drink.getPortionsLeft()) &&
                Validator.validatePrice(drink.getDrinkPrice()) &&
                Validator.validateUrl(drink.getImageUrl());
    }


}
