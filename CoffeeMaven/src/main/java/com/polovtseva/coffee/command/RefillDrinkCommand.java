package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.servlet.http.HttpServletRequest;

/**
 * The command refills a chosen drink.
 */
public class RefillDrinkCommand implements Command {

    static final Logger LOG = LogManager.getLogger(RefillDrinkCommand.class);

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String DRINKS_NUMBER_PARAM = "drinksNumber";
    public static final String DRINKS_ATTR = "drinks";
    public static final String REFILL_ERROR_ATTR = "refillError";
    public static final String REFILL_SUCCESS_ATTR = "refillSuccess";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    /**
     * The command refills a chosen drink.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        try {
            long drinkId = Long.parseLong(request.getParameter(DRINK_ID_PARAM));
            DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
            int drinkCount = Integer.parseInt(request.getParameter(DRINKS_NUMBER_PARAM));
            Drink chosenDrink = drinkService.find(drinkId);
            int newPortionsLeft =  chosenDrink.getPortionsLeft() + drinkCount;
            if (drinkCount > 0 && newPortionsLeft > chosenDrink.getPortionsLeft()) {
                drinkService.updatePortionsLeft(chosenDrink.getId(), chosenDrink.getPortionsLeft() + drinkCount);
                request.setAttribute(REFILL_SUCCESS_ATTR, true);
            } else {
                request.setAttribute(REFILL_ERROR_ATTR, true);
            }
            request.setAttribute(DRINKS_ATTR, drinkService.findAll());
        } catch (NumberFormatException e) {
            LOG.error(e);
            request.setAttribute(REFILL_ERROR_ATTR, true);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PagesNames.MENU_PAGE_KEY;
    }
}
