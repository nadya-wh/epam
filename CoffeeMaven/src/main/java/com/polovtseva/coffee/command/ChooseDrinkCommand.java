package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.command.util.CommandUtil;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * Command lets choose a drink.
 */
public class ChooseDrinkCommand implements Command {

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String USER_ID_ATTR = "userId";
    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String CURRENT_DRINK_ATTR = "currentDrink";
    public static final String DRINKS_NUMBER_PARAM = "drinksNumber";
    public static final String DRINKS_ATTR = "drinks";
    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String ADD_TO_ORDER_ERROR_ATTR = "error";
    public static final String ADD_TO_ORDER_SUCCESS_ATTR = "success";
    public static final String ADD_DRINK_TO_ORDER_PARAM = "addDrinkToOrder";
    public static final String ADD_INGREDIENT_PARAM = "addIngredient";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * Command lets choose a drink.
     * @param request request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException { // TODO: 23.03.2016 refactor
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        String page = PagesNames.MENU_PAGE_KEY;
        long drinkId = Long.parseLong(request.getParameter(DRINK_ID_PARAM));
        DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
        if (request.getSession().getAttribute(USER_ID_ATTR) == null) {
            return PagesNames.LOGIN_PAGE_KEY;
        }
        try {
            int drinkCount = Integer.parseInt(request.getParameter(DRINKS_NUMBER_PARAM));
            Drink chosenDrink = drinkService.find(drinkId);
            chosenDrink.setNumberInOrder(drinkCount);
            if (drinkCount <= 0) {
                request.setAttribute(ADD_TO_ORDER_ERROR_ATTR, true);
            } else if (request.getParameter(ADD_DRINK_TO_ORDER_PARAM) != null) {
                addDrinkToOrder(request, chosenDrink);
                request.setAttribute(DRINKS_ATTR, drinkService.findAll());
            } else if (request.getParameter(ADD_INGREDIENT_PARAM) != null) {
                chooseIngredient(request, chosenDrink);
                IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
                request.setAttribute(INGREDIENTS_ATTR, ingredientService.findAll());
                page = PagesNames.INGREDIENTS_PAGE_KEY;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        } catch (NumberFormatException e) {
            request.setAttribute(ADD_TO_ORDER_ERROR_ATTR, true);
        }
        return page;
    }

    private void chooseIngredient(HttpServletRequest request, Drink chosenDrink) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        request.getSession().setAttribute(CURRENT_DRINK_ATTR, chosenDrink);
    }

    private void addDrinkToOrder(HttpServletRequest request, Drink chosenDrink) {
        Order order = CommandUtil.takeOrder(request);
        order.addDrink(chosenDrink);
        request.getSession().setAttribute(CURRENT_ORDER_ATTR, order);
        request.setAttribute(ADD_TO_ORDER_SUCCESS_ATTR, true);
    }
}
