package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.UserDAOImpl;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;
import com.polovtseva.coffee.service.UserService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * The command lets to change user's role.
 */
public class EditUserCommand implements Command {

    public static final String USER_ID_PARAM = "userId";
    public static final String ROLE_PARAM = "role";
    public static final String PREV_PAGE = "prevPage";
    public static final String USERS_ATTR = "users";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * The command lets to change user's role.
     *
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_USER_LIST.toString());
        request.getSession().setAttribute(PREV_PAGE, PagesNames.USERS_PAGE_KEY);
        String[] idParams = request.getParameterValues(USER_ID_PARAM);
        String[] roles = request.getParameterValues(ROLE_PARAM);
        UserService userService = new UserServiceImpl(UserDAOImpl.getInstance());
        try {
            for (int i = 0; i < idParams.length; i++) {
                long id = Long.parseLong(idParams[i]);
                User user = userService.read(id);
                if (!roles[i].equalsIgnoreCase(user.getRole().toString())) {
                    userService.update(user, UserRole.valueOf(roles[i].toUpperCase()));
                }
            }
            request.setAttribute(USERS_ATTR, userService.readAll());
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PagesNames.USERS_PAGE_KEY;
    }
}
