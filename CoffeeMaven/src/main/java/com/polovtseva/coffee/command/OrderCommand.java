package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.AccountDAOImpl;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.dao.impl.OrderDAOImpl;
import com.polovtseva.coffee.domain.Account;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.AccountService;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.AccountServiceImpl;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import com.polovtseva.coffee.service.impl.OrderServiceImpl;
import com.polovtseva.coffee.command.util.CommandUtil;


import javax.servlet.http.HttpServletRequest;

/**
 * The command makes an order.
 */
public class OrderCommand implements Command {

    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String USER_ID_ATTR = "userId";
    public static final String ERROR_ATTR = "error";
    public static final String ACCOUNT_ERROR_ATTR = "accountError";
    public static final String NUMBER_ERROR_ATTR = "countError";
    public static final String TOTAL_SUM_ATTR = "totalSum";
    public static final String PREV_COMMAND_ATTR = "prevCommand";
    public static final String PREV_PAGE_ATTR = "prevPage";


    /**
     * The command makes an order.
     *
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (request.getSession().getAttribute(CURRENT_ORDER_ATTR) != null) {
            Order order = (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
            double totalSum = CommandUtil.countSum(order);
            long userId = Long.parseLong(request.getSession().getAttribute(USER_ID_ATTR).toString());
            AccountService accountService = new AccountServiceImpl(AccountDAOImpl.getInstance());
            DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
            IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
            try {
                Account account = accountService.findByUserId(userId);
                if (order.getDrinks().size() < 1) {
                    request.setAttribute(TOTAL_SUM_ATTR, totalSum);
                    request.setAttribute(NUMBER_ERROR_ATTR, true);
                } else if (account == null) {
                    request.setAttribute(ACCOUNT_ERROR_ATTR, true);
                } else if (account.getBalance() >= totalSum) {
                    boolean enoughDrinksAndIngredients = true;
                    if (!drinkService.checkEnough(order) || !ingredientService.checkEnough(order)) {
                        enoughDrinksAndIngredients = false;
                    }
                    if (enoughDrinksAndIngredients && drinkService.removePortions(order) && ingredientService.removePortions(order)) {
                        accountService.withdrawMoney(account, totalSum);
                        request.getSession().setAttribute(CURRENT_ORDER_ATTR, null);
                        OrderService orderService = new OrderServiceImpl(OrderDAOImpl.getInstance());
                        orderService.create(order);
                        request.getSession().setAttribute(PREV_COMMAND_ATTR, null);
                        request.getSession().setAttribute(PREV_PAGE_ATTR, PagesNames.SUCCESSFUL_ORDER_PAGE_KEY);
                        return PagesNames.SUCCESSFUL_ORDER_PAGE_KEY;
                    } else {
                        request.setAttribute(NUMBER_ERROR_ATTR, true);
                    }
                } else {
                    request.setAttribute(ERROR_ATTR, true);

                }
                request.setAttribute(TOTAL_SUM_ATTR, totalSum);
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
        }
        return PagesNames.CURRENT_ORDER_PAGE_KEY;
    }
}
