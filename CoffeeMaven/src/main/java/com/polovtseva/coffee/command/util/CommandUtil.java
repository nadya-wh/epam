package com.polovtseva.coffee.command.util;

import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.Order;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by User on 23.03.2016.
 */
public class CommandUtil {

    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String USER_ID_ATTR = "userId";

    /**
     * Retrieves order object from request.
     * @param request object that contains the request the client has made of the servlet.
     * @return order.
     */
    public static Order takeOrder(HttpServletRequest request) {
        if (request.getSession().getAttribute(CURRENT_ORDER_ATTR) == null) {
            int userId = Integer.parseInt(request.getSession().getAttribute(USER_ID_ATTR).toString());
            return new Order(userId);
        }
        return (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
    }

    /**
     * Casts an array of strings to a long array.
     * @param values string array of values to cast.
     * @return int array.
     */
    public static long[] castStringArrayToLongArray(String[] values) {
        long[] res = new long[values.length];
        for (int i = 0; i < values.length; i++) {
            res[i] = Long.parseLong(values[i]);
        }
        return res;
    }

    /**
     * Casts an array of strings to an int array.
     * @param values values string array of values to cast.
     * @return
     */
    public static int[] castStringArrayToIntArray(String[] values) {
        int[] res = new int[values.length];
        for (int i = 0; i < values.length; i++) {
            res[i] = Integer.parseInt(values[i]);
        }
        return res;
    }

    /**
     * Checks whether a list of ingredients contains an ingredient with specified id and number in drink.
     * @param ingredients
     * @param ingredientId
     * @param numberInDrink
     * @return
     */
    public static boolean containsIngredient(List<Ingredient> ingredients, long ingredientId, int numberInDrink) {
        for (Ingredient ingredient : ingredients) {
            if (ingredient.getId() == ingredientId && ingredient.getNumberInDrink() == numberInDrink) {
                return true;
            }
        }
        return false;
    }

    public static Drink findDrink(long drinkId, int drinksCount, long[] ingredientIds, int[] numberInDrink, Order order) {
        List<Drink> drinks = order.getDrinks();
        int ingredientsMatch = 0;
        for (Drink drink : drinks) {
            ingredientsMatch = 0;
            if (drink.getId() == drinkId && drink.getNumberInOrder() == drinksCount &&
                    drink.getIngredients().size() == ingredientIds.length) {
                List<Ingredient> ingredients = drink.getIngredients();
                for (int i = 0; i < ingredientIds.length; i++) {
                    if (CommandUtil.containsIngredient(ingredients, ingredientIds[i], numberInDrink[i])) {
                        ingredientsMatch++;
                    }
                }
                if (ingredientsMatch == ingredients.size()) {
                    return drink;
                }
            }
        }
        return null;
    }

    public static Drink findDrink(long drinkId, int drinksCount, Order order) {
        List<Drink> drinks = order.getDrinks();
        for (Drink drink : drinks) {
            if (drink.getId() == drinkId && drink.getNumberInOrder() == drinksCount &&
                    drink.getIngredients().size() == 0) {
                return drink;
            }
        }
        return null;
    }

    public static double countSum(Order order) {
        double totalSum = 0;
        if (order != null) {
            for (Drink drink : order.getDrinks()) {
                totalSum += drink.getDrinkPrice() * drink.getNumberInOrder();
                for (Ingredient ingredient : drink.getIngredients()) {
                    totalSum += ingredient.getIngredientPrice() * ingredient.getNumberInDrink() * drink.getNumberInOrder();
                }
            }
        }
        return totalSum;
    }
}
