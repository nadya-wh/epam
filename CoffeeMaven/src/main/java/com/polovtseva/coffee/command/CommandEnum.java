package com.polovtseva.coffee.command;

/**
 * A list of all possible commands.
 */
public enum CommandEnum {
    LOGIN(new LoginCommand()),

    LOGOUT(new LogoutCommand()),

    SIGN_UP(new SignUpCommand()),

    GET_DRINKS_LIST(new TakeDrinksListCommand()),

    SHOW_HISTORY(new ShowHistoryCommand()),

    ADD_DRINK(new CreateDrinkCommand()),

    ADD_INGREDIENT(new CreateIngredientCommand()),

    ADD_INGREDIENT_TO_DRINK(new ChooseIngredientCommand()),

    CHOOSE_DRINK(new ChooseDrinkCommand()),

    GET_INGREDIENTS_LIST(new GetIngredientsListCommand()),

    REFILL_COFFEE(new RefillDrinkCommand()),

    REFILL_INGREDIENTS(new RefillIngredientCommand()),

    DELETE_DRINK(new DeleteDrinkCommand()),

    DELETE_INGREDIENT(new DeleteIngredientCommand()),

    ADD_DRINK_TO_ORDER(new AddDrinkToOrderCommand()),

    SHOW_CURRENT_ORDER(new ShowCurrentOrderCommand()),

    ORDER(new OrderCommand()),

    REMOVE_DRINK_FROM_ORDER(new RemoveDrinkFromOrderCommand()),

    CHANGE_LANGUAGE(new ChangeLanguageCommand()),

    SHOW_NOT_APPROVED_ORDERS(new ShowNotApprovedOrdersCommand()),

    APPROVE_ORDER(new ApproveOrderCommand()),

    SHOW_WAITING_LIST(new ShowWaitingListCommand()),

    GET_USER_LIST(new TakeUserListCommand()),

    EDIT_USER(new EditUserCommand()),

    SHOW_ACCOUNT_INFO(new ShowAccountInfoCommand()),

    REPLENISH_BALANCE(new ReplenishBalanceCommand()),

    REMOVE_DRINK(new RemoveDrinkCommand()),

    REMOVE_INGREDIENT(new RemoveIngredientCommand());

    CommandEnum(Command command) {
        this.command = command;
    }

    Command command;

    public Command getCommand() {
        return command;
    }
}
