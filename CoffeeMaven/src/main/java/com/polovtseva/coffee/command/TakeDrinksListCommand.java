package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The command puts a list of drinks into a request object.
 */
public class TakeDrinksListCommand implements Command {

    public static final String DRINKS_ATTR = "drinks";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    /**
     * The command puts a list of drinks into a request object.
     *
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
        try {
            List<Drink> drinks = drinkService.findAll();
            request.setAttribute(DRINKS_ATTR, drinks);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }

        return PagesNames.MENU_PAGE_KEY;
    }
}
