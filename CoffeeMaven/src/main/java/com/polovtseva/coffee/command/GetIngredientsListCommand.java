package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * he command puts a list of ingredients into a request object.
 */
public class GetIngredientsListCommand implements Command {


    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * The command puts a list of ingredients into a request object.
     *
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        String page = PagesNames.INGREDIENTS_PAGE_KEY;
        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {
            List<Ingredient> ingredients = ingredientService.findAll();
            request.setAttribute(INGREDIENTS_ATTR, ingredients);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return page;
    }
}
