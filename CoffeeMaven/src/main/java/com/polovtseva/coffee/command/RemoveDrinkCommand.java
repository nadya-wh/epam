package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * The command removes a chosen drink from coffee-maker.
 */
public class RemoveDrinkCommand implements Command {

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String DRINKS_ATTR = "drinks";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * The command removes a chosen drink from coffee-maker (sets value to property portionsLeft = 0).
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        long drinkId = Long.parseLong(request.getParameter(DRINK_ID_PARAM));
        DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
        try {
            drinkService.updatePortionsLeft(drinkId, 0);
            request.setAttribute(DRINKS_ATTR, drinkService.findAll());
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PagesNames.MENU_PAGE_KEY;
    }
}
