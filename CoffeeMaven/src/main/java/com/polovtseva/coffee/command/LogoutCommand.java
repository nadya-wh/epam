package com.polovtseva.coffee.command;


import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 27.01.2016.
 */
public class LogoutCommand implements Command {

    public static final String USER_ID_ATTR = "userId";
    public static final String USERNAME_ATTR = "username";
    public static final String FIRST_NAME_ATTR = "firstName";
    public static final String LAST_NAME_ATTR = "lastName";
    public static final String ROLE_ATTR = "role";
    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String PREV_COMMAND_ATTR = "prevCommand";
    public static final String PREV_PAGE = "prevPage";


    /**
     * The command lets to logout a user.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(USERNAME_ATTR, null);
        request.getSession().setAttribute(FIRST_NAME_ATTR, null);
        request.getSession().setAttribute(LAST_NAME_ATTR, null);
        request.getSession().setAttribute(ROLE_ATTR, null);
        request.getSession().setAttribute(USER_ID_ATTR, null);
        request.getSession().setAttribute(CURRENT_ORDER_ATTR, null);
        request.getSession().setAttribute(PREV_COMMAND_ATTR, null);
        request.getSession().setAttribute(PREV_PAGE, null);
        return PagesNames.LOGIN_PAGE_KEY;
    }
}
