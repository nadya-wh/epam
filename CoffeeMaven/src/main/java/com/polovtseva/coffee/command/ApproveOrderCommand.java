package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.OrderDAOImpl;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * The command lets approve orders.
 */
public class ApproveOrderCommand implements Command {

    public static final String APPROVE_SUCCESS_ATTR = "approveSuccess";
    public static final String APPROVE_ERROR_ATTR = "approveError";
    public static final String PREV_COMMAND_ATTR = "prevCommand";
    public static final String ORDER_ID_PARAM = "orderId";
    public static final String ORDERS_ATTR = "orders";

    /**
     * The command lets approve(set status = "approved") orders.
     *
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_NOT_APPROVED_ORDERS.toString());
        String page = PagesNames.ORDERS_PAGE_KEY;
        OrderService orderService = new OrderServiceImpl(OrderDAOImpl.getInstance());
        try {
            if (request.getParameterValues(ORDER_ID_PARAM) != null) {
                long[] orderIds = takeOrderIds(request);
                if (orderService.approveOrders(orderIds)) {
                    request.setAttribute(APPROVE_SUCCESS_ATTR, true);
                } else {
                    request.setAttribute(APPROVE_ERROR_ATTR, true);
                }
            }
            request.setAttribute(ORDERS_ATTR, orderService.findAll(false));
        } catch (NumberFormatException | ServiceException e) {
            throw new CommandException(e);
        }
        return page;
    }

    private long[] takeOrderIds(HttpServletRequest request) {
        String[] orderIdString = request.getParameterValues(ORDER_ID_PARAM);
        long[] orderIds = new long[orderIdString.length];
        for (int i = 0; i < orderIdString.length; i++) {
            orderIds[i] = Long.parseLong(orderIdString[i]);
        }
        return orderIds;
    }
}
