package com.polovtseva.coffee.command;

import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.command.util.CommandUtil;

import javax.servlet.http.HttpServletRequest;

/**
 * The command puts info about current order into a request object.
 */
public class ShowCurrentOrderCommand implements Command {

    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String TOTAL_SUM_ATTR = "totalSum";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    /**
     * The command puts info about current order into a request object.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_CURRENT_ORDER.toString());
        String page = PagesNames.CURRENT_ORDER_PAGE_KEY;
        if (request.getSession().getAttribute(CURRENT_ORDER_ATTR) != null) {
            Order currentOrder = (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
            double totalSum = CommandUtil.countSum(currentOrder);
            request.setAttribute(TOTAL_SUM_ATTR, totalSum);
        }
        return page;
    }
}
