package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The command refills a chosen ingredient.
 */
public class RefillIngredientCommand implements Command {

    static final Logger LOG = LogManager.getLogger(RefillIngredientCommand.class);

    public static final String INGREDIENT_ID_PARAM = "ingredientId";
    public static final String INGREDIENT_NUMBER_PARAM = "ingredientsNumber";
    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String REFILL_ERROR_ATTR = "refillError";
    public static final String REFILL_SUCCESS_ATTR = "refillSuccess";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * The command refills a chosen ingredient.
     *
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        String page = PagesNames.INGREDIENTS_PAGE_KEY;

        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {
            long ingredientId = Long.parseLong(request.getParameter(INGREDIENT_ID_PARAM));
            Ingredient chosenIngredient = ingredientService.find(ingredientId);

            int ingredientNumber = Integer.parseInt(request.getParameter(INGREDIENT_NUMBER_PARAM));
            int newPortionsLeft = chosenIngredient.getPortionsLeft() + ingredientNumber;
            if (ingredientNumber > 0 && newPortionsLeft > chosenIngredient.getPortionsLeft()) { // check for overflow
                ingredientService.updatePortionsLeft(chosenIngredient.getId(),
                        newPortionsLeft);
            } else {
                request.setAttribute(REFILL_ERROR_ATTR, true);
            }
            List<Ingredient> ingredients = ingredientService.findAll();
            request.setAttribute(INGREDIENTS_ATTR, ingredients);
            request.setAttribute(REFILL_SUCCESS_ATTR, true);
        } catch (NumberFormatException e) {
            LOG.error(e);
            request.setAttribute(REFILL_ERROR_ATTR, true);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return page;
    }
}
