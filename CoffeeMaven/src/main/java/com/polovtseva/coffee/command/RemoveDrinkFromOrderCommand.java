package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.util.CommandUtil;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;

import javax.servlet.http.HttpServletRequest;

/**
 * The command removes a chosen drink from current order object.
 */
public class RemoveDrinkFromOrderCommand implements Command {

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String COUNT_IN_ORDER_PARAM = "numberInOrder";
    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String INGREDIENT_ID_PARAM = "ingredientId";
    public static final String TOTAL_SUM_PARAM = "totalSum";
    public static final String PREV_COMMAND_ATTR = "prevCommand";
    public static final String NUMBER_IN_DRINK_PARAM = "numberInDrink";


    /**
     * The command removes a chosen drink from current order object.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_CURRENT_ORDER.toString());
        long drinkId = Long.parseLong(request.getParameter(DRINK_ID_PARAM));
        int numberInOrder = Integer.parseInt(request.getParameter(COUNT_IN_ORDER_PARAM));
        Order order = (Order) request.getSession().getAttribute(CURRENT_ORDER_ATTR);
        Drink drinkToRemove;
        if (request.getParameterValues(INGREDIENT_ID_PARAM) != null) {
            long[] ingredientIds = CommandUtil.castStringArrayToLongArray(request.getParameterValues(INGREDIENT_ID_PARAM));
            int[] ingredientNumberInDrink = CommandUtil.castStringArrayToIntArray(request.getParameterValues(NUMBER_IN_DRINK_PARAM));
            drinkToRemove = CommandUtil.findDrink(drinkId, numberInOrder, ingredientIds, ingredientNumberInDrink, order);
        } else {
            drinkToRemove = CommandUtil.findDrink(drinkId, numberInOrder, order);
        }
        if (drinkToRemove != null) {
            order.removeDrink(drinkToRemove);
        }
        request.setAttribute(TOTAL_SUM_PARAM, CommandUtil.countSum(order));
        request.getSession().setAttribute(CURRENT_ORDER_ATTR, order);

        return PagesNames.CURRENT_ORDER_PAGE_KEY;
    }
}
