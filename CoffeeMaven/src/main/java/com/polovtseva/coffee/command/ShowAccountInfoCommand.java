package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.AccountDAOImpl;
import com.polovtseva.coffee.domain.Account;
import com.polovtseva.coffee.service.AccountService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.AccountServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * The command puts info about user account into a request object.
 */
public class ShowAccountInfoCommand implements Command {

    public static final String USER_ID_ATTR = "userId";
    public static final String ACCOUNT_ATTR = "account";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * The command puts info about user account into a request object.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next [age.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_ACCOUNT_INFO.toString());
        if (request.getSession().getAttribute(USER_ID_ATTR) != null) {
            long userId = Integer.parseInt(request.getSession().getAttribute(USER_ID_ATTR).toString());
            AccountService accountService = new AccountServiceImpl(AccountDAOImpl.getInstance());
            try {
                Account account = accountService.findByUserId(userId);
                request.setAttribute(ACCOUNT_ATTR, account);
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
        }
        return PagesNames.ACCOUNT_PAGE_KEY;
    }
}
