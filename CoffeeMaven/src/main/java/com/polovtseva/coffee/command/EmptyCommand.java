package com.polovtseva.coffee.command;


import javax.servlet.http.HttpServletRequest;

/**
 * Empty command.
 */
public class EmptyCommand implements Command {

    public static final String CURRENT_PAGE = "currentPage";

    @Override
    public String execute(HttpServletRequest request) {
        if (request.getSession().getAttribute(CURRENT_PAGE) != null) {
            return request.getSession().getAttribute(CURRENT_PAGE).toString();
        } else {
            return PagesNames.MAIN_PAGE_KEY;
        }
    }
}
