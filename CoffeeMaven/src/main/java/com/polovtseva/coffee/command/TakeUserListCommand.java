package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.UserDAOImpl;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.service.UserService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.UserServiceImpl;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The command puts a list of users into a request object.
 */
public class TakeUserListCommand implements Command {

    public static final String USERS_ATTR = "users";
    public static final String PREV_PAGE = "prevPage";
    private static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * The command puts a list of users into a request object.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_USER_LIST.toString());
        request.getSession().setAttribute(PREV_PAGE, PagesNames.USERS_PAGE_KEY);
        UserService userService = new UserServiceImpl(UserDAOImpl.getInstance());
        try {
            List<User> users = userService.readAll();
            request.setAttribute(USERS_ATTR, users);
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PagesNames.USERS_PAGE_KEY;
    }
}
