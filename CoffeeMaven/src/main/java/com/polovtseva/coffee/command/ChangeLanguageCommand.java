package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * The command changes language.
 */
public class ChangeLanguageCommand implements Command {

    public static final String PREV_COMMAND_ATTR = "prevCommand";
    public static final String PREV_PAGE_ATTR = "prevPage";
    public static final String LANGUAGE_PARAM = "language";

    /**
     * The command changes language.
     * @param request request object that contains the request the client has made of the servlet
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String language = request.getParameter(LANGUAGE_PARAM);
        request.getSession().setAttribute(LANGUAGE_PARAM, language);
        if (request.getSession().getAttribute(PREV_COMMAND_ATTR) != null) {
            String prevCommand = request.getSession().getAttribute(PREV_COMMAND_ATTR).toString();
            return CommandEnum.valueOf(prevCommand).command.execute(request);
        } else if (request.getSession().getAttribute(PREV_PAGE_ATTR) != null) {
            return request.getSession().getAttribute(PREV_PAGE_ATTR).toString();
        } else {
            return PagesNames.MAIN_URL;
        }
    }
}
