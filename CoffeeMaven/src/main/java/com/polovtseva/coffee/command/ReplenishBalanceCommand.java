package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.AccountDAOImpl;
import com.polovtseva.coffee.service.AccountService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.AccountServiceImpl;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 06.03.2016.
 */
public class ReplenishBalanceCommand implements Command {

    private static final Logger LOG = LogManager.getLogger(ReplenishBalanceCommand.class);
    public static final String SUM_ATTR = "sum";
    public static final String ERROR_ATTR = "error";
    public static final String USER_ID_ATTR = "userId";
    public static final String ACCOUNT_ATTR = "account";

    /**
     * The command replenishes balance of current users account.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        if (request.getSession().getAttribute(USER_ID_ATTR) != null && request.getParameter(SUM_ATTR) != null) {
            AccountService accountService = new AccountServiceImpl(AccountDAOImpl.getInstance());
            try {
                long userId = Integer.parseInt(request.getSession().getAttribute(USER_ID_ATTR).toString());
                if (request.getParameter(SUM_ATTR) != null && !request.getParameter(SUM_ATTR).isEmpty()) {
                    double sum = Double.parseDouble(request.getParameter(SUM_ATTR).toString());
                    if (!accountService.increaseBalance(userId, sum)) {
                        request.setAttribute(ERROR_ATTR, true);
                    }
                }
                request.setAttribute(ACCOUNT_ATTR, accountService.findByUserId(userId));

            } catch (NumberFormatException e) {
                LOG.error(e);
                request.setAttribute(ERROR_ATTR, true);
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
        }
        return PagesNames.ACCOUNT_PAGE_KEY;
    }
}
