package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * The command deletes a drink.
 */
public class DeleteDrinkCommand implements Command {

    public static final String DRINK_ID_PARAM = "drinkId";
    public static final String DELETE_ERROR_ATTR = "deleteError";
    public static final String DELETE_SUCCESS_ATTR = "deleteSuccess";
    public static final String DRINKS_ATTR = "drinks";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    /**
     * The command deletes a drink.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        String page = PagesNames.MENU_PAGE_KEY;
        long drinkId = Long.parseLong(request.getParameter(DRINK_ID_PARAM));
        DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
        try {
            Drink chosenDrink = drinkService.find(drinkId);
            if (drinkService.canDelete(chosenDrink.getId())) {
                drinkService.delete(chosenDrink.getId());
                request.setAttribute(DELETE_SUCCESS_ATTR, true);
                request.setAttribute(DRINKS_ATTR, drinkService.findAll());
            } else {
                request.setAttribute(DELETE_ERROR_ATTR, true);
                request.setAttribute(DRINKS_ATTR, drinkService.findAll());
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return page;
    }
}
