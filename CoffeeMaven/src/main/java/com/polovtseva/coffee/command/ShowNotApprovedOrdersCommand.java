package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.OrderDAOImpl;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.OrderServiceImpl;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * The command puts a list of all orders which weren't approved into a request object.
 */
public class ShowNotApprovedOrdersCommand implements Command {

    public static final String ORDERS_ATTR = "orders";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    /**
     * The command puts a list of all orders which weren't approved into a request object.
     *
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_NOT_APPROVED_ORDERS.toString());
        OrderService orderService = new OrderServiceImpl(OrderDAOImpl.getInstance());
        try {
            List<Order> orders = orderService.findAll(false);
            request.setAttribute(ORDERS_ATTR, orders);
            return PagesNames.ORDERS_PAGE_KEY;
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
    }
}
