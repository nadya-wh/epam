package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.OrderDAOImpl;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.OrderServiceImpl;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by User on 04.02.2016.
 */
public class ShowHistoryCommand implements Command {

    public static final String USER_ID_ATTR = "userId";
    public static final String ORDERS_ATTR = "orders";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    /**
     * The command puts a list of orders for a particular user into a request object.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page with necessary data.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {

        String page = PagesNames.LOGIN_PAGE_KEY;
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.SHOW_HISTORY.toString());
        if (request.getSession().getAttribute(USER_ID_ATTR) != null) {
            long userId = Long.parseLong(request.getSession().getAttribute(USER_ID_ATTR).toString());
            OrderService orderService = new OrderServiceImpl(OrderDAOImpl.getInstance());
            try {
                List<Order> orders = orderService.findAll(userId);
                request.setAttribute(ORDERS_ATTR, orders);
                page = PagesNames.HISTORY_PAGE_KEY;
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
        }
        return page;
    }
}
