package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * The command removes an ingredient from a coffee-maker.
 */
public class RemoveIngredientCommand implements Command {

    public static final String INGREDIENT_ID_PARAM = "ingredientId";
    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String PREV_COMMAND_ATTR = "prevCommand";


    /**
     * The command removes an ingredient from a coffee-maker (sets portions left 0).
     *
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        long ingredientId = Long.parseLong(request.getParameter(INGREDIENT_ID_PARAM));
        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {
            ingredientService.updatePortionsLeft(ingredientId, 0);
            request.setAttribute(INGREDIENTS_ATTR, ingredientService.findAll());
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return PagesNames.INGREDIENTS_PAGE_KEY;
    }
}
