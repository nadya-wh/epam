package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import javax.servlet.http.HttpServletRequest;

/**
 * The command creates a new ingredient.
 */
public class CreateIngredientCommand implements Command {

    private static final Logger LOG = LogManager.getLogger(CreateIngredientCommand.class);

    public static final String INGREDIENT_NAME_PARAM = "ingredientName";
    public static final String INGREDIENT_PRICE_PARAM = "ingredientPrice";
    public static final String IMAGE_URL_PARAM = "imageUrl";
    public static final String PORTIONS_LEFT_PARAM = "portionsLeft";
    public static final String ADD_INGREDIENT_ERROR_ATTR = "error";
    public static final String ADD_INGREDIENT_SUCCESS_ATTR = "success";
    public static final String VALID_ERROR = "validationError";
    public static final String PREV_PAGE = "prevPage";

    /**
     * The command creates a new ingredient.
     * @param request request object that contains the request the client has made of the servlet
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_PAGE, PagesNames.MENU_INGREDIENTS_URL);
        String page = PagesNames.ADD_INGREDIENT_PAGE_KEY;
        String ingredientName = request.getParameter(INGREDIENT_NAME_PARAM);
        String imageUrl = request.getParameter(IMAGE_URL_PARAM);
        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {
            double ingredientPrice = Double.parseDouble(request.getParameter(INGREDIENT_PRICE_PARAM));
            int left = Integer.parseInt(request.getParameter(PORTIONS_LEFT_PARAM));
            Ingredient ingredient = new Ingredient(0, ingredientName, ingredientPrice, imageUrl, left);
            ingredientService.create(ingredient);
            request.setAttribute(ADD_INGREDIENT_SUCCESS_ATTR, true);
        } catch (ServiceException e) {
            throw new CommandException(e);
        } catch (NumberFormatException e) {
            LOG.error(e);
            request.setAttribute(VALID_ERROR, true);
        }
        return page;
    }
}
