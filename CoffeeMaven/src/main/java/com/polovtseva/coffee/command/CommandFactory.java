package com.polovtseva.coffee.command;


import javax.servlet.http.HttpServletRequest;

/**
 * Creates a command.
 */
public class CommandFactory {

    public static final String COMMAND_PARAM = "command";
    public static final String WRONG_ACTION_ATTRIBUTE_NAME = "error";

    public static Command defineCommand(HttpServletRequest request) {
        String commandName = request.getParameter(COMMAND_PARAM);
        Command currentCommand = new EmptyCommand();
        if (commandName != null) {
            try {
                CommandEnum commandEnum = CommandEnum.valueOf(commandName.toUpperCase());
                currentCommand = commandEnum.getCommand();
            } catch (IllegalArgumentException e) {
                request.setAttribute(WRONG_ACTION_ATTRIBUTE_NAME, true);
            }
        }
        return currentCommand;
    }

}
