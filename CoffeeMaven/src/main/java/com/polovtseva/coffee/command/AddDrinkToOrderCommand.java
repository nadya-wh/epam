package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.command.util.CommandUtil;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;


import javax.servlet.http.HttpServletRequest;

/**
 * The command adds a particular drink to current order.
 */
public class AddDrinkToOrderCommand implements Command {

    public static final String CURRENT_DRINK_ATTR = "currentDrink";
    public static final String CURRENT_ORDER_ATTR = "currentOrder";
    public static final String DRINKS_ATTR = "drinks";
    public static final String ADD_TO_ORDER_SUCCESS_ATTR = "success";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * The command adds a particular drink to current order.
     *
     * @param request object that contains the request the client has made of the servlet.
     * @return page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_DRINKS_LIST.toString());
        String page = PagesNames.INGREDIENTS_PAGE_KEY;

        if (request.getSession().getAttribute(CURRENT_DRINK_ATTR) != null) {
            Drink drink = (Drink) request.getSession().getAttribute(CURRENT_DRINK_ATTR);
            Order order = CommandUtil.takeOrder(request);
            order.addDrink(drink);
            request.setAttribute(ADD_TO_ORDER_SUCCESS_ATTR, true);
            request.getSession().setAttribute(CURRENT_ORDER_ATTR, order);
            request.getSession().setAttribute(CURRENT_DRINK_ATTR, null);
            page = PagesNames.MENU_PAGE_KEY;
            DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
            try {
                request.setAttribute(DRINKS_ATTR, drinkService.findAll());
            } catch (ServiceException e) {
                throw new CommandException(e);
            }
        }
        return page;
    }


}
