package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Command.
 */
public interface Command {
    String execute(HttpServletRequest request) throws CommandException;

}
