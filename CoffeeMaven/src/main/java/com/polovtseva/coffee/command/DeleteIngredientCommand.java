package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.IngredientServiceImpl;


import javax.servlet.http.HttpServletRequest;

/**
 * Created by User on 08.02.2016.
 */
public class DeleteIngredientCommand implements Command {

    public static final String INGREDIENT_ID_PARAM = "ingredientId";
    public static final String DELETE_ERROR_ATTR = "deleteError";
    public static final String DELETE_SUCCESS_ATTR = "deleteSuccess";
    public static final String INGREDIENTS_ATTR = "ingredients";
    public static final String PREV_COMMAND_ATTR = "prevCommand";

    /**
     * Deletes ingredient.
     * @param request request object that contains the request the client has made of the servlet.
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        request.getSession().setAttribute(PREV_COMMAND_ATTR, CommandEnum.GET_INGREDIENTS_LIST.toString());
        String page = PagesNames.INGREDIENTS_PAGE_KEY;
        long ingredientId = Long.parseLong(request.getParameter(INGREDIENT_ID_PARAM));
        IngredientService ingredientService = new IngredientServiceImpl(IngredientDAOImpl.getInstance());
        try {
            if (ingredientService.canDelete(ingredientId)) {
                ingredientService.delete(ingredientId);
                request.setAttribute(DELETE_SUCCESS_ATTR, true);
            } else {
                request.setAttribute(DELETE_ERROR_ATTR, true);
            }
            request.setAttribute(INGREDIENTS_ATTR, ingredientService.findAll());
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return page;
    }
}
