package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.DrinkServiceImpl;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import javax.servlet.http.HttpServletRequest;

/**
 * The command creates a drink.
 */
public class CreateDrinkCommand implements Command {

    private static final Logger LOG = LogManager.getLogger(CreateDrinkCommand.class);

    public static final String DRINK_NAME_PARAM = "drinkName";
    public static final String DRINK_PRICE_PARAM = "drinkPrice";
    public static final String IMAGE_URL_PARAM = "imageUrl";
    public static final String PORTIONS_LEFT_PARAM = "portionsLeft";
    public static final String CREATE_DRINK_ERROR_ATTR = "error";
    public static final String CREATE_DRINK_SUCCESS_ATTR = "success";
    public static final String VALID_ERROR_ATTR = "validationError";
    public static final String PREV_PAGE = "prevPage";


    /**
     * The command creates a drink.
     * @param request request object that contains the request the client has made of the servlet
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PagesNames.ADD_DRINK_PAGE_KEY;
        request.getSession().setAttribute(PREV_PAGE, PagesNames.DRINKS_URL);
        String drinkName = request.getParameter(DRINK_NAME_PARAM);
        String imageUrl = request.getParameter(IMAGE_URL_PARAM);
        try {
            int portionsLeft = Integer.parseInt(request.getParameter(PORTIONS_LEFT_PARAM));
            double drinkPrice = Double.parseDouble(request.getParameter(DRINK_PRICE_PARAM));
            Drink drink = new Drink(0, drinkName, portionsLeft, drinkPrice, imageUrl);
            DrinkService drinkService = new DrinkServiceImpl(DrinkDAOImpl.getInstance());
            if (drinkService.create(drink)) {
                request.setAttribute(CREATE_DRINK_SUCCESS_ATTR, true);
            } else {
                request.setAttribute(CREATE_DRINK_ERROR_ATTR, true);
            }
        } catch (ServiceException e) {
           throw new CommandException(e);
        } catch (NumberFormatException e) {
            LOG.error(e);
            request.setAttribute(VALID_ERROR_ATTR, true);
        }
        return page;

    }
}
