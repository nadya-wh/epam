package com.polovtseva.coffee.command;

import com.polovtseva.coffee.command.exception.CommandException;
import com.polovtseva.coffee.dao.impl.UserDAOImpl;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;
import com.polovtseva.coffee.service.UserService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.impl.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;

/**
 * The command lets sign up a new user.
 */
public class SignUpCommand implements Command {

    public static final String LOGIN_PARAM_NAME = "username";
    public static final String PASSWORD_PARAM_NAME = "password";
    public static final String CONFIRM_PASSWORD_PARAM_NAME = "confirm-password";
    public static final String FIRST_NAME_PARAM_NAME = "firstname";
    public static final String LAST_NAME_PARAM_NAME = "lastname";
    public static final String ERROR_ATTR = "error";
    public static final String CONFIRM_PASSWORD_ERROR = "confirmError";


    /**
     * The command lets sign up a new user.
     * @param request request object that contains the request the client has made of the servlet
     * @return next page.
     */
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page = PagesNames.SIGN_UP_URL;
        String username = request.getParameter(LOGIN_PARAM_NAME);
        String password = request.getParameter(PASSWORD_PARAM_NAME);
        String confirmPassword = request.getParameter(CONFIRM_PASSWORD_PARAM_NAME);
        String firstName = request.getParameter(FIRST_NAME_PARAM_NAME);
        String lastName = request.getParameter(LAST_NAME_PARAM_NAME);
        if (!password.equals(confirmPassword)) {
            request.setAttribute(CONFIRM_PASSWORD_ERROR, true);
            return page;
        }
        User user = new User(username, password, firstName, lastName, UserRole.USER);
        UserService userService = new UserServiceImpl(UserDAOImpl.getInstance());
        try {
            if (userService.create(user)) {
                page = PagesNames.LOGIN_PAGE_KEY;
            } else {
                request.setAttribute(ERROR_ATTR, true);
                return page;
            }
        } catch (ServiceException e) {
            throw new CommandException(e);
        }
        return page;
    }
}
