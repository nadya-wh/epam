package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.UserDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;
import com.polovtseva.coffee.service.UserService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.service.util.MD5Hash;
import com.polovtseva.coffee.util.Validator;

import java.util.List;

/**
 * Implementation of user service interface.
 */
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public User authorisation(String username, String password) throws ServiceException {
        String hash = MD5Hash.countHash(password);
        try {
            if (userDAO.authenticate(username, hash)) {
                User user = userDAO.findOne(username);
                if (user != null && !UserRole.BLOCKED.equals(user.getRole())) {
                    return user;
                }
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return null;
    }

    public boolean create(User user) throws ServiceException {
        if (user == null || !Validator.validateUser(user)) {
            return false;
        }
        try {
            if (userDAO.findOne(user.getLogin()) != null) {
                return false;
            }
            user.setPassword(MD5Hash.countHash(user.getPassword()));
            return userDAO.create(user) == 1;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public List<User> readAll() throws ServiceException {
        try {
            return userDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public User read(long id) throws ServiceException {
        try {
            return userDAO.findOne(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean update(User user, UserRole role) throws ServiceException {
        if (user == null) {
            return false;
        }
        try {
            return userDAO.update(user, role) == 1;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
