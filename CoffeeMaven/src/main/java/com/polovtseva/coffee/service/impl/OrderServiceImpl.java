package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.OrderDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.dao.impl.DrinkDAOImpl;
import com.polovtseva.coffee.dao.impl.IngredientDAOImpl;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.OrderService;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.List;

/**
 * Implementation of order service interface.
 */
public class OrderServiceImpl implements OrderService {
    private OrderDAO orderDAO;


    public OrderServiceImpl(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;

    }

    @Override
    public List<Order> findAll(long userId) throws ServiceException {
        try {
            List<Order> orders = orderDAO.findAllForUser(userId);
            findAdditionalInfoForOrders(orders);
            return orders;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean create(Order order) throws ServiceException {
        if (order == null) {
            return false;
        }
        try {
            if (orderDAO.create(order) == 1) {
                order.setId(orderDAO.findOne(order.getUserId(), order.getOrderDate()));
                return true;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return false;
    }

    @Override
    public List<Order> findAll(boolean approved) throws ServiceException {
        try {
            List<Order> orders = orderDAO.findAll(approved);
            findAdditionalInfoForOrders(orders);

            return orders;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Order> findAll(long userId, boolean approved) throws ServiceException {
        try {
            List<Order> orders = orderDAO.findAll(userId, approved);
            findAdditionalInfoForOrders(orders);

            return orders;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean approveOrders(long[] orderIds) throws ServiceException {
        try {
            int[] res = orderDAO.approve(orderIds);
            for (int i = 0; i < res.length; i++) {
                if (res[i] != 1) {
                    return false;
                }
            }
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    private void findAdditionalInfoForOrders(List<Order> orders) throws DAOException {
        for (Order order : orders) {
            List<Drink> drinks = DrinkDAOImpl.getInstance().findAllInOrder(order.getId());
            for (Drink drink : drinks) {
                List<Ingredient> ingredients = IngredientDAOImpl.getInstance().findAll(order.getId(), drink.getId());
                drink.setIngredients(ingredients);
            }
            order.setDrinks(drinks);
        }
    }

}
