package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.AccountDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Account;
import com.polovtseva.coffee.service.AccountService;
import com.polovtseva.coffee.service.exception.ServiceException;

/**
 * Implementation of account service interface.
 */
public class AccountServiceImpl implements AccountService {

    private AccountDAO accountDAO;

    public AccountServiceImpl(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }


    public Account findByUserId(long userId) throws ServiceException {
        try {
            return accountDAO.findOneByUserId(userId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean withdrawMoney(Account account, double sum) throws ServiceException {
        try {
            return account.getBalance() >= sum &&
                    accountDAO.updateBalance(account, account.getBalance() - sum) == 1;

        } catch (DAOException e) {
            throw new ServiceException(e);
        }

    }

    public boolean increaseBalance(long userId, double sum) throws ServiceException {
        if (sum < 0) {
            return false;
        }
        Account account = findByUserId(userId);
        sum += account.getBalance();
        try {
            return accountDAO.updateBalance(account, sum) == 1;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
