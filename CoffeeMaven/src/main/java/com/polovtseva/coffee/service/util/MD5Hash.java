package com.polovtseva.coffee.service.util;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Computes the MD5 hash of a string.
 */
public class MD5Hash {

    private final static Logger LOG = LogManager.getLogger(MD5Hash.class);
    public static final String ALGORITHM = "MD5";
    public static final String CHARSET_NAME = "UTF-8";

    /**
     * Computes the MD5 hash of a string.
     * @param password a string to compute.
     * @return MD5 hash of a string.
     */
    public static String countHash(String password) {
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance(ALGORITHM);
            byte[] digest = messageDigest.digest(password.getBytes(CHARSET_NAME));
            BigInteger bigInt = new BigInteger(1, digest);
            return bigInt.toString(16);

        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            LOG.error(e);
        }
        return password;
    }
}
