package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.List;

/**
 * Ingredient service interface for performing operations on ingredients.
 */
public interface IngredientService {

    /**
     * Creates an ingredient.
     *
     * @param ingredient to create.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean create(Ingredient ingredient) throws ServiceException;

    /**
     * Reads all ingredients.
     *
     * @return a list of ingredients.
     * @throws ServiceException
     */
    List<Ingredient> findAll() throws ServiceException;

    /**
     * Finds a specified ingredient.
     *
     * @param id ingredient's id.
     * @return ingredient.
     * @throws ServiceException
     */
    Ingredient find(long id) throws ServiceException;

    /**
     * Updates a number of portions.
     *
     * @param id    ingredient's id.
     * @param count new number of portions.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean updatePortionsLeft(long id, int count) throws ServiceException;

    /**
     * Deletes a specified ingredient.
     *
     * @param id ingredient's id.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean delete(long id) throws ServiceException;

    /**
     * Checks if a drink can be deleted.
     * @param id drink's id.
     * @return true, if operation is possible; false, otherwise.
     * @throws ServiceException
     */
    boolean canDelete(long id) throws ServiceException;

    boolean checkEnough(Order order) throws ServiceException;

    boolean removePortions(Order order) throws ServiceException;

}
