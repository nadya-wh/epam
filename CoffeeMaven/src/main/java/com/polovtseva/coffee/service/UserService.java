package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.List;

/**
 * User service interface for performing operations on users.
 */
public interface UserService {

    /**
     * Checks user's credentials.
     * @param username login.
     * @param password password.
     * @return a valid user, if authorisation succeeds; null otherwise.
     * @throws ServiceException
     */
    User authorisation(String username, String password) throws ServiceException;

    /**
     * Creates a user.
     * @param user to create.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean create(User user) throws ServiceException;

    /**
     * Reads all users.
     * @return a list of users.
     * @throws ServiceException
     */
    List<User> readAll() throws ServiceException;

    /**
     * Reads a user by id.
     * @param id user's id.
     * @return a user.
     * @throws ServiceException
     */
    User read(long id) throws ServiceException;

    /**
     * Updates user's role.
     * @param user to update.
     * @param role new role.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean update(User user, UserRole role) throws ServiceException;

}
