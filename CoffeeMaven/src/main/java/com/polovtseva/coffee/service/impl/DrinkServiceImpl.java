package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.DrinkDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.DrinkService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.util.Validator;

import java.util.*;

/**
 * Implementation of drink service interface.
 */
public class DrinkServiceImpl implements DrinkService {

    private DrinkDAO drinkDAO;

    public DrinkServiceImpl(DrinkDAO drinkDAO) {
        this.drinkDAO = drinkDAO;
    }

    public List<Drink> findAll() throws ServiceException {
        try {
            List<Drink> drinks = drinkDAO.findAll();
            return drinks;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean create(Drink drink) throws ServiceException {
        if (!Validator.validateDrink(drink)) {
            return false;
        }
        try {
            return drinkDAO.create(drink) == 1;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public Drink find(long drinkId) throws ServiceException {
        try {
            return drinkDAO.findOne(drinkId);
        } catch (DAOException e) {
            throw new ServiceException(e);

        }
    }

    public boolean updatePortionsLeft(long drinkId, int portionsLeft) throws ServiceException {
        try {
            return drinkDAO.updatePortions(drinkId, portionsLeft) == 1;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean delete(long drinkId) throws ServiceException {
        try {
            if (canDelete(drinkId)) {
                return drinkDAO.delete(drinkId) > 0;
            }
            return false;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    public boolean canDelete(long drinkId) throws ServiceException {
        try {
            return drinkDAO.countInOrders(drinkId) == 0;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    public boolean removePortions(Order order) throws ServiceException {
        try {
            Map<Long, Integer> info = prepareInfo(order);
            if (info != null) {
                drinkDAO.updatePortions(info);
                return true;
            }
            return false;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    private Map<Long, Integer> prepareInfo(Order order) throws DAOException {
        Map<Long, Integer> values = new HashMap<>();//id, number
        for (Drink drink : order.getDrinks()) {
            if (values.containsKey(drink.getId())) {
                int count = values.get(drink.getId());
                count += drink.getNumberInOrder();
                values.put(drink.getId(), count);
            } else {
                values.put(drink.getId(), drink.getNumberInOrder());
            }
        }
        for (Long id : values.keySet()) {
            Drink drink = drinkDAO.findOne(id);
            if (drink.getPortionsLeft() - values.get(id) >= 0) {
                values.put(id, drink.getPortionsLeft() - values.get(id));
            } else {
                return null;
            }
        }
        return values;
    }

    public boolean checkEnough(Order order) throws ServiceException {
        Map<Long, Integer> values;
        try {
            values = prepareInfo(order);
            return values != null;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
