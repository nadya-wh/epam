package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.Account;
import com.polovtseva.coffee.service.exception.ServiceException;

/**
 * Account service interface for performing operations on accounts.
 */
public interface AccountService {
    /**
     * Finds account by user id.
     * @param userId user's id.
     * @return account.
     * @throws ServiceException
     */
    Account findByUserId(long userId) throws ServiceException;

    /**
     * Changes account's balance.
     * @param account
     * @param sum to withdraw.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean withdrawMoney(Account account, double sum) throws ServiceException;

    /**
     * Increases balance.
     * @param userId user's id to whom account belongs.
     * @param sum to increase.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean increaseBalance(long userId, double sum) throws ServiceException;

}
