package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.List;

/**
 * Drink service interface for performing operations on drinks.
 */
public interface DrinkService {

    /**
     * Reads all drinks.
     * @return list of drinks.
     * @throws ServiceException
     */
    List<Drink> findAll() throws ServiceException;

    /**
     * Creates a drink.
     * @param drink to create.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean create(Drink drink) throws ServiceException;

    /**
     * Reads a specified drink.
     * @param drinkId drink's id.
     * @return drink.
     * @throws ServiceException
     */
    Drink find(long drinkId) throws ServiceException;

    /**
     * Updates a number of portions.
     * @param drinkId drink's id.
     * @param portionsLeft new number of portions.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean updatePortionsLeft(long drinkId, int portionsLeft) throws ServiceException;

    /**
     * Deletes a specifies drink.
     * @param drinkId which drink to delete.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean delete(long drinkId) throws ServiceException;

    /**
     * Removes portions.
     * @param order order object.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean removePortions(Order order) throws ServiceException;

    boolean canDelete(long drinkId) throws ServiceException;

    boolean checkEnough(Order order) throws ServiceException;
}
