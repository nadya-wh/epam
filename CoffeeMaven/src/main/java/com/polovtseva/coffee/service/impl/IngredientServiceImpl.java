package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.IngredientDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.IngredientService;
import com.polovtseva.coffee.service.exception.ServiceException;
import com.polovtseva.coffee.util.Validator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of ingredient service interface.
 */
public class IngredientServiceImpl implements IngredientService {

    private IngredientDAO ingredientDAO;

    public IngredientServiceImpl(IngredientDAO ingredientDAO) {
        this.ingredientDAO = ingredientDAO;
    }

    @Override
    public boolean create(Ingredient ingredient) throws ServiceException {
        if (ingredient == null ||
                !Validator.validateIngredient(ingredient)) {
            return false;
        }
        try {
            return ingredientDAO.create(ingredient) == 1;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Ingredient> findAll() throws ServiceException {
        try {
            return ingredientDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Ingredient find(long id) throws ServiceException {
        try {
            return ingredientDAO.findOne(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean updatePortionsLeft(long id, int count) throws ServiceException {
        try {
            return ingredientDAO.updatePortions(id, count) == 1;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean delete(long id) throws ServiceException {
        try {
            if (!canDelete(id)) {
                return false;
            }
            return ingredientDAO.delete(id) > 0;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean canDelete(long id) throws ServiceException {
        try {
            if (ingredientDAO.countIngredientInOrders(id) != 0) {
                return false;
            }
            return true;
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean checkEnough(Order order) throws ServiceException {
        try {
            Map<Long, Integer> values = prepareInfo(order);
            if (values != null) {
                return true;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }

        return false;
    }

    @Override
    public boolean removePortions(Order order) throws ServiceException {
        try {
            Map<Long, Integer> values = prepareInfo(order);
            if (values != null) {
                int[] result = ingredientDAO.updatePortions(values);
                for (int r : result) {
                    if (r != 1) {
                        return false;
                    }
                }
                return true;
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
        return false;
    }

    private Map<Long, Integer> prepareInfo(Order order) throws DAOException {
        Map<Long, Integer> values = new HashMap<>();//id, number
        for (Drink drink : order.getDrinks()) {
            for (Ingredient ingredient : drink.getIngredients()) {
                if (values.containsKey(ingredient.getId())) {
                    int count = values.get(ingredient.getId());
                    count += ingredient.getNumberInDrink();
                    values.put(ingredient.getId(), count);
                } else {
                    values.put(ingredient.getId(), ingredient.getNumberInDrink());
                }
            }
        }
        for (Long id : values.keySet()) {
            Ingredient ingredient = ingredientDAO.findOne(id);
            if (ingredient.getPortionsLeft() - values.get(id) >= 0) {
                values.put(id, ingredient.getPortionsLeft() - values.get(id));
            } else {
                return null;
            }
        }
        return values;
    }

}
