package com.polovtseva.coffee.service;

import com.polovtseva.coffee.domain.Order;
import com.polovtseva.coffee.service.exception.ServiceException;

import java.util.List;

/**
 * Order service interface for performing operations on orders.
 */
public interface OrderService {

    /**
     * Reads all orders.
     * @param userId user's id.
     * @return a list of orders.
     * @throws ServiceException
     */
    List<Order> findAll(long userId) throws ServiceException;

    /**
     * Creates an order.
     * @param order to create.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean create(Order order) throws ServiceException;

    /**
     * Finds all (not)approved orders
     * @param approved orders' status.
     * @return a list of orders.
     * @throws ServiceException
     */
    List<Order> findAll(boolean approved) throws ServiceException;

    /**
     * Finds all (not)approved orders for user
     * @param userId user's id.
     * @param approved order's status.
     * @return a list of orders.
     * @throws ServiceException
     */
    List<Order> findAll(long userId, boolean approved) throws ServiceException;

    /**
     * Approves orders.
     * @param orderIds to approve.
     * @return true, if operation succeeded; false otherwise.
     * @throws ServiceException
     */
    boolean approveOrders(long[] orderIds) throws ServiceException;

}
