package com.polovtseva.coffee.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Tag for formatting date.
 */
public class LocalDateTimeTag extends TagSupport {
    private LocalDateTime value;

    public void setValue(LocalDateTime value) {
        this.value = value;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            String result = value.format(DateTimeFormatter.ISO_LOCAL_DATE);
            pageContext.getOut().write(result);
        } catch (IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }
}
