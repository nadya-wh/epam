package com.polovtseva.coffee.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * Tag for displaying a footer.
 */
public class FooterTag extends TagSupport {


    public static final String FOOTER = "<h4>&copy; Nadezhda Polovtseva</h4>";

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        try {
            out.write(FOOTER);
        } catch (IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }



}
