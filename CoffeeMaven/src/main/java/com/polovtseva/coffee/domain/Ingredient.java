package com.polovtseva.coffee.domain;

import java.io.Serializable;

/**
 * Ingredient.
 */
public class Ingredient extends Entity implements Serializable {
    private String ingredientName;
    private double ingredientPrice;
    private String imageUrl;
    private int portionsLeft;
    private int numberInDrink;

    public Ingredient() {
        super();
    }

    public Ingredient(long id, String ingredientName, double ingredientPrice, String imageUrl, int portionsLeft) {
        super(id);
        this.ingredientName = ingredientName;
        this.ingredientPrice = ingredientPrice;
        this.imageUrl = imageUrl;
        this.portionsLeft = portionsLeft;
    }

    public Ingredient(long id, String ingredientName, double ingredientPrice, String imageUrl, int portionsLeft, int numberInDrink) {
        super(id);
        this.ingredientName = ingredientName;
        this.ingredientPrice = ingredientPrice;
        this.imageUrl = imageUrl;
        this.portionsLeft = portionsLeft;
        this.numberInDrink = numberInDrink;
    }

    public String getIngredientName() {
        return ingredientName;
    }

    public void setIngredientName(String ingredientName) {
        this.ingredientName = ingredientName;
    }

    public double getIngredientPrice() {
        return ingredientPrice;
    }

    public void setIngredientPrice(double ingredientPrice) {
        this.ingredientPrice = ingredientPrice;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPortionsLeft() {
        return portionsLeft;
    }

    public void setPortionsLeft(int portionsLeft) {
        this.portionsLeft = portionsLeft;
    }

    public int getNumberInDrink() {
        return numberInDrink;
    }

    public void setNumberInDrink(int numberInDrink) {
        this.numberInDrink = numberInDrink;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ingredient that = (Ingredient) o;

        if (Double.compare(that.ingredientPrice, ingredientPrice) != 0) return false;
        if (portionsLeft != that.portionsLeft) return false;
        if (numberInDrink != that.numberInDrink) return false;
        if (ingredientName != null ? !ingredientName.equals(that.ingredientName) : that.ingredientName != null)
            return false;
        return imageUrl != null ? imageUrl.equals(that.imageUrl) : that.imageUrl == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = ingredientName != null ? ingredientName.hashCode() : 0;
        temp = Double.doubleToLongBits(ingredientPrice);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (imageUrl != null ? imageUrl.hashCode() : 0);
        result = 31 * result + portionsLeft;
        result = 31 * result + numberInDrink;
        return result;
    }
}
