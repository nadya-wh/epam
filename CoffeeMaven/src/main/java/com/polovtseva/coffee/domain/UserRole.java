package com.polovtseva.coffee.domain;

/**
 * User role enumeration.
 */
public enum UserRole {
    USER("user"),
    ADMIN("admin"),
    BLOCKED("blocked");

    String value;

    UserRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
