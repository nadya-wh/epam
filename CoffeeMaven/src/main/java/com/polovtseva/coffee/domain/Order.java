package com.polovtseva.coffee.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Order.
 */
public class Order extends Entity implements Serializable {
    private long userId;
    private LocalDateTime orderDate;
    private List<Drink> drinks;
    private boolean approved;

    public Order(int userId) {
        this.userId = userId;
        this.orderDate = LocalDateTime.now();
        drinks = new ArrayList<>();
    }

    public Order(long id, long userId, LocalDateTime orderDate, ArrayList<Drink> drinks) {
        super(id);
        this.userId = userId;
        this.orderDate = orderDate;
        this.drinks = drinks;
    }

    public Order(long id, long userId, LocalDateTime orderDate, List<Drink> drinks, boolean approved) {
        super(id);
        this.userId = userId;
        this.orderDate = orderDate;
        this.drinks = drinks;
        this.approved = approved;
    }

    public Order(long id, long userId, LocalDateTime orderDate) {
        super(id);
        this.userId = userId;
        this.orderDate = orderDate;
        this.drinks = new ArrayList<>();
    }

    public Order(long id, long userId, LocalDateTime orderDate, boolean approved) {
        super(id);
        this.userId = userId;
        this.orderDate = orderDate;
        this.approved = approved;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public LocalDateTime getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDateTime orderDate) {
        this.orderDate = orderDate;
    }

    public List<Drink> getDrinks() {
        return Collections.unmodifiableList(drinks);
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public void setDrinks(ArrayList<Drink> drinks) {
        this.drinks = drinks;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean addDrink(Drink drink) {
        return drinks.add(drink);
    }

    public boolean removeDrink(Drink drink) {
        return drinks.remove(drink);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (userId != order.userId) return false;
        if (approved != order.approved) return false;
        if (orderDate != null ? !orderDate.equals(order.orderDate) : order.orderDate != null) return false;
        return drinks != null ? drinks.equals(order.drinks) : order.drinks == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (userId ^ (userId >>> 32));
        result = 31 * result + (orderDate != null ? orderDate.hashCode() : 0);
        result = 31 * result + (drinks != null ? drinks.hashCode() : 0);
        result = 31 * result + (approved ? 1 : 0);
        return result;
    }
}
