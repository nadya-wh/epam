package com.polovtseva.coffee.domain;

import java.io.Serializable;

/**
 * Entity.
 */
public abstract class Entity implements Serializable {
    private long id;

    public Entity() {

    }

    public Entity(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


}
