package com.polovtseva.coffee.filter;

import com.polovtseva.coffee.command.PagesNames;
import com.polovtseva.coffee.domain.UserRole;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Security filter.
 */
@WebFilter(filterName = "RoleSecurityFilter",
        urlPatterns = {"/coffee/drinks", "/coffee/ingredients", "/coffee/orders", "/coffee/users"})
public class AdminSecurityFilter implements Filter {

    public static final String USER_ROLE_ATTR = "role";

    public void destroy() {
    }

    /**
     *
     * @param req request.
     * @param resp response.
     * @param chain filter chain.
     * @throws ServletException servlet exception.
     * @throws IOException exception.
     */
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpRequest = (HttpServletRequest) req;
        HttpServletResponse httpResponse = (HttpServletResponse) resp;
        if (httpRequest.getSession().getAttribute(USER_ROLE_ATTR) != null) {
            UserRole role = UserRole.valueOf(httpRequest.getSession().getAttribute(USER_ROLE_ATTR).toString().toUpperCase());
            if (role != UserRole.ADMIN) {
                httpResponse.sendError(401);
                return;
            }
        } else {
            httpResponse.sendRedirect(PagesNames.SIGN_IN_URL);
            return;
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
