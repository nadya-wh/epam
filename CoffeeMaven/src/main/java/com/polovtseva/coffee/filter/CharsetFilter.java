package com.polovtseva.coffee.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

/**
 * Encoding filter.
 */
@WebFilter(filterName = "CharsetFilter", urlPatterns = {"/*"},
        initParams = {
                @WebInitParam(name = "encoding", value = "UTF-8", description = "Encoding Param")})
public class CharsetFilter implements Filter {

    public static final String ENCODING_INIT_PARAM = "encoding";
    private String encoding;

    public void init(FilterConfig config) throws ServletException {
        encoding = config.getInitParameter(ENCODING_INIT_PARAM);
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain next)
            throws IOException, ServletException {
        String requestEncoding = request.getCharacterEncoding();
        if (!encoding.equalsIgnoreCase(requestEncoding)) {
            request.setCharacterEncoding(encoding);
        }
        next.doFilter(request, response);
    }

    public void destroy() {
    }

}
