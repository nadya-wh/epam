package com.polovtseva.coffee.dao;

import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Order;

import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 * Interface allows perform operations on database for orders.
 */
public interface OrderDAO extends GenericDAO<Order> {

    /**
     * Reads all user's orders.
     *
     * @param userId user;s id.
     * @return all user's orders.
     * @throws DAOException if a database access error occurs.
     */
    ArrayList<Order> findAllForUser(long userId) throws DAOException;



    /**
     * Reads specified entity.
     *
     * @param userId user'ss id.
     * @param date   order's date and time.
     * @return read order.
     * @throws DAOException if a database access error occurs.
     */
    long findOne(long userId, LocalDateTime date) throws DAOException;


    /**
     *
     * @param approved
     * @return
     * @throws DAOException
     */
    ArrayList<Order> findAll(boolean approved) throws DAOException;

    ArrayList<Order> findAll(long userId, boolean approved) throws DAOException;

    int[] approve(long[] orderIds) throws DAOException;

}
