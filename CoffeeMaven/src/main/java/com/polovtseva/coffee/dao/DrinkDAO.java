package com.polovtseva.coffee.dao;


import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Drink;

import java.util.ArrayList;
import java.util.Map;

/**
 * Interface allows perform operations on database for drinks.
 */
public interface DrinkDAO extends GenericDAO<Drink> {

    /**
     * Reads all entities in order.
     * @param orderId order's id.
     * @return read drinks.
     * @throws DAOException if a database access error occurs.
     */
    ArrayList<Drink> findAllInOrder(long orderId) throws DAOException;

    /**
     * Updates number of portions.
     * @param drinkId drink's id.
     * @param portionsNumber new number of portions.
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or
     * (2) 0 for SQL statements that return nothing.
     * @throws DAOException if a database access error occurs.
     */
    int updatePortions(long drinkId, int portionsNumber) throws DAOException;

    /**
     * Counts how many times the drink was ordered.
     * @param drinkId drink's id.
     * @return how many times the drink was ordered.
     * @throws DAOException if a database access error occurs.
     */
    int countInOrders(long drinkId) throws DAOException;

    int[] updatePortions(Map<Long, Integer> values) throws DAOException;

}
