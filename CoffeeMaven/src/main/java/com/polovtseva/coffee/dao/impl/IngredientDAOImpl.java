package com.polovtseva.coffee.dao.impl;

import com.polovtseva.coffee.dao.IngredientDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.dao.pool.ConnectionPool;
import com.polovtseva.coffee.domain.Ingredient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of IngredientDAO.
 */
public class IngredientDAOImpl implements IngredientDAO {

    private static final IngredientDAOImpl instance = new IngredientDAOImpl();

    private static final String SQL_SELECT_INGREDIENT = "SELECT ingredient_id, ingredient_name, " +
            "ingredient_price, image_url, portions_left FROM ingredient;";

    private static final String SQL_SELECT_INGREDIENT_BY_ID = "SELECT ingredient_id, ingredient_name, " +
            "ingredient_price, image_url, portions_left FROM ingredient WHERE ingredient_id=?;";

    private static final String SQL_COUNT_DRINKS_WITH_INGREDIENT = "SELECT COUNT(*) FROM order_ingredient " +
            "WHERE ingredient_id=?;";

    private static final String SQL_INSERT_INGREDIENT = "INSERT INTO ingredient (ingredient_name, " +
            "ingredient_price, image_url, portions_left) VALUES(?, ?, ?, ?);";

    private static final String SQL_DELETE_INGREDIENT = "DELETE FROM ingredient WHERE ingredient_id=?;";

    private static final String SQL_UPDATE_INGREDIENT_PORTIONS = "UPDATE ingredient SET portions_left=? WHERE " +
            "ingredient_id=?";

    private static final String SQL_SELECT_IN_ORDER_DRINK = "SELECT ingredient_id, ingredient_name, " +
            "ingredient_price, image_url, portions_left, portions_count FROM ingredient NATURAL JOIN " +
            "order_ingredient WHERE order_id=? AND drink_id=?;";

    private static final String SQL_COUNT_INGREDIENTS = "SELECT COUNT(*) FROM order_ingredient " +
            "WHERE ingredient_id =?;";


    private IngredientDAOImpl() {
    }

    /**
     * Returns an IngredientDAOImpl object.
     *
     * @returnan IngredientDAOImpl object.
     */
    public static IngredientDAOImpl getInstance() {
        return instance;
    }


    @Override
    public List<Ingredient> findAll() throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_INGREDIENT);
        ) {
            List<Ingredient> ingredients = new ArrayList<>();
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int ingredientId = resultSet.getInt("ingredient_id");
                String ingredientName = resultSet.getString("ingredient_name");
                double ingredientPrice = resultSet.getDouble("ingredient_price");
                String imageUrl = resultSet.getString("image_url");
                int left = resultSet.getInt("portions_left");
                ingredients.add(new Ingredient(ingredientId, ingredientName, ingredientPrice, imageUrl, left));
            }
            return ingredients;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Ingredient findOne(long id) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_INGREDIENT_BY_ID);
        ) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                long ingredientId = resultSet.getLong("ingredient_id");
                String ingredientName = resultSet.getString("ingredient_name");
                double ingredientPrice = resultSet.getDouble("ingredient_price");
                String imageUrl = resultSet.getString("image_url");
                int left = resultSet.getInt("portions_left");
                return new Ingredient(ingredientId, ingredientName, ingredientPrice, imageUrl, left);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    @Override
    public int delete(long id) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement countStatement = connection.prepareStatement(SQL_COUNT_DRINKS_WITH_INGREDIENT);
                PreparedStatement deleteStatement = connection.prepareStatement(SQL_DELETE_INGREDIENT);
        ) {
            countStatement.setLong(1, id);
            ResultSet resultSet = countStatement.executeQuery();
            int count = -1;
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            if (count == 0) {
                deleteStatement.setLong(1, id);
                return deleteStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return 0;
    }

    @Override
    public int delete(Ingredient entity) throws DAOException {
        return delete(entity.getId());
    }

    @Override
    public int create(Ingredient entity) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_INGREDIENT);
        ) {
            preparedStatement.setString(1, entity.getIngredientName());
            preparedStatement.setDouble(2, entity.getIngredientPrice());
            preparedStatement.setString(3, entity.getImageUrl());
            preparedStatement.setInt(4, entity.getPortionsLeft());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }


    @Override
    public int updatePortions(long ingredientId, int portionsNumber) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_UPDATE_INGREDIENT_PORTIONS);
        ) {
            preparedStatement.setLong(2, ingredientId);
            preparedStatement.setInt(1, portionsNumber);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<Ingredient> findAll(long orderId, long drinkId) throws DAOException {
        List<Ingredient> ingredients = new ArrayList<>();
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_SELECT_IN_ORDER_DRINK)
        ) {
            preparedStatement.setLong(1, orderId);
            preparedStatement.setLong(2, drinkId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long ingredientId = resultSet.getLong("ingredient_id");
                String ingredientName = resultSet.getString("ingredient_name");
                double ingredientPrice = resultSet.getDouble("ingredient_price");
                String imageUrl = resultSet.getString("image_url");
                int left = resultSet.getInt("portions_left");
                int numberInDrink = resultSet.getInt("portions_count");
                ingredients.add(new Ingredient(ingredientId, ingredientName, ingredientPrice, imageUrl,
                        left, numberInDrink));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return ingredients;
    }

    @Override
    public int countIngredientInOrders(long ingredientId) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_COUNT_INGREDIENTS);
        ) {
            preparedStatement.setLong(1, ingredientId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
            return 0;
        } catch (SQLException e) {
            throw new DAOException(e);
        }

    }

    @Override
    public int[] updatePortions(Map<Long, Integer> values) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_INGREDIENT_PORTIONS);
        ) {
            for (Long id : values.keySet()) {
                preparedStatement.setLong(2, id);
                preparedStatement.setInt(1, values.get(id));
                preparedStatement.addBatch();
            }
            return preparedStatement.executeBatch();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
