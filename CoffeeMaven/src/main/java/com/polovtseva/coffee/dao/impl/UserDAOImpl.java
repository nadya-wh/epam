package com.polovtseva.coffee.dao.impl;

import com.polovtseva.coffee.dao.UserDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.dao.pool.ConnectionPool;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of UserDAO.
 */
public class UserDAOImpl implements UserDAO {

    private static UserDAOImpl instance = new UserDAOImpl();

    private static final String SQL_SELECT_ALL = "SELECT user_id, role_name, username, password, first_name, last_name " +
            "FROM user u JOIN role r ON (u.role = r.role_id)";

    private static final String SQL_CHECK_USER_EXIST = "SELECT user_id FROM user WHERE username = ? AND password = ?;";
    private static final String SQL_INSERT_USER = "INSERT INTO user(role, username, password, " +
            "first_name, last_name) VALUES (?, ?, ?, ?, ?);";
    private static final String SQL_SELECT_ROLE_ID = "SELECT role_id FROM role WHERE role_name=?;";
    private static final String SQL_SELECT_USER_BY_ID = "SELECT role_name, username, password, first_name, last_name " +
            "FROM user u JOIN role r ON (u.role = r.role_id) WHERE user_id=?;";
    private static final String SQL_SELECT_USER_BY_USERNAME = "SELECT user_id, role_name, username, password, first_name, last_name " +
            "FROM user u JOIN role r ON (u.role = r.role_id) WHERE username=?;";
    private static final String SQL_DELETE_USER_BY_ID = "DELETE FROM user WHERE user_id=?;";

    private static final String SQL_UPDATE_ROLE = "UPDATE user SET role=? WHERE user_id=?;";

    private UserDAOImpl() {
    }

    /**
     * Returns an UserDAOImpl object.
     *
     * @returnan UserDAOImpl object.
     */
    public static UserDAOImpl getInstance() {
        return instance;
    }

    @Override
    public List<User> findAll() throws DAOException {
        List<User> users = new ArrayList<>();
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL)
        ) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                long userId = resultSet.getLong("user_id");
                UserRole userRole = UserRole.valueOf(resultSet.getString("role_name").toUpperCase());
                String login = resultSet.getString("username");
                String password = resultSet.getString("password");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                users.add(new User(userId, login, password, firstName, lastName, userRole));
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return users;
    }

    @Override
    public User findOne(long id) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_ID)
        ) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                UserRole userRole = UserRole.valueOf(resultSet.getString("role_name").toUpperCase());
                String login = resultSet.getString("username");
                String password = resultSet.getString("password");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                return new User(id, login, password, firstName, lastName, userRole);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    @Override
    public int delete(long id) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER_BY_ID);
        ) {
            preparedStatement.setLong(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public int delete(User entity) {
        return 0;
    }

    @Override
    public int create(User entity) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER);
        ) {
            int role = findRoleId(entity.getRole());
            preparedStatement.setInt(1, role);
            preparedStatement.setString(2, entity.getLogin());
            preparedStatement.setString(3, entity.getPassword());
            preparedStatement.setString(4, entity.getFirstName());
            preparedStatement.setString(5, entity.getLastName());
            return preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }


    @Override
    public boolean authenticate(String login, String password) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement statement = connection.prepareStatement(SQL_CHECK_USER_EXIST);
        ) {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return true;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return false;
    }

    @Override
    public User findOne(String username) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_USER_BY_USERNAME);
        ) {
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                UserRole userRole = UserRole.valueOf(resultSet.getString("role_name").toUpperCase());
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                long userId = resultSet.getInt("user_id");
                return new User(userId, username, null, firstName, lastName, userRole);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    @Override
    public int update(User user, UserRole role) throws DAOException {
        long roleId = findRoleId(role);
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_ROLE);
        ) {
            preparedStatement.setLong(1, roleId);
            preparedStatement.setLong(2, user.getId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    private int findRoleId(UserRole role) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ROLE_ID);
        ) {
            preparedStatement.setString(1, role.getValue());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("role_id");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return -1;
    }
}
