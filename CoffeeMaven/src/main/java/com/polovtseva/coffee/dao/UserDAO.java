package com.polovtseva.coffee.dao;

import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;

/**
 * Interface allows perform operations on database for users.
 */
public interface UserDAO extends GenericDAO<User> {

    /**
     * Authenticates user.
     * @param login user's login.
     * @param password user's password.
     * @return true, if user with such credentials exists, false otherwise.
     */
    boolean authenticate(String login, String password) throws DAOException;

    /**
     * Reades a user with specified username.
     * @param username
     * @return read user.
     * @throws DAOException if a database access error occurs.
     */
    User findOne(String username) throws DAOException;

    /**
     * Updates user, sets new role.
     * @param user to update.
     * @param role to set.
     * @return or each order either (1) the row count for SQL Data Manipulation Language (DML) statements or
     * (2) 0 for SQL statements that return nothing.
     * @throws DAOException if a database access error occurs.
     */
    int update(User user, UserRole role) throws DAOException;

}
