package com.polovtseva.coffee.dao.impl;

import com.polovtseva.coffee.dao.DrinkDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.dao.pool.ConnectionPool;
import com.polovtseva.coffee.domain.Drink;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of DrinkDAO.
 */
public class DrinkDAOImpl implements DrinkDAO {

    private static final DrinkDAOImpl instance = new DrinkDAOImpl();

    private static final String SQL_SELECT_DRINKS = "SELECT drink_id, drink_name, " +
            "portions_left, drink_price, image_url FROM drink;";

    private static final String SQL_SELECT_DRINKS_IN_ORDER = "SELECT drink_id, drink_name, " +
            "portions_left, drink_price, image_url, portions_count FROM drink NATURAL JOIN order_drink WHERE order_id=?;";

    private static final String SQL_INSERT_DRINK = "INSERT INTO drink (drink_name, portions_left, " +
            "drink_price, image_url) VALUES(?, ?, ?, ?);";

    private static final String SQL_SELECT_DRINK_BY_ID = "SELECT drink_id, drink_name, portions_left, drink_price, " +
            "image_url FROM drink WHERE drink_id=?;";

    private static final String SQL_UPDATE_PORTIONS = "UPDATE drink SET portions_left=? WHERE " +
            "drink_id=?";

    private static final String SQL_DELETE_DRINK_BY_ID = "DELETE FROM drink WHERE drink_id=?;";

    private static final String SQL_COUNT_DRINKS_IN_ORDER = "SELECT COUNT(*) FROM drink " +
            "NATURAL JOIN order_drink WHERE drink_id=?;";

    private DrinkDAOImpl() {
    }

    /**
     * Returns an DrinkDAOImpl object.
     *
     * @returnan DrinkDAOImpl object.
     */
    public static DrinkDAOImpl getInstance() {
        return instance;
    }

    @Override
    public List<Drink> findAll() throws DAOException {
        List<Drink> drinks;
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_DRINKS)
        ) {
            ResultSet resultSet = preparedStatement.executeQuery();
            drinks = takeDrinks(resultSet, false);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return drinks;
    }

    @Override
    public Drink findOne(long id) throws DAOException {
        Drink drink = null;
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_SELECT_DRINK_BY_ID)

        ) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                drink = takeDrink(resultSet, false);
            }

        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return drink;
    }

    @Override
    public int delete(long id) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_DELETE_DRINK_BY_ID);
        ) {
            preparedStatement.setLong(1, id);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }

    }

    @Override
    public int delete(Drink entity) throws DAOException {
        return delete(entity.getId());
    }

    @Override
    public int create(Drink entity) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_DRINK);
        ) {
            preparedStatement.setString(1, entity.getDrinkName());
            preparedStatement.setInt(2, entity.getPortionsLeft());
            preparedStatement.setDouble(3, entity.getDrinkPrice());
            preparedStatement.setString(4, entity.getImageUrl());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public ArrayList<Drink> findAllInOrder(long orderId) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_DRINKS_IN_ORDER);
        ) {
            preparedStatement.setLong(1, orderId);
            ResultSet resultSet = preparedStatement.executeQuery();
            ArrayList<Drink> drinks = takeDrinks(resultSet, true);
            return drinks;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }


    @Override
    public int updatePortions(long drinkId, int portionsNumber) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_UPDATE_PORTIONS);
        ) {
            preparedStatement.setInt(1, portionsNumber);
            preparedStatement.setLong(2, drinkId);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public int[] updatePortions(Map<Long, Integer> values) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_UPDATE_PORTIONS);
        ) {
            for (Long id : values.keySet()) {
                preparedStatement.setInt(1, values.get(id));
                preparedStatement.setLong(2, id);
                preparedStatement.addBatch();
            }
            return preparedStatement.executeBatch();

        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public int countInOrders(long drinkId) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_COUNT_DRINKS_IN_ORDER);
        ) {
            preparedStatement.setLong(1, drinkId);
            ResultSet resultSet = preparedStatement.executeQuery();
            int count = 0;
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            return count;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    private ArrayList<Drink> takeDrinks(ResultSet resultSet, boolean inOrder) throws SQLException {
        ArrayList<Drink> drinks = new ArrayList<>();
        while (resultSet.next()) {
            Drink drink = takeDrink(resultSet, inOrder);
            drinks.add(drink);
        }
        return drinks;
    }

    private Drink takeDrink(ResultSet resultSet, boolean inOrder) throws SQLException {
        long drinkId = resultSet.getLong("drink_id");
        String drinkName = resultSet.getString("drink_name");
        int portionsLeft = resultSet.getInt("portions_left");
        double drinkPrice = resultSet.getDouble("drink_price");
        String imageUrl = resultSet.getString("image_url");
        int numberInOrder = 0;
        if (inOrder) {
            numberInOrder = resultSet.getInt("portions_count");
        }
        return new Drink(drinkId, drinkName, portionsLeft, drinkPrice, imageUrl, numberInOrder);
    }


}
