package com.polovtseva.coffee.dao.pool;

import com.polovtseva.coffee.dao.pool.exception.ConnectionPoolException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Connection pool.
 */
public class ConnectionPool {

    private static final Logger LOG = LogManager.getLogger(ConnectionPool.class);

    private final BlockingQueue<ProxyConnection> freeConnections;
    private final BlockingQueue<ProxyConnection> givenConnections;

    private static final ConnectionPool INSTANCE = new ConnectionPool();
    private AtomicBoolean poolReleased = new AtomicBoolean(false);

    private static AtomicBoolean instanceInitialized = new AtomicBoolean(false);


    private ConnectionPool() {
        freeConnections = new ArrayBlockingQueue<>(ConnectionPoolConfiguration.DB_MAX_CONNECTIONS);
        givenConnections = new ArrayBlockingQueue<>(ConnectionPoolConfiguration.DB_MAX_CONNECTIONS);
    }

    /**
     * Returns connection pool instance to access database.
     *
     * @return connection pool instance
     */
    public static ConnectionPool getInstance() {
        return INSTANCE;
    }


    private ProxyConnection createConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(ConnectionPoolConfiguration.DB_URL,
                ConnectionPoolConfiguration.DB_USER_NAME,
                ConnectionPoolConfiguration.DB_PASSWORD);
        return new ProxyConnection(connection);
    }

    /**
     * Initializes connection pool.
     *
     */
    public void init() {
        LOG.info("Initializing connection pool.");
        if (instanceInitialized.compareAndSet(false, true)) {
            try {
                Class.forName(ConnectionPoolConfiguration.DB_DRIVER);
                for (int i = 0; i < ConnectionPoolConfiguration.DB_MAX_CONNECTIONS; i++) {
                    freeConnections.add(createConnection());
                }
                poolReleased.set(false);
            } catch (ClassNotFoundException | SQLException e) {
                LOG.fatal(e);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Returns a connection to database.
     *
     * @return Connection to database.
     */
    public Connection getConnection()  {
        try {
            if (!poolReleased.get()) {
                ProxyConnection connection = freeConnections.take();
                givenConnections.add(connection);
                return connection;
            }
        } catch (InterruptedException e) {
            LOG.error(e);
        }
        return null;
    }


    /**
     * This method puts connection int connection pool.
     *
     * @param connection The connection that was taken from pool.
     */
    void returnConnection(ProxyConnection connection) {
        if (connection != null) {
            freeConnections.offer(connection);
            givenConnections.remove(connection);
        }
    }

    /**
     * Releases all connections in connection pool.
     */
    public void releasePool() {
        LOG.info("Release connection pool");
        poolReleased.set(true);
        closeConnectionsQueue(freeConnections);
        closeConnectionsQueue(givenConnections);
    }

    private void closeConnectionsQueue(BlockingQueue<ProxyConnection> connections) {
        while (connections.size() != 0) {
            try {
                connections.take().destroy();
            } catch (InterruptedException | SQLException e) {
                LOG.error(e);
            }
        }
    }
}