package com.polovtseva.coffee.dao.impl;

import com.polovtseva.coffee.dao.AccountDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.dao.pool.ConnectionPool;
import com.polovtseva.coffee.domain.Account;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Implementation of AccountDAO.
 */
public class AccountDAOImpl implements AccountDAO {

    private static final AccountDAOImpl instance = new AccountDAOImpl();

    private static final String SQL_UPDATE_BALANCE = "UPDATE account SET balance=? WHERE account_id=?;";

    private static final String SQL_SELECT_ACCOUNT_BY_USER_ID = "SELECT account_id, balance FROM account " +
            "WHERE user_id=?;";

    private AccountDAOImpl() {
    }

    /**
     * Returns an AccountDAOImpl object.
     *
     * @return an AccountDAOImpl object.
     */
    public static AccountDAOImpl getInstance() {
        return instance;
    }

    @Override
    public int updateBalance(Account entity, double newBalance) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_UPDATE_BALANCE);
        ) {
            preparedStatement.setDouble(1, newBalance);
            preparedStatement.setLong(2, entity.getId());
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Account findOneByUserId(long userId) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_SELECT_ACCOUNT_BY_USER_ID);
        ) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int accountId = resultSet.getInt("account_id");
                double balance = resultSet.getDouble("balance");
                return new Account(accountId, userId, balance);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return null;
    }

    @Override
    public List<Account> findAll() throws DAOException {
        return null;
    }

    @Override
    public Account findOne(long id) throws DAOException {
        return null;
    }

    @Override
    public int delete(long id) throws DAOException {
        return 0;
    }

    @Override
    public int delete(Account entity) throws DAOException {
        return 0;
    }

    @Override
    public int create(Account entity) throws DAOException {
        return 0;
    }


}
