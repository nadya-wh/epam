package com.polovtseva.coffee.dao;

import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.domain.Entity;

import java.util.List;

/**
 * Generic DAO, describes CRUD operations.
 */
public interface GenericDAO<T extends Entity> { //, K

    /**
     * Reads all specified entities.
     * @return All entities.
     * @throws DAOException Exception is thrown if SQLException occurred.
     */
    List<T> findAll() throws DAOException;

    /**
     * Reads entity by id.
     * @param id Entity's id.
     * @return Read entity.
     * @throws DAOException Exception is thrown if SQLException occurred.
     */
    T findOne(long id) throws DAOException;

    /**
     * Deletes specified entity.
     * @param id Entity's id.
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or
     * (2) 0 for SQL statements that return nothing
     * @throws DAOException if a database access error occurs.
     */
    int delete(long id) throws DAOException;

    /**
     * Deletes specified entity.
     * @param entity to delete.
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or
     * (2) 0 for SQL statements that return nothing.
     * @throws DAOException if a database access error occurs.
     */
    int delete(T entity) throws DAOException;

    /**
     * Creates entity in database.
     * @param entity to create.
     * @return either (1) the row count for SQL Data Manipulation Language (DML) statements or
     * (2) 0 for SQL statements that return nothing.
     * @throws DAOException DAOException if a database access error occurs.
     */
    int create(T entity) throws DAOException;

}
