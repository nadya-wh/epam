package com.polovtseva.coffee.dao.pool;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Connection pool configuration.
 */
public class ConnectionPoolConfiguration {

    private static final Logger LOG = LogManager.getLogger(ConnectionPoolConfiguration.class);

    public static final String DB_USER_NAME;
    public static final String DB_PASSWORD;
    public static final String DB_URL;
    public static final Integer DB_MAX_CONNECTIONS;
    public static final String DB_ENCODING;
    public static final Boolean DB_USE_UNICODE;
    public static final String DB_DRIVER;

    static {
        try {
            ResourceBundle resource = ResourceBundle.getBundle("database");
            DB_URL = resource.getString("db.url");
            DB_USER_NAME = resource.getString("db.user");
            DB_PASSWORD = resource.getString("db.password");
            DB_MAX_CONNECTIONS = Integer.parseInt(resource.getString("db.poolsize"));
            DB_ENCODING = resource.getString("db.encoding");
            DB_USE_UNICODE = Boolean.parseBoolean(resource.getString("db.useUnicode"));
            DB_DRIVER = resource.getString("db.driver");
        } catch (MissingResourceException e) {
            LOG.fatal(e);
            throw new RuntimeException();
        }
    }

    private ConnectionPoolConfiguration() {
    }
}
