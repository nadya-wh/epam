package com.polovtseva.coffee.dao.impl;

import com.polovtseva.coffee.dao.OrderDAO;
import com.polovtseva.coffee.dao.exception.DAOException;
import com.polovtseva.coffee.dao.pool.ConnectionPool;
import com.polovtseva.coffee.domain.Drink;
import com.polovtseva.coffee.domain.Ingredient;
import com.polovtseva.coffee.domain.Order;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of OrderDAO.
 */
public class OrderDAOImpl implements OrderDAO {

    private static final Logger LOG = LogManager.getLogger(OrderDAO.class);

    private static OrderDAOImpl instance = new OrderDAOImpl();

    private static final String SQL_SELECT_ORDERS = "SELECT order_id, order_date, user_id, approved FROM uorder " +
            "WHERE user_id=?;";

    private static final String SQL_SELECT_ORDERS_BY_APPROVED = "SELECT order_id, order_date, user_id, approved FROM uorder " +
            "WHERE approved=?;";

    private static final String SQL_SELECT_ORDERS_BY_APPROVED_AND_USER = "SELECT order_id, order_date, user_id, approved FROM uorder " +
            "WHERE approved=? AND user_id=?;";

    private static final String SQL_INSERT_ORDER = "INSERT INTO uorder(order_date, " +
            "user_id, approved) VALUES(?, ?, ?) ;";

    private static final String SQL_INSERT_INTO_ORDER_DRINK = "INSERT INTO order_drink (order_id, drink_id, portions_count) " +
            "VALUES (?, ?, ?);";

    private static final String SQL_INSERT_ORDER_INGREDIENT = "INSERT INTO order_ingredient (order_id, ingredient_id, drink_id, " +
            "portions_count) VALUES (?, ?, ?, ?);";

    private static final String SQL_SELECT_ORDER = "SELECT order_id FROM uorder WHERE " +
            "user_id=? AND order_date=?;";

    private static final String SQL_UPDATE_APPROVE = "UPDATE uorder SET approved=? WHERE order_id=?;";


    private OrderDAOImpl() {
    }

    /**
     * Returns an OrderDAOImpl object.
     *
     * @returnan OrderDAOImpl object.
     */
    public static OrderDAOImpl getInstance() {
        return instance;
    }


    @Override
    public int create(Order entity) throws DAOException {
        PreparedStatement preparedStatement = null;
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
        ) {
            proxyConnection.setAutoCommit(false);
            preparedStatement = proxyConnection.prepareStatement(SQL_INSERT_ORDER);
            preparedStatement.setTimestamp(1, Timestamp.valueOf(entity.getOrderDate()));
            preparedStatement.setLong(2, entity.getUserId());
            preparedStatement.setBoolean(3, entity.isApproved());
            if (preparedStatement.executeUpdate() == 1) {
                preparedStatement.close();
                preparedStatement = proxyConnection.prepareStatement(SQL_SELECT_ORDER);
                long id = findOrderId(preparedStatement, entity);
                entity.setId(id);
                preparedStatement.close();
                preparedStatement = proxyConnection.prepareStatement(SQL_INSERT_INTO_ORDER_DRINK);
                createOrderDrink(preparedStatement, entity);
                preparedStatement.close();
                preparedStatement = proxyConnection.prepareStatement(SQL_INSERT_ORDER_INGREDIENT);
                createOrderDrinkIngredient(preparedStatement, entity);
                proxyConnection.commit();
                proxyConnection.setAutoCommit(true);
                return 1;
            }
            proxyConnection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                LOG.error(e);
            }
        }
        return 0;
    }


    @Override
    public ArrayList<Order> findAllForUser(long userId) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ORDERS);
        ) {
            preparedStatement.setLong(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return takeOrders(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public ArrayList<Order> findAll(boolean approved) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ORDERS_BY_APPROVED);
        ) {
            preparedStatement.setBoolean(1, approved);
            ResultSet resultSet = preparedStatement.executeQuery();
            return takeOrders(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public ArrayList<Order> findAll(long userId, boolean approved) throws DAOException {
        try (
                Connection connection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ORDERS_BY_APPROVED_AND_USER);
        ) {
            preparedStatement.setBoolean(1, approved);
            preparedStatement.setLong(2, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            return takeOrders(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }


    @Override
    public long findOne(long userId, LocalDateTime date) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_SELECT_ORDER);
        ) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setTimestamp(2, Timestamp.valueOf(date));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getLong("order_id");
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return 0;
    }

    @Override
    public int[] approve(long[] orderIds) throws DAOException {
        try (
                Connection proxyConnection = ConnectionPool.getInstance().getConnection();
                PreparedStatement preparedStatement = proxyConnection.prepareStatement(SQL_UPDATE_APPROVE);
        ) {
            for (int i = 0; i < orderIds.length; i++) {
                preparedStatement.setBoolean(1, true);
                preparedStatement.setLong(2, orderIds[i]);
                preparedStatement.addBatch();
            }
            return preparedStatement.executeBatch();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    private int[] createOrderDrink(PreparedStatement preparedStatement, Order order) throws SQLException {
        for (Drink drink : order.getDrinks()) {
            preparedStatement.setLong(1, order.getId());
            preparedStatement.setLong(2, drink.getId());
            preparedStatement.setInt(3, drink.getNumberInOrder());
            preparedStatement.addBatch();
        }
        return preparedStatement.executeBatch();
    }

    private int[] createOrderDrinkIngredient(PreparedStatement preparedStatement, Order order) throws SQLException {
        for (Drink drink : order.getDrinks()) {
            for (Ingredient ingredient : drink.getIngredients()) {
                preparedStatement.setLong(1, order.getId());
                preparedStatement.setLong(2, ingredient.getId());
                preparedStatement.setLong(3, drink.getId());
                preparedStatement.setInt(4, ingredient.getNumberInDrink());
                preparedStatement.addBatch();
            }
        }
        return preparedStatement.executeBatch();
    }

    private long findOrderId(PreparedStatement preparedStatement, Order order) throws SQLException {
        preparedStatement.setLong(1, order.getUserId());
        preparedStatement.setTimestamp(2, Timestamp.valueOf(order.getOrderDate()));
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            return resultSet.getLong("order_id");
        }
        return 0;
    }

    private ArrayList<Order> takeOrders(ResultSet resultSet) throws SQLException, DAOException {
        ArrayList<Order> orders = new ArrayList<>();
        while (resultSet.next()) {
            long orderId = resultSet.getInt("order_id");
            Timestamp orderDate = resultSet.getTimestamp("order_date");
            long userId = resultSet.getInt("user_id");
            boolean approved = resultSet.getBoolean("approved");
            ArrayList<Drink> drinks = DrinkDAOImpl.getInstance().findAllInOrder(orderId);
            orders.add(new Order(orderId, userId, orderDate.toLocalDateTime(), drinks, approved));
        }
        return orders;
    }

    @Override
    public List<Order> findAll() throws DAOException {
        return null;
    }

    @Override
    public Order findOne(long id) throws DAOException {
        return null;
    }

    @Override
    public int delete(long id) throws DAOException {
        return 0;
    }

    @Override
    public int delete(Order entity) throws DAOException {
        return 0;
    }

}
