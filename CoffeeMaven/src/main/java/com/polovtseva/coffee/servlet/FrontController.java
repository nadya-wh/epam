package com.polovtseva.coffee.servlet;

import com.polovtseva.coffee.command.Command;
import com.polovtseva.coffee.command.CommandFactory;
import com.polovtseva.coffee.command.PagesNames;
import com.polovtseva.coffee.command.exception.CommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet.
 */
@WebServlet(
        name = "Controller",
        urlPatterns = {"/coffee/*", "/coffee", "/user/history", "/user/order", "/user/*"}
)
public class FrontController extends HttpServlet {

    private static final Logger LOG = LogManager.getLogger(FrontController.class);


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doRequest(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doRequest(req, resp);
    }


    private String processRequest(HttpServletRequest req) {
        Command command = CommandFactory.defineCommand(req);
        try {
            return command.execute(req);
        } catch (CommandException e) {
            LOG.error(e);
            return PagesNames.TECH_ERROR_PAGE_KEY;
        }
    }
    private void doRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String page = processRequest(req);
        req.getRequestDispatcher(page).forward(req, resp);
    }

    @Override
    public void destroy() {
    }
}
