package com.polovtseva.coffee.listener;


import com.polovtseva.coffee.dao.pool.ConnectionPool;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionBindingEvent;

/**
 * Listener. Initializes and destroys connection pool.
 */
@WebListener
public class ServletListener implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {


    public ServletListener() {
    }

    public void contextInitialized(ServletContextEvent sce) {
        ConnectionPool.getInstance().init();
    }

    public void contextDestroyed(ServletContextEvent sce) {
        ConnectionPool.getInstance().releasePool();
    }


    public void sessionCreated(HttpSessionEvent se) {
    }

    public void sessionDestroyed(HttpSessionEvent se) {
    }

    public void attributeAdded(HttpSessionBindingEvent sbe) {
    }

    public void attributeRemoved(HttpSessionBindingEvent sbe) {
    }

    public void attributeReplaced(HttpSessionBindingEvent sbe) {
    }
}
