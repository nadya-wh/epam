<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="/signin"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="login.button.submit"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">


    <script src="../js/ie-emulation-modes-warning.js"></script>
</head>
<header>
    <%@include file="jspf/navbar.jspf" %>
</header>
<body>


<div class="container" style="vertical-align: middle !important;">
    <form class="form-signin" name="login" action="/coffee" method="POST">
        <input type="hidden" name="command" value="login">
        <h2 class="form-signin-heading" align="center">
            <fmt:message key="login.button.submit"/>
        </h2>
        <c:if test="${errorLoginPasswordMessage}">
            <fmt:bundle basename="i18n.messages">
                <div id="error" class="alert alert-danger">
                    <fmt:message key="message.invalidloginerror"/>
                </div>
            </fmt:bundle>
        </c:if>
        <label for="inputUsername" class="sr-only">
            <fmt:message key="login.label.username"/>
        </label>
        <input type="text" id="inputUsername" class="form-control custom-input" name="username"
               placeholder="<fmt:message key="login.label.username"/>" required autofocus>
        <label for="inputPassword" class="sr-only">
            <fmt:message key="login.label.password"/>
        </label>
        <input type="password" id="inputPassword" class="form-control custom-input" name="password"
               placeholder="<fmt:message key="login.label.password"/>" required>

        <button class="btn btn-primary center-block custom-button" type="submit">
            <fmt:message key="login.button.submit"/>
        </button>
        <a href="<c:url value="/signup"/>">
            <h5 class="form-signin-heading" align="center">
                <fmt:message key="login.message.wanttosignup"/>
            </h5>
        </a>
    </form>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <ctg:pfooter/>
</footer>
</html>
