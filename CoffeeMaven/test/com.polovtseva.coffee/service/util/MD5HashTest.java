package com.polovtseva.coffee.service.util;

import org.junit.Assert;
import org.junit.Test;


/**
 * Test for MD5Hash.
 */
public class MD5HashTest {

    @Test
    public void countHashTest() {
        String expected = "21232f297a57a5a743894a0e4a801fc3";
        String actual = MD5Hash.countHash("admin");
        Assert.assertEquals(expected, actual);
    }


}