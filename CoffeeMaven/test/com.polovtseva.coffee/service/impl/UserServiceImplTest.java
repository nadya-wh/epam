package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.UserDAO;
import com.polovtseva.coffee.domain.User;
import com.polovtseva.coffee.domain.UserRole;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 * Test for UserServiceImpl.
 */
public class UserServiceImplTest {

    @Mock
    private UserDAO userDAO;

    private UserServiceImpl userService;
    private User correctUser;
    private User incorrectUser;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userService = new UserServiceImpl(userDAO);
        correctUser = new User(1, "lallal", "DifficultPassword123456789.", "firstname", "lastname", UserRole.USER);
        incorrectUser = new User(1, "d.f.", "DifficultPassword123456789.", "firstname", "lastname", UserRole.USER);
    }

    @Test
    public void testAuthorisation() throws Exception {
        when(userDAO.authenticate(anyString(), anyString())).thenReturn(true);
        when(userDAO.findOne(anyString())).thenReturn(correctUser);
        assertNotNull(userService.authorisation(correctUser.getLogin(), correctUser.getPassword()));
    }

    @Test
    public void testCreate() throws Exception {
        when(userDAO.create(any(User.class))).thenReturn(1);
        assertTrue(userService.create(correctUser));
        assertFalse(userService.create(incorrectUser));
        assertFalse(userService.create(null));
    }
}