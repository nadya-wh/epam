package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.OrderDAO;
import com.polovtseva.coffee.domain.Order;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import static org.mockito.Mockito.*;

/**
 * Test for OrderServiceImpl.
 */
public class OrderServiceImplTest {

    @Mock
    private OrderDAO orderDAO;

    private OrderServiceImpl orderService;
    private Order correctOrder;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        orderService = new OrderServiceImpl(orderDAO);
        correctOrder = new Order(1, 1, LocalDateTime.now());
    }

    @Test
    public void testFindAll() throws Exception {
        List<Order> answer = new ArrayList<>();
        when(orderDAO.findAll()).thenReturn(answer);
        assertEquals(answer, orderService.findAll(1));
    }

    @Test
    public void testCreateOrder() throws Exception {
        when(orderDAO.create(any(Order.class))).thenReturn(1);
        assertTrue(orderService.create(correctOrder));
        assertFalse(orderService.create(null));
    }
}