package com.polovtseva.coffee.service.impl;

import com.polovtseva.coffee.dao.DrinkDAO;
import com.polovtseva.coffee.domain.Drink;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


/**
 * Test for DrinkServiceImpl.
 */
@RunWith(MockitoJUnitRunner.class)
public class DrinkServiceImplTest {


    @Mock
    private DrinkDAO drinkDAO;

    private Drink wrongDrink;
    private Drink correctDrink;
    private DrinkServiceImpl drinkService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        drinkService = new DrinkServiceImpl(drinkDAO);
        wrongDrink = new Drink(1, "lal", -123, 0.6, "lalala");
        correctDrink = new Drink(1, "lal", 123, 0.6, "lalala");

        when(drinkDAO.create(any(Drink.class))).thenReturn(1);

    }

    @Test
    public void testCreate() throws Exception {

        assertNotNull(drinkDAO);

        assertFalse(drinkService.create(wrongDrink));
        assertTrue(drinkService.create(correctDrink));

    }


    @Test
    public void testDeletePositive() throws Exception {
        when(drinkDAO.countInOrders(anyInt())).thenReturn(0);
        when(drinkDAO.delete(anyInt())).thenReturn(1);
        assertTrue(drinkService.delete(wrongDrink.getId()));

    }

    @Test
    public void testDeleteNegative() throws Exception {
        when(drinkDAO.delete(anyInt())).thenReturn(0);
        when(drinkDAO.countInOrders(anyInt())).thenReturn(0);
        assertFalse(drinkService.delete(wrongDrink.getId()));

    }


}