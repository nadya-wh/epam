<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="/jsp/account.jsp"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="account.title"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">
    <link href="../css/custom-block.css" rel="stylesheet">

    <script src="../js/ie-emulation-modes-warning.js"></script>
</head>
<header>
    <%@include file="jspf/navbar.jspf" %>
</header>
<body>


<div class="container" style="vertical-align: middle !important; horiz-align: center !important;">
    <c:choose>
        <c:when test="${not empty account}">
            <form action="/user/account" method="POST">
                <input type="hidden" name="command" value="REPLENISH_BALANCE">
                <div class="custom-block">
                    <h4>
                        <fmt:message key="account.accountnumber"/> : ${account.id}
                    </h4>
                </div>
                <br>
                <div class="custom-block">
                    <h4>
                        <fmt:message key="account.balance"/> : ${account.balance}
                    </h4>
                </div>
                <br>
                <div align="center">
                    <input type="number" min="0" max="10000" name="sum" class="form-control custom-input" style="width: 10% !important;
            display: inline-block !important; margin-right: 30px; margin-left: -265px;"/>
                    <button class="btn btn-primary center-block" type="submit"
                            style="display: inline-block !important; vertical-align: middle !important;">
                        <fmt:message key="account.replenishbalance"/>
                    </button>

                </div>
            </form>
        </c:when>
        <c:when test="${empty account}">
            <div class="alert alert-danger">
                <fmt:bundle basename='messages'>
                    <fmt:message key="message.currentorder.accounterror"/>
                </fmt:bundle>
            </div>
        </c:when>
    </c:choose>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <%--<ctg:pfooter/>--%>
</footer>
</html>
