<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="/coffee/menu"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="menu.header"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">


    <script src="../js/ie-emulation-modes-warning.js"></script>

    <script src="../js/substituteImage.js"></script>

</head>
<header>

</header>
<body>

<%@include file="jspf/navbar.jspf" %>

<%--<div class="container">--%>
<%--<form action="/coffee" method="post">--%>
<%--<input type="hidden" name="com.polovtseva.coffee.command" value="GET_INGREDIENTS_LIST">--%>
<%--<button type="button" class="btn btn-primary btn-lg pull-right" onclick="submit()">--%>
<%--<fmt:message key="addingredient.header"/>--%>
<%--</button>--%>
<%--</form>--%>
<%--</div>--%>

<div class="container" style="vertical-align: middle !important;">
    <h2 class="form-signin-heading" align="center">
        <fmt:message key="menu.form.header"/>
    </h2>
    <fmt:bundle basename="messages">

        <c:choose>
            <c:when test="${refillError}">
                <div class="alert alert-danger">
                    <fmt:message key="message.refillerror"/>
                </div>
            </c:when>
            <c:when test="${refillSuccess}">
                <div class="alert alert-success">
                    <fmt:message key="message.refillsuccess"/>
                </div>
            </c:when>
            <c:when test="${deleteError}">
                <div class="alert alert-danger">
                    <fmt:message key="message.delete.error"/>
                </div>
            </c:when>
            <c:when test="${error}">
                <div class="alert alert-danger">
                    <fmt:message key="message.addtobasketerror"/>
                </div>
            </c:when>
            <c:when test="${success}">
                <div class="alert alert-success">
                    <fmt:message key="message.addtobasketsuccess"/>
                </div>
            </c:when>
            <c:when test="${deleteSuccess}">
                <div class="alert alert-success">
                    <fmt:message key="message.delete.success"/>
                </div>
            </c:when>
        </c:choose>
    </fmt:bundle>

    <c:if test="${role != 'admin'}">

        <c:forEach var="item" items="${drinks}">
            <c:if test="${item.portionsLeft > 0}">
                <form action="/coffee/menu" method="POST">
                    <input type="hidden" name="command" value="choose_drink">
                    <input type="hidden" name="drinkId" value="${item.id}">
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">

                            <img src="${item.imageUrl}" alt="" height="300px"
                                 style="height: 300px !important;margin-top: 25px;"
                                 onError="this.onerror=null;this.src='../picture/Java_logo.png';">
                            <div class="caption">

                                <h4 class="pull-right">
                                    <fmt:formatNumber maxFractionDigits="2" value="${item.drinkPrice}"/>
                                </h4>
                                <h4>
                                        ${item.drinkName}
                                </h4>
                                <input type="submit" name="addIngredient"
                                       value="<fmt:message key="addingredient.header"/>"
                                       class="btn btn-primary btn-xs pull-right">

                                <br>
                                <br>
                                <input class="custom-input" style="border-radius: 4px;" class="form-signin"
                                       name="drinksNumber" type="number" value="1"
                                       min="1" max="${item.portionsLeft}">

                                <input type="submit" name="addDrinkToOrder"
                                       value="<fmt:message key="menu.form.addtoorder"/>"
                                       class="btn btn-primary btn-xs pull-right">

                                <br>


                            </div>
                        </div>
                    </div>
                </form>
                <form id="add-ingredient-to-drink-form" action="/coffee" method="post">
                    <input type="hidden" name="command" value="ADD_INGREDIENT_TO_DRINK">
                    <input type="hidden" name="drinkId" value="${item.id}">
                </form>

            </c:if>
        </c:forEach>
    </c:if>
    <c:if test="${role == 'admin'}">
        <form action="/coffee" method="POST">
            <input type="hidden" name="command" value="GET_INGREDIENTS_LIST">
            <input type="submit" class="btn btn-primary pull-right"
                   value="<fmt:message key="showingredients"/> ">
        </form>
        <br>
        <br>
        <c:forEach var="item" items="${drinks}">
            <div class="col-sm-4 col-lg-4 col-md-4">

                <form id="delete-form" action="/coffee/menu" method="POST">
                    <input type="hidden" name="command" value="DELETE_DRINK">
                    <input type="hidden" name="drinkId" value="${item.id}">
                    <button type="button" class="btn btn-default btn-xs"
                            onclick="submit()"
                            style="border-color: transparent;">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </button>
                </form>


                <div class="thumbnail">
                    <img src="${item.imageUrl}" alt="" height="300px" style="height: 300px !important;"
                         onError="this.onerror=null;
                         this.src='../picture/Java_logo.png';">
                    <div class="caption">
                        <h4 class="pull-right">
                            <fmt:formatNumber maxFractionDigits="2" value="${item.drinkPrice}"/>$
                        </h4>
                        <h4>
                                ${item.drinkName}
                        </h4>
                        <div style="display: inline-block;">
                            <h5>
                                <fmt:message key="menu.form.left"/>: ${item.portionsLeft}
                            </h5>
                            <form action="/coffee/menu" method="POST">
                                <input type="hidden" name="command" value="REMOVE_DRINK">
                                <input type="hidden" name="drinkId" value="${item.id}">
                                <button type="button" class="btn btn-primary btn-xs pull-right" onclick="submit()"
                                        style="margin-bottom: 8px;">
                                    <fmt:message key="menu.form.remove"/>
                                </button>
                            </form>
                        </div>
                        <br>
                        <form action="/coffee/menu" method="POST">
                            <input type="hidden" name="command" value="REFILL_COFFEE">
                            <input type="hidden" name="drinkId" value="${item.id}">
                            <input class="custom-input" style="border-radius: 4px;" class="form-signin"
                                   name="drinksNumber" type="number" value="1"
                                   min="0">
                            <button type="button" class="btn btn-primary btn-xs pull-right" onclick="submit()">
                                <fmt:message key="menu.form.refill"/>
                            </button>
                        </form>

                    </div>
                </div>


            </div>
        </c:forEach>
    </c:if>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <div class="footer">
        <ctg:pfooter/>
    </div>
</footer>
</html>
