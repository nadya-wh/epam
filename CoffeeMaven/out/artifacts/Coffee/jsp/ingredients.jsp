<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="/jsp/menu.jsp"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="ingredients.form.header"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">


    <script src="../js/ie-emulation-modes-warning.js"></script>
    <script src="../js/substituteImage.js"></script>
</head>
<header>

</header>
<body>

<%@include file="jspf/navbar.jspf" %>


<div class="container" style="vertical-align: middle !important;">
    <h2 class="form-signin-heading" align="center">
        <fmt:message key="ingredients.form.header"/>
    </h2>

    <c:if test="${success}">
        <div class="alert alert-success">
            <fmt:bundle basename="messages">
                <fmt:message key="message.addingredientsuccess"/>
            </fmt:bundle>
        </div>
    </c:if>
    <c:if test="${error}">
        <div class="alert alert-danger">
            <fmt:bundle basename="messages">
                <fmt:message key="message.addingredienterror"/>
            </fmt:bundle>
        </div>
    </c:if>
    <c:if test="${deleteError}">
        <div class="alert alert-danger">
            <fmt:bundle basename="messages">
                <fmt:message key="message.delete.error"/>
            </fmt:bundle>
        </div>
    </c:if>
    <c:if test="${deleteSuccess}">
        <div class="alert alert-success">
            <fmt:bundle basename="messages">
                <fmt:message key="message.delete.success"/>
            </fmt:bundle>
        </div>
    </c:if>
    <c:if test="${refillSuccess}">
        <div class="alert alert-success">
            <fmt:bundle basename="messages">
                <fmt:message key="message.refillsuccess"/>
            </fmt:bundle>
        </div>
    </c:if>
    <c:if test="${refillError}">
        <div class="alert alert-success">
            <fmt:bundle basename="messages">
                <fmt:message key="message.refillerror"/>
            </fmt:bundle>
        </div>
    </c:if>
    <c:if test="${role != 'admin'}">
        <c:forEach var="item" items="${ingredients}">
            <c:if test="${item.portionsLeft > 0}">
                <form id="form" action="/coffee/menu" method="POST">
                    <input id="command" type="hidden" name="command" value="add_ingredient_to_drink">
                    <input type="hidden" name="ingredientId" value="${item.id}">
                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class="thumbnail">
                            <img src="${item.imageUrl}" alt="" height="300px" style="height: 300px !important;"
                                 onError="this.onerror=null;this.src='../picture/Java_logo.png';">
                            <div class="caption">
                                <h4 class="pull-right">
                                    <fmt:formatNumber maxFractionDigits="2" value="${item.ingredientPrice}"/>$
                                </h4>
                                <h4>
                                        ${item.ingredientName}
                                </h4>

                                <input class="custom-input" style="border-radius: 4px;" class="form-signin"
                                       name="ingredientsNumber" type="number" value="1"
                                       min="1" max="${item.portionsLeft}">
                                <button type="button" class="btn btn-primary btn-xs pull-right" onclick="submit()">
                                    <fmt:message key="ingredients.form.add"/>
                                </button>

                            </div>
                        </div>
                    </div>
                </form>
            </c:if>
        </c:forEach>
        <form action="/coffee" method="post">
            <input type="hidden" name="command" value="ADD_DRINK_TO_ORDER">
            <input type="submit" value="<fmt:message key="menu.form.addtoorder"/> "
                   class="btn btn-primary btn-lg pull-right">
        </form>
    </c:if>

    <c:if test="${role == 'admin'}">
        <c:forEach var="item" items="${ingredients}">
            <div class="col-sm-4 col-lg-4 col-md-4">
                <form id="delete-form" action="/coffee/menu" method="POST">
                    <input type="hidden" name="command" value="DELETE_INGREDIENT">
                    <input type="hidden" name="ingredientId" value="${item.id}">
                    <button type="button" class="btn btn-default btn-xs pull-portionsLeft"
                            onclick="submit()"
                            style="border-color: transparent;">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </button>
                </form>


                <div class="thumbnail">
                    <img src="${item.imageUrl}" alt="" height="300px" style="height: 300px !important;"
                         onError="this.onerror=null;this.src='../picture/Java_logo.png';">
                    <div class="caption">
                        <h4 class="pull-right">
                            <fmt:formatNumber maxFractionDigits="2" value="${item.ingredientPrice}"/>$
                        </h4>
                        <h4>
                                ${item.ingredientName}
                        </h4>
                        <div style="display: inline-block;">
                            <h5>
                                <fmt:message key="menu.form.left"/>: ${item.portionsLeft}
                            </h5>
                            <form action="/coffee/menu" method="POST">
                                <input type="hidden" name="command" value="REMOVE_INGREDIENT">
                                <input type="hidden" name="ingredientId" value="${item.id}">
                                <button type="button" class="btn btn-primary btn-xs pull-right" onclick="submit()"
                                        style="margin-bottom: 8px;">
                                    <fmt:message key="menu.form.remove"/>
                                </button>
                            </form>
                        </div>
                        <br>
                        <form action="/coffee/menu" method="POST">
                            <input type="hidden" name="command" value="refill_ingredients">
                            <input type="hidden" name="ingredientId" value="${item.id}">

                            <input class="custom-input" style="border-radius: 4px;" class="form-signin"
                                   name="ingredientsNumber" type="number" value="1"
                                   min="0">
                            <button type="button" class="btn btn-primary btn-xs pull-right" onclick="submit()">
                                <fmt:message key="menu.form.refill"/>
                            </button>
                        </form>

                    </div>
                </div>


            </div>
        </c:forEach>
    </c:if>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <%--<ctg:pfooter/>--%>
</footer>
</html>
