<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<c:set var="currentPage"
       value="/jsp/currentorder.jsp"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="nav.profile.currentorder"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">
    <link href="../css/custom-table.css" rel="stylesheet">


    <script src="../js/ie-emulation-modes-warning.js"></script>
</head>
<header>

</header>
<body>

<%@include file="jspf/navbar.jspf" %>


<div class="container" style="vertical-align: middle !important;">
    <c:if test="${error}">
        <fmt:bundle basename="messages">
            <div class="alert alert-danger">
                <fmt:message key="message.currentorder.notenoughmoney"/>
            </div>
        </fmt:bundle>
    </c:if>
    <c:if test="${accountError}">
        <fmt:bundle basename="messages">
            <div class="alert alert-danger">
                <fmt:message key="message.currentorder.accounterror"/>
            </div>
        </fmt:bundle>
    </c:if>
    <c:if test="${countError}">
        <fmt:bundle basename="messages">
            <div class="alert alert-danger">
                <fmt:message key="message.currentorder.counterror"/>
            </div>
        </fmt:bundle>
    </c:if>
    <c:if test="${numberError}">
        <fmt:bundle basename="messages">
            <div class="alert alert-danger">
                <fmt:message key="message.currentorder.counterror"/>
            </div>
        </fmt:bundle>
    </c:if>
    <div class="div-table">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>

                </th>
                <th>
                    <fmt:message key="drinkname"/>
                </th>
                <th>

                </th>
                <th>
                    <fmt:message key="price"/>
                </th>
                <th>

                </th>
                <th>
                    <fmt:message key="adddrink.form.portionsnumber"/>
                </th>
            </tr>
            </thead>
            <tbody>

            <c:forEach var="item" items="${currentOrder.drinks}">
                <form action="/user/order" method="POST">
                    <input type="hidden" name="command" value="REMOVE_DRINK_FROM_ORDER">
                    <input type="hidden" name="drinkId" value="${item.id}">
                    <input type="hidden" name="drink" value="${item}">
                    <input type="hidden" name="numberInOrder" value="${item.numberInOrder}">
                    <tr>
                        <td>
                            <button type="button" class="btn btn-default btn-xs"
                                    onclick="submit()"
                                    style="border-color: transparent;">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                        </td>
                        <td>${item.drinkName}</td>
                        <td>
                            <img src="${item.imageUrl}" height="80" width="80">
                        </td>
                        <td>

                            <fmt:formatNumber maxFractionDigits="2" value="${item.drinkPrice}"/>
                        </td>
                        <td>
                            <c:if test="${item.ingredients.size() > 0}">
                                <table class="table custom-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <fmt:message key="addingredient.form.ingredientname"/>
                                        </th>
                                        <th>

                                        </th>
                                        <th>
                                            <fmt:message key="addingredient.form.ingredientprice"/>
                                        </th>
                                        <th>
                                            <fmt:message key="addingredient.form.portionsLeft"/>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="ingredient" items="${item.ingredients}">
                                        <tr>
                                            <input type="hidden" name="ingredientId" value="${ingredient.id}">
                                            <input type="hidden" name="numberInDrink" value="${ingredient.numberInDrink}">
                                            <td>
                                                    ${ingredient.ingredientName}
                                            </td>
                                            <td>
                                                <img src="${ingredient.imageUrl}" height="80" width="80">
                                            </td>
                                            <td>
                                                <fmt:formatNumber maxFractionDigits="2"
                                                                  value="${ingredient.ingredientPrice}"/>$
                                            </td>
                                            <td>
                                                    ${ingredient.numberInDrink}
                                            </td>
                                        </tr>

                                    </c:forEach>

                                    </tbody>
                                </table>
                            </c:if>
                        </td>

                        <td>
                                ${item.numberInOrder}
                        </td>

                    </tr>
                </form>
            </c:forEach>

            </tbody>
        </table>
    </div>
</div>
<div class="container">
    <h2 class="pull-left">
        <fmt:message key="totalsum"/>: <fmt:formatNumber maxFractionDigits="2" value="${totalSum}"/> $
    </h2>
</div>
<div class="container">

    <form action="/user/order" method="POST">
        <input type="hidden" name="command" value="order">
        <button class="btn btn-primary custom-button pull-left" type="submit">
            <fmt:message key="currentorder.order.submit"/>
        </button>
    </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
</footer>
</html>
