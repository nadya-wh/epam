<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage"
       value="/"
       scope="session"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}"/>
<fmt:setBundle basename="i18n.text"/>

<!DOCTYPE html>
<html lang="${language}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../picture/coffee-icon.png">
    <title>
        <fmt:message key="orders.title"/>
    </title>
    <script src="../js/jquery.min.js"></script>
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/signin.css" rel="stylesheet">
    <link href="../css/custom-button.css" rel="stylesheet">
    <link href="../css/custom-select.css" rel="stylesheet" type="text/css">
    <link href="../css/custom-input.css" rel="stylesheet">
    <link href="../css/navbar-static-top.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href="../css/font.css" rel="stylesheet">
    <link href="../css/footer.css" rel="stylesheet">
    <link href="../css/background.css" rel="stylesheet">
    <link href="../css/custom-table.css" rel="stylesheet">


    <script src="../js/ie-emulation-modes-warning.js"></script>
</head>
<header>
    <%@include file="jspf/navbar.jspf" %>
</header>
<body>

<div class="container">
    <c:choose>
        <c:when test="${approveError}">
            <div class="alert alert-danger">
                <fmt:bundle basename="messages">
                    <fmt:message key="message.approve.error"/>
                </fmt:bundle>
            </div>
        </c:when>
        <c:when test="${approveSuccess}">
            <div class="alert alert-success">
                <fmt:bundle basename="messages">
                    <fmt:message key="message.approve.success"/>
                </fmt:bundle>
            </div>
        </c:when>
    </c:choose>
    <form action="/coffee" method="POST">
        <button class="btn btn-primary center-block custom-button" type="submit">
            <fmt:message key="orders.approve"/>
        </button>
        <input type="hidden" name="command" value="APPROVE_ORDER">
        <div class="div-table">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>

                    </th>
                    <th>
                        <fmt:message key="date"/>
                    </th>
                </tr>
                </thead>
                <tbody>

                <c:forEach var="item" items="${orders}">
                    <tr>
                        <td>
                                ${item.id}
                        </td>
                        <td>
                                ${item.orderDate}
                        </td>
                        <td>
                            <c:if test="${item.drinks.size() > 0}">
                                <table class="table table-striped custom-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            <fmt:message key="drinkname"/>
                                        </th>
                                        <th>
                                            <fmt:message key="image"/>
                                        </th>
                                        <th>
                                            <fmt:message key="price"/>
                                        </th>
                                        <th>
                                            <fmt:message key="adddrink.form.portionsnumber"/>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <c:forEach var="drink" items="${item.drinks}">
                                        <tr>
                                            <td>
                                                    ${drink.drinkName}
                                            </td>
                                            <td>
                                                <img src="${drink.imageUrl}" height="80" width="80">
                                            </td>
                                            <td>
                                                <fmt:formatNumber maxFractionDigits="2" value="${drink.drinkPrice}"/>
                                            </td>
                                            <td>
                                                ${drink.numberInOrder}
                                            </td>
                                            <td>
                                                <c:if test="${drink.ingredients.size() > 0}">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                <fmt:message key="addingredient.form.ingredientname"/>
                                                            </th>
                                                            <th>

                                                            </th>
                                                            <th>
                                                                <fmt:message key="addingredient.form.ingredientprice"/>
                                                            </th>
                                                            <th>
                                                                <fmt:message key="addingredient.form.portionsLeft"/>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <c:forEach var="ingredient" items="${drink.ingredients}">
                                                            <tr>
                                                                <td>
                                                                        ${ingredient.ingredientName}
                                                                </td>
                                                                <td>
                                                                    <img src="${ingredient.imageUrl}" height="80"
                                                                         width="80">
                                                                </td>
                                                                <td>
                                                                    <fmt:formatNumber maxFractionDigits="2"
                                                                                      value="${ingredient.ingredientPrice}"/>
                                                                </td>
                                                                <td>
                                                                        ${ingredient.numberInDrink}
                                                                </td>
                                                            </tr>

                                                        </c:forEach>

                                                        </tbody>
                                                    </table>
                                                </c:if>
                                            </td>

                                        </tr>
                                    </c:forEach>

                                    </tbody>
                                </table>
                            </c:if>
                        </td>

                        <td>
                            <input type="checkbox" class="checkbox checkbox-primary" name="orderId" value="${item.id}">
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <button class="btn btn-primary center-block custom-button" type="submit">
            <fmt:message key="orders.approve"/>
        </button>
    </form>

</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../js/bootstrap.min.js"></script>

</body>
<footer>
    <ctg:pfooter/>
</footer>
</html>
