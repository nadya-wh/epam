package com.polovtseva.web_parsing_xml.servlet;

import com.polovtseva.web_parsing_xml.creator.DeviceArrayCreator;
import com.polovtseva.web_parsing_xml.entity.Device;
import com.polovtseva.web_parsing_xml.entity.Laptop;
import com.polovtseva.web_parsing_xml.entity.LaptopTablet;
import com.polovtseva.web_parsing_xml.exception.NoSuchParserException;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by User on 11.01.2016.
 */
@WebServlet("/parsing")
public class ParsingServlet extends HttpServlet {

    public ParsingServlet() {

    }

    @Override
    public void init() throws ServletException {
        String prefix = getServletContext().getRealPath("/");
        String filename = "log4j.properties"; //
        PropertyConfigurator.configure(prefix + filename);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String prefix = getServletContext().getRealPath("/");
        String xmlFilename = "res/devices.xml";
        String parser = req.getParameter("parsersComboBox");
        Set<Device> devices = null;
        Set<Laptop> laptops = new HashSet<>();
        Set<LaptopTablet> laptopTablets = new HashSet<>();
        try {
            devices = DeviceArrayCreator.create(parser, prefix + xmlFilename);

        } catch (NoSuchParserException e) {

        }
        for (Device d : devices) {
            if (d instanceof LaptopTablet) {
                laptopTablets.add((LaptopTablet)d);
            } else {
                laptops.add((Laptop) d);
            }
        }

        req.setAttribute("laptops", laptops);
        req.setAttribute("laptopTablets", laptopTablets);
        req.setAttribute("parser", parser);
        req.getRequestDispatcher("jsp/result.jsp").forward(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
