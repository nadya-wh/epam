package com.polovtseva.web_parsing_xml.exception;

/**
 * Created by User on 11.01.2016.
 */
public class NoSuchParserException extends Exception {
    public NoSuchParserException() {
        super();
    }

    public NoSuchParserException(String message) {
        super(message);
    }

    public NoSuchParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchParserException(Throwable cause) {
        super(cause);
    }

    protected NoSuchParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
