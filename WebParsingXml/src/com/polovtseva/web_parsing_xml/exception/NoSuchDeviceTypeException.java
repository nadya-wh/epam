package com.polovtseva.web_parsing_xml.exception;

/**
 * Created by User on 02.01.2016.
 */
public class NoSuchDeviceTypeException extends Exception {
    public NoSuchDeviceTypeException() {
    }

    public NoSuchDeviceTypeException(String message) {
        super(message);
    }

    public NoSuchDeviceTypeException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchDeviceTypeException(Throwable cause) {
        super(cause);
    }

    public NoSuchDeviceTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
