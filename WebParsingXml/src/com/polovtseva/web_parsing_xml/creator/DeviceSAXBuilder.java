package com.polovtseva.web_parsing_xml.creator;

import com.polovtseva.web_parsing_xml.entity.Device;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.util.Set;

/**
 * Created by User on 01.01.2016.
 */
public class DeviceSAXBuilder extends DeviceBuilder{

    static Logger LOG = Logger.getLogger(DeviceSAXBuilder.class);

    private DeviceHandler deviceHandler;
    private XMLReader reader;

    public DeviceSAXBuilder() {
        deviceHandler = new DeviceHandler();
        try {
            reader = XMLReaderFactory.createXMLReader();
            reader.setContentHandler(deviceHandler);
        } catch (SAXException e) {
            LOG.fatal(e);
        }
    }

    public Set<Device> getDevices() {
        return devices;
    }

    public void buildSetDevices(String filename) {
        try {
            reader.parse(filename);
        } catch (SAXException e) {
            LOG.error(e);
        } catch (IOException e) {
            LOG.error(e);
        }
        devices = deviceHandler.getDevices();
    }
}
