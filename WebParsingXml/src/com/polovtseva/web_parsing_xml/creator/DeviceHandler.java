package com.polovtseva.web_parsing_xml.creator;


import com.polovtseva.web_parsing_xml.entity.*;
import com.polovtseva.web_parsing_xml.exception.NoSuchDeviceTypeException;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by User on 01.01.2016.
 */
public class DeviceHandler extends DefaultHandler {

    static Logger LOG = Logger.getLogger(DeviceHandler.class);

    private Set<Device> devices;
    private Device current;
    private DeviceEnum currentEnum;
    private EnumSet<DeviceEnum> withText;
    private Manufacture currentManufacture;

    public DeviceHandler() {
        withText = EnumSet.range(DeviceEnum.PRICE, DeviceEnum.TYPE);
        devices = new HashSet<>();
    }

    public Set<Device> getDevices() {
        return devices;
    }


    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (DeviceEnum.LAPTOP.getValue().equals(localName) ||
                DeviceEnum.LAPTOP_TABLET.getValue().equals(localName)) {
            if (current != null) {
                devices.add(current);
            }
        } else if (DeviceEnum.ORIGIN.getValue().equals(localName)) {
            current.setOrigin(currentManufacture);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String s = new String(ch, start, length).trim();
        if (currentEnum != null) {
            switch (currentEnum) {
                case COUNTRY:
                    if (currentManufacture != null && !s.isEmpty()) {
                        currentManufacture.setCountry(s);
                    }
                    break;
                case STREET:
                    if (currentManufacture != null && !s.isEmpty()) {
                        currentManufacture.setStreet(s);
                    }
                    break;
                case CITY:
                    if (currentManufacture != null && !s.isEmpty()) {
                        currentManufacture.setCity(s);
                    }
                    break;
                case NAME:
                    if (currentManufacture != null && !s.isEmpty()) {
                        currentManufacture.setName(s);
                    }
                    break;
                case PRICE:
                    try {
                        float price = Float.parseFloat(s);
                        current.setPrice(price);
                    } catch (NumberFormatException e) {
                        LOG.error(e);
                    }
                    break;
                case CRITICAL:
                    boolean critical = Boolean.getBoolean(s);
                    current.setCritical(critical);
                    break;
            }
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (DeviceEnum.LAPTOP.getValue().equals(localName)) {
            current = new Laptop();
            putDeviceAttributes(attributes);
            putBatteryCapacity(attributes);

        } else if (DeviceEnum.LAPTOP_TABLET.getValue().equals(localName)) {
            current = new LaptopTablet();
            putDeviceAttributes(attributes);
            putBatteryCapacity(attributes);
            float tabletWeight = Float.parseFloat(attributes.getValue(DeviceEnum.TABLET_WEIGHT.getValue()));
            ((LaptopTablet)current).setTabletWeight(tabletWeight);
        } else if (DeviceEnum.ORIGIN.getValue().equals(localName)) {
            currentManufacture = new Manufacture();
            currentEnum = DeviceEnum.ORIGIN;
        } else {
            DeviceEnum temp = DeviceEnum.valueOf(localName.toUpperCase());
            if (withText.contains(temp)) {
                currentEnum = temp;
            }
        }
    }

    private void putDeviceAttributes(Attributes attributes) {
        current.setName(attributes.getValue(DeviceEnum.NAME.getValue()));
        try {
            String type = attributes.getValue(DeviceEnum.TYPE.getValue());
            if (type != null && !type.isEmpty()) {
                current.setDeviceType(DeviceType.takeValueFromString(type));
            } else {
                current.setDeviceType(DeviceType.COMPUTER);
            }
        } catch (NoSuchDeviceTypeException e) {
            LOG.error(e);
        }
    }
    private void putBatteryCapacity(Attributes attributes) {
        int batteryCapacity = 0;
        try {
            batteryCapacity = Integer.parseInt(attributes.getValue(DeviceEnum.BATTERY_CAPACITY.getValue()));
        } catch (NumberFormatException e) {
            LOG.error(e);
        }
        ((Laptop) current).setBatteryCapacity(batteryCapacity);
    }
}
