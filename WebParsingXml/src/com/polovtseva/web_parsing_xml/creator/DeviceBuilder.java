package com.polovtseva.web_parsing_xml.creator;

import com.polovtseva.web_parsing_xml.entity.Device;

import java.util.Set;

/**
 * Created by User on 02.01.2016.
 */
public abstract class DeviceBuilder {

    protected Set<Device> devices;

    public abstract void buildSetDevices(String filename);
    public abstract Set<Device> getDevices();

}
