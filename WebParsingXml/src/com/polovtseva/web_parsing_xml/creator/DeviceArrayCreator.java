package com.polovtseva.web_parsing_xml.creator;

import com.polovtseva.web_parsing_xml.entity.Device;
import com.polovtseva.web_parsing_xml.exception.NoSuchParserException;

import java.util.Set;

/**
 * Created by User on 11.01.2016.
 */
public class DeviceArrayCreator {
    public static Set<Device> create(String parser, String filename) throws NoSuchParserException {
        DeviceBuilder deviceBuilder = null;
        if ("DOM".equalsIgnoreCase(parser)) {
            deviceBuilder = new DeviceDOMBuilder();
        } else if ("StAX".equalsIgnoreCase(parser)) {
            deviceBuilder = new DeviceStAXBuilder();
        } else if ("SAX".equalsIgnoreCase(parser)) {
            deviceBuilder = new DeviceSAXBuilder();
        }
        if (deviceBuilder != null) {
            deviceBuilder.buildSetDevices(filename);
            return deviceBuilder.getDevices();
        }
        throw new NoSuchParserException();
    }
}
