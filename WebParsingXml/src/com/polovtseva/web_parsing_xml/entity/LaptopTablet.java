package com.polovtseva.web_parsing_xml.entity;

/**
 * Created by User on 01.01.2016.
 */
public class LaptopTablet extends Laptop {
    private float tabletWeight;

    public LaptopTablet() {
    }

    public float getTabletWeight() {
        return tabletWeight;
    }


    public void setTabletWeight(float tabletWeight) {
        this.tabletWeight = tabletWeight;
    }

    @Override
    public String toString() {
        return "Laptop-Tablet{" +
                "tabletWeight=" + tabletWeight +
                ", origin=" + getOrigin() +
                ", price=" + getPrice() +
                ", name=" + getName() +
                ", critical= " + isCritical() +
                ", deviceType=" + getDeviceType() +
                '}';
    }
}
