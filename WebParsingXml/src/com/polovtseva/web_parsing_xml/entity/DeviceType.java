package com.polovtseva.web_parsing_xml.entity;

import com.polovtseva.web_parsing_xml.exception.NoSuchDeviceTypeException;

/**
 * Created by User on 02.01.2016.
 */
public enum  DeviceType {
    COMPUTER("computer"),
    COMPUTER_COMPONENT("computer-component");

    private String value;

    DeviceType(String v) {
        value = v;
    }

    public static DeviceType takeValueFromString(String value) throws NoSuchDeviceTypeException {
        for (DeviceType c: DeviceType.values()) {
            if (c.value.equals(value)) {
                return c;
            }
        }
        throw new NoSuchDeviceTypeException();
    }
}
