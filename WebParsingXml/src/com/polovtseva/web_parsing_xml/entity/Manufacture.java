package com.polovtseva.web_parsing_xml.entity;

/**
 * Created by User on 01.01.2016.
 */
public class Manufacture {
    private String country;
    private String city;
    private String name;
    private String street;

    public Manufacture() {
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    @Override
    public String toString() {
        return "Manufacture{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", name='" + name + '\'' +
                ", street='" + street + '\'' +
                '}';
    }
}
