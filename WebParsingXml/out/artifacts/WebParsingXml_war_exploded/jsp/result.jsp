<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Parsing</title>
</head>
<body>
<p>Chosen ${parser}</p>
<h5>Laptops:</h5>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Origin country</td>
        <td>Origin city</td>
        <td>Origin street</td>
        <td>Origin name</td>
        <td>Price</td>
        <td>Critical</td>
        <td>Device Type</td>
        <td>Battery capacity</td>
    </tr>
    <c:forEach var="item" items="${laptops}">
        <tr>
            <td>${item.name}</td>
            <td>${item.origin.country}</td>
            <td>${item.origin.city}</td>
            <td>${item.origin.street}</td>
            <td>${item.origin.name}</td>
            <td>${item.price}</td>
            <td>${item.critical}</td>
            <td>${item.deviceType}</td>
            <td>${item.batteryCapacity}</td>
        </tr>
    </c:forEach>
</table>
<h5>Laptop-Tablets:</h5>
<table border="1">
    <tr>
        <td>Name</td>
        <td>Origin country</td>
        <td>Origin city</td>
        <td>Origin street</td>
        <td>Origin name</td>
        <td>Price</td>
        <td>Critical</td>
        <td>Device Type</td>
        <td>Battery capacity</td>
        <td>Tablet weight</td>
    </tr>
    <c:forEach var="item" items="${laptopTablets}">
        <tr>
            <td>${item.name}</td>
            <td>${item.origin.country}</td>
            <td>${item.origin.city}</td>
            <td>${item.origin.street}</td>
            <td>${item.origin.name}</td>
            <td>${item.price}</td>
            <td>${item.critical}</td>
            <td>${item.deviceType}</td>
            <td>${item.batteryCapacity}</td>
            <td>${item.tabletWeight}</td>
        </tr>
    </c:forEach>
</table>
<br>
<button onclick="goBack()">Go Back</button>

<script>
    function goBack() {
        window.history.back();
    }
</script>

</body>
</html>
